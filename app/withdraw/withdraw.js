const amplifypay = require('../../utils/amplifier').bank,
	queryHelper = require('../../utils/query-helper'),
	db = require('../../database/database'),
	paystacker = require('../../utils/paystacker'),
	functions = require('../../utils/functions'),
	constant = require('../../utils/constants'),
	mongoose = require('mongoose'),

	Loan = db.Loan,
	User = db.User,
	Withdraw = db.Withdraw,
	status_code_success = '0',
	loan_paid = '1',
	loan_not_paid = '0';

module.exports = (function(){

let Obj = {

	decline: function (withdrawal_id, callback) {
		var payload = {};

		Loan.findOne({
		    withdraw: withdrawal_id
		}).populate('user').populate('withdraw').exec(function(err, loan) {

			if (err !== null || !loan)
			{
				console.log(err);
				return callback(false, {
					message: 'There was an error'
				});
			}

		    let user = loan.user,
		        withdrawal = loan.withdraw;

		    payload.template = "declinewithdrawal";
		    payload.subject = "Loan Withdrawal Declined";
		    payload.email = user.email;
		    payload.name = user.firstname + " " + user.lastname;
		    //formatMoney extends a prototype and is located in functions.js
		    payload.amount = withdrawal.amount.formatMoney(2, '.', ',');

		    functions.Mailer(payload);

		    Obj._completeDecline(loan, withdrawal, callback);
		});
	},

	approve: function (user_id) {

	},

	_completeDecline: function (loan, withdrawal, callback){

		//withdrawal.payout_status = constant.status.DECLINED;
		
		withdrawal.remove(function(err, w) {

		    if (err !== null || !w)
		    {
		    	console.log(err);
		    	return callback(false, {
		    		message: 'There was an error'
		    	});
		    }

		    console.log('Complete decline');

		    Obj._resetLoanStatus(loan, w, callback);
		});
	},

	_resetLoanStatus: function(loan, withdrawal, callback){

		loan.status = constant.status.APPROVED;

		loan.save(function(err, data) {

		    if (err !== null || !data)
		    {
		    	console.log(err);
		    	return callback(false, {
		    		message: 'There was an error'
		    	});
		    }

		    console.log('Reset loan status');

		    Obj._resetTokenisation(loan, withdrawal, callback);
		});
	},

	_resetTokenisation: function(loan, withdrawal, callback){

		loan.user.rebill_token = '';

		loan.user.save(function(err, data) {

		    if (err !== null || !data)
		    {
		    	console.log(err);
		    	return callback(false, {
		    		message: 'There was an error'
		    	});
		    }

		    console.log('Reset tokenisation', 'done!');

		    callback(true, {
	    		payout: constant.status.DECLINED,
	    		message: 'Withdrawal processed successfully'
	    	});
		});
	},

	_getWithdrawals: function (page, status, callback){

		queryHelper(Withdraw)
		    //.with({path: 'user', select:'firstname lastname phone'})
		    .populate({path: 'user loan', select: '-pin'})
		    .$where("this.payout_status === '" + status + "'")
		    .page(page || 1)
		    .paginate(20)
		    .getMany(function(err, data) {

		    if (err !== null) {
		    	console.log(err);
		        callback(false,{
		        	data: [],
		            message: "No withdrawal found"
		        });
		    } else {
		        callback(
		            true,
		           	{
		            	withdrawals: data.modelData,
		            	resultCount: data.resultCount,
		            	currentPage: data.currentPage,
		            	pageCount: data.pageCount,
		            	totalItems: data.totalItems,
		            	itemsPerPage: 20
		        });
		    }
		});
	},

	getPendingWithdrawals: function (page, callback){

		Obj._getWithdrawals(page, constant.status.PENDING, callback);
	},

	getCompletedWithdrawals: function (page, callback){

		Obj._getWithdrawals(page, constant.status.COMPLETED, callback);
	},

	getDeclinedWithdrawals: function (page, callback){

		Obj._getWithdrawals(page, constant.status.DECLINED, callback);

	},

	createWithdrawalRequest: function (loan, user, callback) {

		let obj = {
			amount: loan.amount,
			payout_status: constant.status.PENDING,
			user: user._id,
			loan:loan._id
		};

		Withdraw.create(obj, function (err, withdrawal) {
			if (err !== null)
			{
				console.log(err);
				callback(false, {
					message: 'There was an error saving token'
				});
			}
			else
			{
				let payload = {};

				payload.template = "withdraw";
				payload.subject = "Withdrawal Application";

				//formatMoney extends a prototype and is located in functions.js
				payload.amount = loan.amount.formatMoney(2, '.', ',');

				payload.email = user.email;
				payload.name = user.firstname + " " + user.lastname;

				functions.EmailCert(payload);

				callback(true, withdrawal);
			}
		});
	}
};

return Obj;
})();
