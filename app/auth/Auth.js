var LocalStrategy   = require('passport-local').Strategy;
var cors = require('cors');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var request = require('request');
var constant = require('../../utils/constants');
var functions = require('../../utils/functions');
var Flutterwave = require('flutterwave');
var flutterwave = new Flutterwave(constant.flutterwave.api_key,constant.flutterwave.merchant_key);
db = require('../../database/database');
var User= db.User;


module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
      done(null, user);
    });

    passport.deserializeUser(function(user, done) {
      done(null, user);
    });


  //============================================= SIGNUP =========================================//

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'phone',
        passwordField : 'pin',
        passReqToCallback : true
    },
        function(req, phone, pin, done) {
            var user = req.body;
            User.findOne({'phone' :  user.phone}, function(err, user_obj) {
               if (err)
                   return done(err);

               if (user_obj === null) {

                   	User.find({}, function (err, users) {

                      	if (users){
                          	user.id = (users.length + 1).toString();
                      	}
                      	else {
                          	user.id = "1";
                      	}

                       	user.pin =  bcrypt.hashSync(user.pin);
                       	user.occupation = "0";
                       	user.phone_status = user.email_status = user.profile_status = "unverified";
                       	// console.log(user);
                       	User.insert(user);

                       return done(null, user);
                   	});
               	}
               	else {
                  return done(null, false, req.flash('info', 'Phone number already exist'));
               	}
            });
        }
    ));

  //============================================= LOGIN =========================================//

    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    },
        function(req, email, password, done) {

            let user;

            // console.log(req.body);

            if (req.body.email == "hello@payconnect.ng"  && req.body.password == "Abuja1234!") {

                user = {
                    email    : "hello@payconnect.ng",
                    password : "Abuja1234!"
                };

                return done(null, user);

            }else if (req.body.email == "admin@payconnect.ng"  && req.body.password == "Abuja1234!") {

                user = {
                    email    : "admin@payconnect.ng",
                    password : "Abuja1234!"
                };

                return done(null, user);

            }else if (req.body.email == "agent@payconnect.ng"  && req.body.password == "Abuja1234!") {

                user = {
                    email    : "agent@payconnect.ng",
                    password : "Abuja1234!"
                };

                return done(null, user);

            }else if (req.body.email == "admin@alphacredit.ng"  && req.body.password == "Abuja1234!") {
                user = {
                    email    : "admin@alphacredit.ng",
                    password : "Abuja1234!"
                };

                return done(null, user);
            } else{

                return done(null, false, req.flash('info', 'Access denied! wrong credencials'));
            }
        }
    ));
};
