let amplifier = require('../../utils/amplifier').bank,
	dbHelper = require('../../utils/query-helper'),
	functions = require('../../utils/functions'),
	db = require('../../database/database'),
	Investment = db.Investment,
	User = db.User;

module.exports = function(user_id, amount, source){
	
	let code, type = '', duration = '', duration_count, loan_interest;

	function sendMail(invest)
	{
		let payload = {};

		payload.plan = invest.type;
		payload.amount = amount;
		payload.return = total;
		payload.rate = invest.rate;
		payload.interest = result;
		payload.end = invest.paydate;
		payload.subject = "Payconnect Investment Certificate";
		payload.template = "investmail";

		payload.start = functions.Create(); //Date

		functions.EmailCert(payload);
	}

	Obj = {
		invest: function(callback)
		{
			//get user data
		    User.findOne({_id: user_id})
		    .then(function(user){
				
				if (user.balance >= amount && source === 'wallet')
				{
					intrest = parseFloat(amount) * (loan_interest / 100);
		            result = intrest * duration_count;
		            total = result + parseFloat(amount);

					invest = {};
					invest.type = type;
					invest.code = code;
					invest.months = duration;
					invest.amount = amount;
					invest.total_interest = result;
					invest.monthly_interest = intrest;
					invest.rate = `${loan_interest}%`;
					invest.start = functions.today();
					invest.paydate = functions.later(duration_count);
					invest.payback = total;
					invest.user = user_id;

					Investment.create(invest).then(function(investment){
						
						user.balance -= amount;

						user.save(function(err, user){

							sendMail(investment);

							callback(true, {
								message: 'Invest created successfully', 
								balance: user.balance, 
								investment: investment
							});
						});

					}).catch(function(e){
		    			console.log(e);

						callback(false, {message: 'There was a problem creating investment'});
				    });
				}
				else if(user.balance < amount)
				{
					console.log('Insufficient fund');
					callback(false/*status*/, {message: 'Insufficient fund'});
				}
		    })
		    .catch(function(e){
		    	console.log('User Data not found');
				callback(false, {message: 'There was a problem creating investment'});
		    });	
		},

		monthly: function(){
			loan_interest = 3.25;
			duration_count = 1;
			code = 1;

			type = 'monthly';
			duration = '1 Month';

			return this;
		},

		quarterly: function(){

			loan_interest = 3.85;
			duration_count = 3;
			code = 2;

			type = 'quarterly';
			duration = '3 Months';

			return this;
		},

		biannually: function(){

			loan_interest = 5;
			duration_count = 6;
			code = 3;

			type = 'Bi-annually';
			duration = '6 Months';

			return this;
		},

		annually: function(){

			loan_interest = 6.5;
			duration_count = 12;
			code = 4;

			type = 'annually';
			duration = '12 Months';

			return this;
		},
		type: function (type){

			switch ('' + type)
			{
				case '1':
					this.monthly();
					break;
				case '2':
					this.quarterly();
					break;
				case '3':
					this.biannually();
					break;
				case '4':
					this.annually();
					break;
			}

			return this;
		}
	};

	return Obj;
};