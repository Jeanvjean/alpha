const db = require('../../database/database'),
	paystacker = require('../../utils/paystacker');

module.exports = {

	getBanks: function (callback) {

		paystacker().getBanks(function(err, resp){

			if (err !== null || resp.status === false)
			{
				console.log(err);
				return callback(false, {
					message: 'There was an error',
					data: {}
				});
			}

			let banks = [], prop;

			resp.data.forEach(function(o){

				if (o.active === true)
				{
					banks.push({
						bank_name: o.name,
						bank_code: o.code
					});
				}
			});	

        	let validBanks = ['044', '023', '063', '050', '084', '070', '011', 
				'214', '058', '030', '082', '014', '014', '076', '221', 
				'068', '232', '100', '032', '033', '215', '035', '057'];

        	banks = banks.filter(function(bank){

        		return validBanks.indexOf(bank.bank_code) !== -1;

        	});

        	function compare(a, b) {
        	  if (a.bank_name < b.bank_name)
        	    return -1;
        	  if (a.bank_name > b.bank_name)
        	    return 1;
        	  return 0;
        	}

        	banks.sort(compare);

			callback(true, {banks: banks});
		});
	},

	getAccountDetails: function (account_number, bank_code, callback) {

		let data = {
			account_number: account_number,
			bank_code: bank_code
		};

		paystacker().data(data).getAccountDetails(function(err, resp){

			if (err !== null)
			{
				console.log(err);
				return callback(false, {
					message: 'There was an error',
					data: {}
				});
			}

			if (resp.status === false)
			{
				console.log(resp);
				return callback(false, {
					message: 'Account number or bank is not valid',
					data: {}
				});
			}

			resp.data.account_name = resp.data.account_name.trim();

			callback(true, {data: resp.data});
		});
	}
};
