const amplifypay = require('../../utils/amplifier').bank,
	queryHelper = require('../../utils/query-helper'),
	db = require('../../database/database'),
	paystack = require('paystack')('sk_test_976f5e5b4594bba88bfc5281eaa6e47a57c34274'),
	paystacker = require('../../utils/paystacker'),
	constant = require('../../utils/constants'),
    functions = require('../../utils/functions'),

	//paystack = require('paystack')('sk_live_22eb5eb7630c8ffb986c19ca0ade0722da8b9005'),

	Loan = db.Loan,
	User = db.User,
	status_code_success = '0',
	loan_paid = '1',
	loan_not_paid = '0',

	loanEmailSender = constant.LOAN_EMAIL_SENDER;

module.exports = {
	_q: '',
	tenure: '1',
	userIds: [],
	q: function (q) {

		if (q)
			this._q = q;
		else
			this._q = '';

		return this;
	},
	// Get user ids to aid loan searching
	// Only users that fit search criteria will have their ids stored
	// which will be used to fetch loans later
	getUserIds: function (callback)
	{
		let that = this;

		if (!this._q)
		{
			return callback('', []);
		}

		// Get all users that fit query criteria
		User.find({$text: {$search: that._q}}, function(err, users){

			let userIds = [];

			if (users)
			{
				users.forEach(function(user){

					userIds.push(user._id);
				});

				that.userIds = userIds;

				callback(that._q, userIds);
			}
		});
	},

	requestLoan: function (user_id) {
	},

	approveLoan: function (user_id) {
	},

	declineLoan: function (user_id) {
	},

	getLoans: function(status, page, callback){

		let itemsPerPage = 20,
			qString = this._q;

		this.getUserIds(function(query, userIds){

			let cond = !query.length ? {} : {user: {"$in": userIds}};

			queryHelper(Loan, cond)
			    .$where("this.status === '" + status + "'")
			    .with({path: 'user', select: '-pin -otp'})
			    .page(page)
			    .paginate(itemsPerPage)
			    .getMany(function(err, data) {

			    if (err !== null) {

			        callback(false, {
			            status: false,
			            loans: {},
			            message: "No loan found"
			        });

			    } else {

			        callback(true, {
			        	status: true,
			            loans: data.modelData,
			            resultCount: data.resultCount,
			            currentPage: data.currentPage,
			            pageCount: data.pageCount,
			            totalItems: data.totalItems,
			            itemsPerPage: itemsPerPage,
			            message: "All loans fetched successfully"
			        });
			    }
			});
		});
	},

	getPendingLoans: function(page, callback){

		// this.getLoans(constant.status.PENDING, page, callback);
		let itemsPerPage = 100,
          	today = functions.today();

    	this.getUserIds(function(query, userIds){
			console.log(userIds)
    		let cond = !query.length ? {} : {user: {"$in": userIds}};

    		//Get loans with conditions
			queryHelper(Loan, cond)
			    .$where("this.status === '" + constant.status.PENDING + "' && this.repayment_left > 0")
	            //.$where('this.end_date < ' + "'" + today + "'" ) !== ''
			    //.$where('!')
			    .with({path: 'user',
			    	match: { user_alert: { $ne: '' }},
			    	select: '-pin -otp'})
			    .page(page)
			    .paginate(itemsPerPage).getMany(function(err, data) {
					// console.log(data)
			    let loans = data.modelData.filter(function(loan) {
					console.log(loan)
			        return loan.user; // return loans with user record
			    });
				// console.log(loans)

			    if (err !== null) {

			        callback(false, {
			            status: false,
			            loans: {},
			            message: "No loan found"
			        });

			    } else {

			        callback(true, {
			        	status: true,
			            loans: loans,
			            resultCount: data.resultCount,
			            currentPage: data.currentPage,
			            pageCount: data.pageCount,
			            totalItems: data.totalItems,
			            itemsPerPage: itemsPerPage,
			            message: "All loans fetched successfully"
			        });
			    }
			});
		});
	},

	getNoDocLoans: function(page, callback){

		let itemsPerPage = 100,
          	today = functions.today();

    	this.getUserIds(function(query, userIds){

    		let cond = !query.length ? {} : {user: {"$in": userIds}};

    		//Get loans with conditions
			queryHelper(Loan, cond)

			    .$where("this.status === '" + constant.status.PENDING + "' && this.repayment_left > 0")
	            //.$where('this.end_date < ' + "'" + today + "'" ) !== ''
			    //.$where('!')
			    .with({path: 'user',
			    	match: { user_alert: ''},
			    	select: '-pin -otp'})
			    .page(page)
			    .paginate(itemsPerPage).getMany(function(err, data) {

			    let loans = data.modelData.filter(function(loan) {
			        return !loan.user.user_alert; // return loans with no user record
			    });

			    if (err !== null) {

			        callback(false, {
			            status: false,
			            loans: {},
			            message: "No loan found"
			        });

			    } else {

			        callback(true, {
			        	status: true,
			            loans: loans,
			            resultCount: data.resultCount,
			            currentPage: data.currentPage,
			            pageCount: data.pageCount,
			            totalItems: data.totalItems,
			            itemsPerPage: itemsPerPage,
			            message: "All loans fetched successfully"
			        });
			    }
			});
		});
	},

	getApprovedLoans: function(page, callback){

		this.getLoans(constant.status.APPROVED, page, callback);
	},

	getDeclinedLoans: function(page, callback){

		this.getLoans(constant.status.DECLINED, page, callback);
	},

	getAcceptedLoans: function(page, callback){

		this.getLoans(constant.status.ACCEPTED, page, callback);
	},

	getCompletedLoans: function(page, callback){

		this.getLoans(constant.status.REPAID, page, callback);
	},

	getRejectedLoans: function(page, callback){

		this.getLoans(constant.status.REJECTED, page, callback);
	},

	getOutstandingLoans: function(page, callback){

		let itemsPerPage = 20,
            today = new Date(functions.today()).getTime();

		this.getUserIds(function(query, userIds){

    		let cond = !query.length ? {} : {user: {"$in": userIds}};

			queryHelper(Loan, cond)
			    .$where("this.repayment_left === 0")
			    .$where("this.status === '" + constant.status.ACCEPTED + "'")
	            .$where('new Date(this.end_date).getTime() <= ' + "'" + today + "'" )
			    .with({path: 'user', select: '-pin -otp'})
			    .page(page)
			    .paginate(itemsPerPage).getMany(function(err, data) {

			    if (err !== null) {

			        callback(false, {
			            status: false,
			            loans: {},
			            message: "No loan found"
			        });

			    }
			    else {

			        callback(true, {
			        	status: true,
			            loans: data.modelData,
			            resultCount: data.resultCount,
			            currentPage: data.currentPage,
			            pageCount: data.pageCount,
			            totalItems: data.totalItems,
			            itemsPerPage: itemsPerPage,
			            message: "All loans fetched successfully"
			        });
			    }
			});
		});
	},

	getLoanHistory: function(user_id, page, callback){

		console.log(user_id, page);
		let itemsPerPage = 5,
            today = new Date(functions.today()).getTime();

		queryHelper(Loan, {
				user: user_id,
				$or: [
					{status: constant.status.REPAID},
					{status: constant.status.DECLINED},
					{status: constant.status.REJECTED},
				]
			})
		    //.$where("this.repayment_left == 0")
		    //.$where(`this.user === "${user_id}"`)
		   	//.$where("this.status === '" + constant.status.REPAID + "'")
            //.$where('new Date(this.end_date).getTime() <= ' + "'" + today + "'" )
		    //.with({path: 'user', select:'firstname lastname phone id_card user_alert'})
		    .page(page)
		    .paginate(itemsPerPage).getMany(function(err, data) {

		    if (err !== null) {

		        callback(false, {
		            status: false,
		            data: {},
		            message: "No loan found"
		        });

		    } else {

		        callback(true, {
		        	status: true,
		            data: data.modelData,
		            resultCount: data.resultCount,
		            currentPage: data.currentPage,
		            pageCount: data.pageCount,
		            totalItems: data.totalItems,
		            itemsPerPage: itemsPerPage,
		            message: "All loans fetched successfully"
		        });
		    }
		});
	},

	rejectLoan: function (loan_id, user_id, callback) {

		Loan.findOne({
		    _id: loan_id,
		    user: user_id
		}).exec(function(err, loan) {

			if (err !== null || !loan)
			{
				console.log(err);
				return callback(false, {
					message: 'There was an error'
				});
			}

		    loan.status = 'rejected';

		    loan.save(function(err, saved) {

		    	if (err !== null)
		    	{
		    		console.log(err);
		    		return callback(false, {
		    			message: 'There was an error'
		    		});
		    	}
		    	else
		    	{
		    		callback(true, {
		    			message: 'Loan rejected!'
		    		});
		    	}
		    });
		});
	},

	_processLoanDifference: function(loan, amount, callback)
	{
		let that = this,

		updateObj = {
		    $inc: {
		        payment_made: +parseFloat(amount),
		        repayment_left: -parseFloat(amount)
		    }
		};

		if ( loan.repayment_left - amount === 0 )
		{
			updateObj.$set = {status: constant.status.REPAID};
		}

		loan.update(updateObj, function(err, loan_update){

			if (err !== null)
			{
				console.log(err);
				callback(false, {
					message: 'Error repaying loan'
				});
			}
			else
			{
				that._updateUserLoanPrivilege(loan, amount, callback);
			}
		});
	},

	_updateUserLoanPrivilege: function(loan, amount, callback)
	{

		let that = this,
			//The highest amount this user can request in the app
			last_eligible_amt = loan.user.eligible_loan_amounts.pop(),
			//The highest loan amount a user can collect (30% of monthly_net_income)
			max_eligible_amt = (parseFloat(loan.user.monthly_net_income) * constant.LOAN_MAX_PERCENT),
			current_loan_eligible_amt, updateArr = [],
			today = new Date(functions.today()).getTime(),
			repayment_complete = parseInt(loan.repayment_left - amount) == 0 ? true : false;

		console.log('loan.repayment_left - amount = ', parseInt(loan.repayment_left - amount));
		console.log(loan.user.eligible_loan_amounts, 'max_eligible_amt > last_eligible_amt', max_eligible_amt > last_eligible_amt);

		if (new Date(loan.end_date).getTime() >= today && repayment_complete)
		{
			if (!loan.user.eligible_loan_amounts.length)
			{
				//Initial eligible amount(10000) is not set, set it
				updateArr.push(constant.INITIAL_LOAN_AMOUNT);
			}

			let loan_eligible_inc = constant.INITIAL_LOAN_AMOUNT * constant.LOAN_INCREMENT_PERCENT;

			// User's eligible loan amount should be max 30% of new monthly income
			if (max_eligible_amt > last_eligible_amt)
			{
				if ((loan_eligible_inc + last_eligible_amt) > max_eligible_amt)
				{
					updateArr.push(max_eligible_amt);
				}
				else
				{
					updateArr.push(last_eligible_amt + loan_eligible_inc);
				}
			}
		}

		loan.user.update({$pushAll: {eligible_loan_amounts: updateArr}}, function(err, user){

			if (err !== null)
			{
				console.log(err);
				callback(false, {
					message: 'Error repaying loan'
				});
			}
			else
			{
				let loan_user = loan.user.firstname,
					loan_amt = loan.amount.formatMoney(2, '.', ','),
					loan_payment_amt = amount.formatMoney(2, '.', ','),
					loanMessage;

				if (repayment_complete)
				{
					loanEmailMessage = `Hello ${loan_user},
					Your PayConnect Loan - ₦${loan_amt} has been successfully repaid.
					Login to the app and reapply. Thank you`;

					loanTextMessage = `Hello ${loan_user}, Your PayConnect Loan - =N=${loan_amt} has been successfully repaid. Login to the app and reapply. Thank you`;
				}
				else
				{
					loanEmailMessage = `Hello ${loan_user},
					₦${loan_payment_amt} of your PayConnect loan of ₦${loan_amt} has been successfully repaid.
					Thank you`;

					loanTextMessage = `Hello ${loan_user}, =N=${loan_payment_amt} of your PayConnect loan of ₦${loan_amt} has been successfully repaid. Thank you`;
				}


				let emailOptions = {
				    email: loan.user.email,
				    subject: 'Payconnect',
				    template: 'email/messenger',
				    message: loanEmailMessage
				};

				functions.Mailer(emailOptions);

				functions.messenger(loan.user.phone, loanTextMessage);

				callback(true, {
					message: 'Loan repayment successful'
				});
			}
		});
	},

	authorizePayment: function (loan, amount, callback) {

		self = this;

		amplifypay().recurring().data({
			reference: loan.rebill_token
		}).authorize(function(err, res){

			/*if (err !== null || res.statusCode !== status_code_success)
			{
				console.log(err);
				return callback(false, {
					message: 'Error authorizing loan payment'
				});
			}*/

			self._processLoanDifference(loan, amount, callback);
		});
	},

	repayLoan: function (loan_id, amount, callback)
	{
		let self = this, minAmount = 100;

		amount = parseFloat(amount);

		console.log(amount);

		Loan.findOne({_id: loan_id}).populate('user').exec(function(err, loan){
			console.log(loan)
			if ( err !== null || !loan )
			{
				console.log(err);
				return callback(false, {message: 'Error was an error.'});
			}

			if (!amount)
			{
				return callback(false, {message: 'Empty loan amount.'});
			}

			if (loan.repayment_left === 0)
			{
				return callback(false, {message: 'Loan has already been repayed.'});
			}

			//Amount cannot be less that minAmount unless amount is equal to repayment_left
			if (amount < minAmount && amount !== loan.repayment_left)
			{
				return callback(false, {message: `You cannot pay less than ${minAmount} naira.`});
			}

			if ((loan.repayment_left - minAmount) < minAmount && amount !== loan.repayment_left)
			{
				return callback(false, {message: 'Your payment must be ' + loan.repayment_left + ' naira.'});
			}

			if (amount > loan.repayment_left)
			{
				return callback(false, {message: 'Invalid loan amount.'});
			}


		    if (loan.user.rebill_token) {

		        paystacker().data({
		            "authorization_code": loan.user.rebill_token,
		            "email": loan.user.email,
		            "amount": amount * 100
		        }).chargeAuthorization(function(error, resp) {

		            if (resp)
		            {
		                if (resp.status === false || resp.data.status === "failed")
		                {
		                	console.log(resp);
		                    return callback(false, {message: 'There was an error.'});
		                }
		                else
		                {
		                    return self._processLoanDifference(loan, amount, callback);
		                }
		            }
		            else
		            {
		                console.log(error);
		        		return callback(false, {message: 'There was an error.'});
		            }
		        });
		    }
		    else
		    {
		        return callback(false, {message: 'Account not tokenized.'});
		    }
		});
	},

	deleteLoan: function (loan_id, callback) {

		Loan.findOne({_id: loan_id}, function(err, loan){

			loan.remove();

			if (!err)
			{
				callback(true, {
					message: "Loans deleted"
				});
			}
			else
			{
				callback(false, {
					message: 'Error deleting loans'
				});
			}
		});
	},

	//send bulk email/sms to users
	sendReminder: function (options, callback)
	{
		if (options.loan_status === 'outstanding')
		{
			return this.sendOutstandingReminder(options, callback);
		}
		else if (options.loan_status === 'pending')
		{
			return this.sendPendingReminder(options, callback);
		}
		else if (options.loan_status === 'no-doc')
		{
			return this.sendNoDocReminder(options, callback);
		}

		var maillist = '';
		var smslist = '';

		Loan.find({status: options.loan_status}).populate('user').exec(function(err, loans){

			if (err || !loans.length)
			{
				return callback(false, {
					message: "Message not sent"
				});
			}

			let user = null;

			loans.forEach(function(loan){

				if (loan && loan.user)
				{
					user = loan.user;
					//Compile email list
					if (options.send_sms && user.phone)
						smslist +=  smslist ? ',' + user.phone : user.phone;

					//Compile phone numbers eg 0803478678,
					if (options.send_email && user.email)
						maillist +=  maillist ? ', ' + user.email  : user.email;
				}
			});

			if (options.send_email)
			{
				let emailOptions = {
				    email: maillist,
				    subject: options.title,
				    template: 'email/messenger',
				    message: options.message,
				    from: loanEmailSender
				};

				functions.Mailer(emailOptions);
			}

			if (options.send_sms)
			{
				functions.messenger(smslist, options.message, options.title);
			}

			return callback(true, {
				message: "Message sent"
			});
		});
	},

	//send bulk email/sms to users
	sendOutstandingReminder: function (options, callback)
	{
		let maillist = '',
			smslist = '',
			today = new Date(functions.today()).getTime();

		Loan.find({
			status: constant.status.ACCEPTED,

		})
		.$where('new Date(this.end_date).getTime() <= ' + "'" + today + "'" )
		.populate('user').exec(function(err, loans){

			if (err || !loans.length)
			{
				return callback(false, {
					message: "Message not sent"
				});
			}

			let user = null;

			loans.forEach(function(loan){

				user = loan.user;
				//Compile email list
				if (options.send_sms && user.phone)
					smslist +=  smslist ? ',' + user.phone : user.phone;

				//Compile phone numbers eg 0803478678,
				if (options.send_email && user.email)
					maillist +=  maillist ? ',' + user.email  : user.email;
			});

			if (options.send_email)
			{
				let emailOptions = {
				    email: maillist,
				    subject: options.title,
				    template: 'email/messenger',
				    message: options.message,
				    from: loanEmailSender
				};

				functions.Mailer(emailOptions);
			}

			if (options.send_sms)
			{
				functions.messenger(smslist, options.message, options.title);
			}

			return callback(true, {
				message: "Message sent"
			});
		});
	},

	//send bulk email/sms to users
	sendPendingReminder: function (options, callback)
	{
		let maillist = '',
			smslist = '',
			today = new Date(functions.today()).getTime();

		Loan.find({
			status: constant.status.PENDING,
		})
	    .populate({path: 'user',
	    	match: {user_alert: {$ne: ''}},
	    	select:'firstname lastname phone email id_card user_alert'})
		.exec(function(err, data){

			let loans = data.filter(function(loan) {
			    return loan.user; // return loans with user record
			});

			if (err || !loans.length)
			{
				return callback(false, {
					message: "Message not sent"
				});
			}

			let user = null;

			loans.forEach(function(loan){

				user = loan.user;
				//Compile email list
				if (options.send_sms && user.phone)
					smslist +=  smslist ? ',' + user.phone : user.phone;

				//Compile phone numbers eg 0803478678,
				if (options.send_email && user.email)
					maillist +=  maillist ? ',' + user.email  : user.email;
			});

			if (options.send_email)
			{
				let emailOptions = {
				    email: maillist,
				    subject: options.title,
				    template: 'email/messenger',
				    message: options.message,
				    from: loanEmailSender
				};

				functions.Mailer(emailOptions);
			}

			if (options.send_sms)
			{
				functions.messenger(smslist, options.message, options.title);
			}

			return callback(true, {
				message: "Message sent"
			});
		});
	},

	//send bulk email/sms to users
	sendNoDocReminder: function (options, callback)
	{
		let maillist = '',
			smslist = '',
			today = new Date(functions.today()).getTime();

		Loan.find({
			status: constant.status.PENDING,
		})
		.populate({path: 'user',
			match: {user_alert: ''},
			select:'firstname lastname phone email id_card user_alert'})
		.exec(function(err, data){

			let loans = data.filter(function(loan) {
			    return loan.user && !loan.user.user_alert; // return loans with no user record
			});

			if (err || !loans.length)
			{
				return callback(false, {
					message: "Message not sent"
				});
			}

			let user = null;

			loans.forEach(function(loan){

				user = loan.user;

				console.log(user);

				//Compile email list
				if (options.send_sms && user.phone)
					smslist +=  smslist ? ',' + user.phone : user.phone;

				//Compile phone numbers eg 0803478678,
				if (options.send_email && user.email)
					maillist +=  maillist ? ',' + user.email  : user.email;
			});

			if (options.send_email)
			{
				let emailOptions = {
				    email: maillist,
				    subject: options.title,
				    template: 'email/messenger',
				    message: options.message,
				    from: loanEmailSender
				};

				functions.Mailer(emailOptions);
			}

			if (options.send_sms)
			{
				functions.messenger(smslist, options.message, options.title);
			}

			return callback(true, {
				message: "Message sent"
			});
		});
	},

	_createLoan: function (reqBody, callback) {
		let phone = reqBody.phone,
			amount = parseFloat(reqBody.amount),
			loan = {},
			that = this,
			result, total;

		User.findOne({
		    'phone': phone
		}, function(err, user) {
			// console.log(user)
		    if (!user) {
		        callback(false, {
		            message: "Invalid account"
		        });
		    } else {

		    	if ( user && user.account_status === "blocked" )
		    	{
		    		return callback(false, {
		    		    message: "Account is blocked"
		    		});
		    	}


		        User.update({
		            'phone': phone
		        }, {
		            $set: {
		                occupation: reqBody.occupation
		            }
		        });

		        Loan.findOne({
		            'user': user._id,
		            'status': constant.status.PENDING
		        }, function(err, loan_obj) {

		            if (loan_obj)
		            {
		                return callback(false, {
		                    message: "User already has a pending loan"
		                });
		            }
		            else {
						// console.log(amount)
		            	if (user.eligible_loan_amounts.indexOf(amount) === -1)
		            	{
		            	    return callback(false, {
		            	        message: "Invalid loan amount"
		            	    });
		            	}

		                if (amount >= 1000 && amount <=3000) {
		                	result = parseFloat(amount) * constant.LOAN_INTEREST_PERCENT1;
							loan.tenure = "7"
		                }else if (amount >= 3001 && amount <= 5000) {
		                	result = parseFloat(amount) * constant.LOAN_INTEREST_PERCENT2;
							loan.tenure = "10"
		                }else if (amount >= 5001 ) {
		                	result = parseFloat(amount) * constant.LOAN_INTEREST_PERCENT3;
							loan.tenure = "15"
		                }else if (amount == 0) {
		                	result = amount
		                }
		                total = result + parseFloat(amount);
						console.log(result,total)
		                // if ( !reqBody.occupation )
		                // {
		                //     return callback(false, {
		                //         data: {},
		                //         message: "Loan could not be created"
		                //     });
		                // }

		                // loan.tenure = that.tenure;
		                // loan.start_date = functions.Create();
		                // loan.end_date = functions.later(1);
		                loan.payment_made = 0;
		                loan.repayment_left = total;

		                //loan.tokennized = user.rebill_token ? true : false;
		                loan.status = "pending";
		                loan.phone = phone;
		                loan.name = user.firstname + " " + user.lastname;
		                loan.interest = result.toFixed(2);
		                loan.bvn_status = '1';
		                loan.amount = parseFloat(amount);
		                loan.user = user._id;

		                let loanMessage = `There is a new loan request to be processed.`;

		                let emailOptions = {
		                    email: constant.LOAN_NOTIFICATION_EMAIL,
		                    subject: 'Payconnect - New loan request',
		                    template: 'email/messenger',
		                    message: loanMessage
		                };

		                functions.Mailer(emailOptions);

		                Loan.create(loan, function(err, loan){
		                    console.log(err);

		                    callback(true, {
		                        data: loan,
		                        message: "Loan created successfully"
		                    });
		                });
		            }
		        });
		    }
		});
	},

	createLoan: function (reqBody, callback){

		switch ('' + reqBody.occupation)
		{
			case '1':
				this.tenure = '1';
				break;
			case '2':
				this.tenure = '2';
				break;
			case '3':
				this.tenure = '3';
				break;
			case '4':
				this.tenure = '4';
				break;
		}

		this._createLoan(reqBody, callback);
	}
};
