const amplifypay = require('../../utils/amplifier').bank,
	queryHelper = require('../../utils/query-helper'),
	db = require('../../database/database'),
	paystacker = require('../../utils/paystacker'),
	functions = require('../../utils/functions'),
	constant = require('../../utils/constants'),
	mongoose = require('mongoose'),

	User = db.User,
	Loan = db.Loan,

	loanEmailSender = constant.LOAN_EMAIL_SENDER;

module.exports = (function(){

let Obj = {
	_q: '',
	q: function (q) {
		
		if (q)
			this._q = q;
		else
			this._q = '';

		return this;
	},

	getEmployees: function (callback) {

		let employers = [
			'9mobile',
			'Access Bank',
			'Airforce',
			'Airtel',
			'Army',
			'Citibank',
			'Civil Defence',
			'Diamond Bank',
			'Ecobank Nigeria',
			'Fidelity Bank Nigeria',
			'First Bank of Nigeria',
			'First City Monument Bank',
			'Glo',
			'Guaranty Trust Bank',
			'Heritage Bank plc',
			'Keystone Bank Limited',
			'MTN',
			'Navy',
			'NTel',
			'Road Safety',
			'Skye Bank',
			'Stanbic IBTC Bank Nigeria Limited',
			'Standard Chartered Bank',
			'Sterling Bank',
			'Suntrust Bank Nigeria Limited',
			'Teachers Board',
			'Union Bank of Nigeria',
			'United Bank for Africa',
			'Unity Bank plc',
			'Ventures Platform',
			'Wema Bank',
			'Zenith Bank'
		];

		callback(
		    true,
		   	{
		    	employers: employers
		});
	},

	getUsers: function (page, callback){

		let that = this;

		queryHelper(db.User, !that._q ? {} : {$text: {$search: that._q}})
		    //.with({path: 'user', select:'firstname lastname phone'})
		    //.populate({path: 'user', select: '-pin'})
		    //.$where("this.payout_status === '" + status + "'")
		    .page(page || 1)
		    .paginate(100)
		    .getMany(function(err, data) {

		    if (err !== null) {
		    	console.log(err);
		        callback(false,{
		        	data: [],
		            message: "No user found"
		        });
		    } else {
		        callback(
		            true,
		           	{
		            	users: data.modelData,
		            	resultCount: data.resultCount,
		            	currentPage: data.currentPage,
		            	pageCount: data.pageCount,
		            	totalItems: data.totalItems,
		            	itemsPerPage: 100
		        });
		    }
		});
	},

	delete: function (user_id, callback) {

		User.findOne({_id: user_id}, function(err, user){

			console.log("user data removed");
			user.remove();

			if (!err)
			{
				callback(true, {
					message: "User deleted"
				});
			}
			else
			{
				callback(false, {
					message: 'Error deleting user'
				});
			}
		});
	},

	update: function (user, callback) {


		User.update({_id: user._id}, {$set: user}, function(err, user){

			if (!err)
			{
				callback(true, {
					message: "User data saved"
				});
			}
			else
			{
				callback(false, {
					message: 'User data not saved'
				});
			}
		});
	},

	messenger: function (message, email, sms, callback)
	{

	},
	//send bulk email/sms to users
	sendBulkMessage: function (options, callback)
	{
		var maillist = '';
		var smslist = '';

		User.find({}, function(err, users){

			if (err || !users)
			{
				return callback(false, {
					message: "Message not sent"
				})
			}

			users.forEach(function(user){
				//Compile email list
				if (options.send_sms && user.phone)
					smslist +=  smslist ? ',' + user.phone : user.phone;
				
				//Compile phone numbers eg 0803478678, 
				if (options.send_email && user.email)
					maillist +=  maillist ? ', ' + user.email  : user.email;
			});

			if (options.send_email)
			{
				let emailOptions = {
				    email: maillist,
				    subject: options.title,
				    template: 'email/messenger',
				    message: options.message,
				    from: loanEmailSender
				};
				
				functions.Mailer(emailOptions);
			}

			if (options.send_sms)
			{
				functions.messenger(smslist, options.message, options.title);
			}

			return callback(true, {
				message: "Message sent"
			});
		});
	},
	//Send single message to user
	sendMessage: function (options, callback) {

		User.findOne({_id: options.user_id}, function(err, user){

			if (err || !user)
			{
				return callback(false, {
					message: "Message not sent"
				});
			}

			if (options.send_email)
			{
				let emailOptions = {
				    email: user.email,
				    subject: options.title,
				    template: 'email/messenger',
				    message: options.message,
				    from: loanEmailSender
				};
				
				functions.Mailer(emailOptions);
			}

			if (options.send_sms)
			{
				functions.messenger(user.phone, options.message, user.title);
			}

			return callback(true, {
				message: "Message sent"
			});
		});
	},
};

return Obj;
})();
