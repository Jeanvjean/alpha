const amplifypay = require('../../utils/amplifier').bank,
	queryHelper = require('../../utils/query-helper'),
	db = require('../../database/database'),
	functions = require('../../utils/functions'),
	constant = require('../../utils/constants'),
	mongoose = require('mongoose'),

	User = db.User;


module.exports = (function(){

let Obj = {
	sendBulkMessage: function (callback) {

	},

	sendMessage: function (callback){
	},

	sendBulkSMS: function (callback) {
	},

	sendBulkEmail: function (callback){
	},

	sendSMS: function (callback) {
	},

	sendEmail: function (callback){
	}
};

return Obj;
})();
