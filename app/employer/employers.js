const amplifypay = require('../../utils/amplifier').bank,
	queryHelper = require('../../utils/query-helper'),
	paystacker = require('../../utils/paystacker'),
	functions = require('../../utils/functions'),
	constant = require('../../utils/constants'),
	mongoose = require('mongoose'),

	Employer = require('../../database/models/employer');


module.exports = (function(){

let Obj = {
	getEmployers: function (callback) {

		let employers = [];

		Employer.find({}, function(err, emps) {

			emps.forEach(function(emp) {
				
				employers.push(emp.employer_name);
			})

			employers.sort();

			callback(
			    true,
			   	{
			    	employers: employers
			});
		});
	},

	getAdminEmployers: function (page, callback) {

		queryHelper(Employer)
		    //.with({path: 'user', select:'firstname lastname phone'})
		    //.populate({path: 'user', select: '-pin'})
		    //.$where("this.payout_status === '" + status + "'")
		    .page(page || 1)
		    .paginate(100)
		    .getMany(function(err, data) {

		    if (err !== null) {
		    	console.log(err);
		        callback(false,{
		        	data: [],
		            message: "No employer found"
		        });
		    } else {
		        callback(
		            true,
		           	{
		            	employers: data.modelData,
		            	resultCount: data.resultCount,
		            	currentPage: data.currentPage,
		            	pageCount: data.pageCount,
		            	totalItems: data.totalItems,
		            	itemsPerPage: 100
		        });
		    }
		});
	},

	addEmployer: function (employer_name, callback) {

		let employer = {
			employer_name: employer_name
		};

		Employer.create(employer, function(err, employer){
			
			if (employer)
			{
				callback(true, {
					message: "Employer added"
				});
			}
			else
			{
				callback(false, {
					message: 'Error adding employer'
				});
			}
		});
	},

	updateEmployer: function (employer_id, employer_name, callback) {

		let employer = {
			employer_name: employer_name
		};

		Employer.update({_id: employer_id}, {$set: {employer_name: employer_name}}, function(err, employer){
			
			if (employer)
			{
				callback(true, {
					message: "Employer updated"
				});
			}
			else
			{
				callback(false, {
					message: 'Error updating employer'
				});
			}
		});
	},

	removeEmployer: function (employer_id, callback) {

		Employer.findOne({_id: employer_id}).remove(function(err){
			
			console.log(err);
			if (!err)
			{
				callback(true, {
					message: "Employer deleted"
				});
			}
			else
			{
				callback(false, {
					message: 'Error deleting employer'
				});
			}
		});
	},
};

return Obj;
})();
