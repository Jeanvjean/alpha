const paystacker = require('../../utils/paystacker'),
	queryHelper = require('../../utils/query-helper'),
	db = require('../../database/database'),
	functions = require('../../utils/functions'),
	constant = require('../../utils/constants'),

	mongoose = require('mongoose'),
	Loan = db.Loan,
	Withdraw = db.Withdraw,
	status_code_success = '0';

module.exports = (function(){

	Obj =  {

		loan: null,
		user: null,
		pin: null,

		sendPhone: function (reference, phone, callback)
		{
			let data = {
				phone: phone,
				reference: reference
			};

			console.log(data);

			paystacker().data(data).sendPhone(function(err, resp){

				if (err !== null || resp.status === false)
				{
					console.log(err, resp);

					callback(false, {
						message: 'There was an error'
					});
				}
				else
				{
					console.log(resp.data);

					if (resp.data.status === 'send_otp')
					{
						//Obj.sendOTP(reference, callback);
						Obj._saveReference(Obj.user, reference, callback);
					}

				}
			});
		},

		sendPIN: function (reference, phone, callback)
		{
			let data = {
				pin: Obj.pin,
				reference: reference
			};

			console.log(data);

			paystacker().data(data).sendPIN(function(err, resp){

				if (err !== null || resp.status === false)
				{
					console.log(err, resp);

					callback(false, {
						message: 'There was an error'
					});
				}
				else
				{
					if (resp.data.status === 'send_otp')
					{
						//Save reference and request for otp
						Obj._saveReference(Obj.user, reference, callback);
					}
					else if (resp.data.status === 'send_phone')
					{
						Obj.sendPhone(reference, phone, callback);
					}
					else
					{
						console.log(Obj.loan, resp);
						/*

						callback(false, {
							message: 'There was an error'
						});*/

						Obj._saveRebillToken(Obj.loan, resp.data.authorization.authorization_code, callback);
					}
				}
			});
		},

		openUrl: function (user, data, callback)
		{
			user.txn_reference = data.reference;

			//Save rebill token
			user.save(function(err, user){

				if (err !== null)
				{
					console.log(err);
					callback(false, {
						message: 'There was an error creating token'
					});
				}
				else
				{
					callback(true, {
						message: '',
						open_url: true,
						url: data.url
					});
				}
			});
		},

		//sendOTP
		complete: function (loan_id, otp, callback)
		{
			Loan.findOne({_id: loan_id}).populate('user').exec(function (err, loan) {

				if (err !== null || !loan)
				{
					console.log(err, 'There was an error getting user data');
					return callback(false, {
						message: 'There was an error getting user data'
					});
				}

				let data = {
					otp: otp,
					reference: loan.user.txn_reference
				};

				paystacker().data(data).sendOTP(function(err, resp){

					if (err !== null || resp.status === false)
					{
						console.log(err, resp);

						callback(false, {
							message: 'There was an error'
						});
					}
					else
					{
						console.log(resp.data);
						Obj._saveRebillToken(loan, resp.data.authorization.authorization_code, callback);
					}
				});
			});
		},

		initialize: function(loan_id, card_details, callback) {

			Loan.findOne({_id: loan_id}).populate('user').exec(function (err, loan) {

				if (err !== null || !loan)
				{
					console.log(err, 'There was an error getting user data');
					return callback(false, {
						message: 'There was an error.'
					});
				}

				/*if (loan.tokennized === true)
				{
					return callback(false, {
						message: 'Account already tokennized.'
					});
				}*/

				let user = loan.user;

				Obj.loan = loan;
				Obj.user = loan.user;
				Obj.pin = card_details.pin;

				let query_data = {
					email: user.email,
					card: {
						number: card_details.number,
						cvv: card_details.cvv,
						expiry_month: card_details.expiry_month,
						expiry_year: card_details.expiry_year
					},
					amount: 100 * 100,
					callback_url: 'http://localhost:8080/payments/paystack/tokenisation/callback'
				};

				console.log(query_data);

				paystacker().data(query_data).tokenize(function(err, resp){

					if (err !== null || resp.status === false)
					{
						console.log(err, resp);

						callback(false, {
							message: 'There was an error'
						});
					}
					else
					{
						console.log(resp.data);

						if (resp.data.status === 'send_pin')
						{
							Obj.sendPIN(resp.data.reference, user.phone, callback);
						}
						else if (resp.data.status === 'open_url')
						{
							Obj.openUrl(user, resp.data, callback);
						}
						else if(resp.data && resp.data.authorization && resp.data.authorization.authorization_code)
						{
							Obj._saveRebillToken(loan, resp.data.authorization.authorization_code, callback);
						}
						else
						{
							console.log(err, resp);

							callback(false, {
								message: 'There was an error'
							});
						}
					}
				});
			});
		},

		completeAuth: function(loan_id, callback) {

			Loan.findOne({_id: loan_id}).populate('user').exec(function (err, loan) {

				if (err !== null || !loan)
				{
					console.log(err, 'There was an error getting user data');
					return callback(false, {
						message: 'There was an error getting user data'
					});
				}

				let data = {
					reference: loan.user.txn_reference
				};

				paystacker().data(data).verifyCharge(function(err, resp){

					console.log('Resp: ', resp);

					if (err !== null || resp.status === false)
					{
						console.log(err);

						callback(false, {
							message: 'Unable to charge card'
						});
					}
					else
					{
						console.log(resp.data);
						Obj._saveRebillToken(loan, resp.data.authorization.authorization_code, callback);
					}
				});
			});
		},

		/*initiate: function(user_id, callback){

			User.findOne({_id: user_id}).exec(function (err, user) {

				if (err !== null)
				{
					console.log(err);
					return callback(false, {
						message: 'There was an error getting user data'
					});
				}
				else
				{
					Obj.getTokenRef(user, callback);
				}
	  		});
		},

		//Get transaction reference from amplify
		//for subsequent payments
		getTokenRef: function(user_data, callback){

			let data = {
				"transactionid": user_data._id,
				"amount": '100.00',
				"accountNumber": user_data.account_number || '0690000000',
				"bankCode": user_data.bank_code || '044',
				"currency": "NGN",
				"narration": "Payconnet Token Setup",
				"customerInfo": {
				  	"firstname": user_data.firstname,
				  	"lastname": user_data.lastname,
				  	"email": user_data.email,
				  	"phone": user_data.phone,
				  	"address": "",
				  	"dob": '',
				  	"bvn": ''
				}
			};

			paystacker().data(data).pay(function(err, amplify_res){

				if (err !== null || amplify_res.statusCode !== '0')
				{
					console.log(err, amplify_res);

					callback(false, {
						message: 'There was an error'
					});
				}
				else
				{
					Obj._saveRebillToken(user_data, amplify_res, callback);
				}
			});
		},

		//Authorize payment for tokenization
		complete: function(user_id, otp, callback){

			User.findOne({_id: user_id}, function(err, user){

				if (err !== null)
				{
					return callback(false, {
						message: 'There was an error getting user data'
					});
				}

				let data = {
					reference: user.rebill_token,
					otp: otp,
				};

				paystacker().data(data).complete(function(err, amplify_res){

					if (err !== null || amplify_res.statusCode !== '0')
					{
						console.log(err, amplify_res);

						Obj._resetRebillToken(user, amplify_res, callback);
					}
					else
					{
						callback(true, {
							message: 'Token created'
						});
					}
				});
			});
		},*/

		_saveReference: function(user, reference, callback){

			user.txn_reference = reference;

			//Save rebill token
			user.save(function(err, user){

				if (err !== null)
				{
					console.log(err);
					callback(false, {
						message: 'There was an error saving token'
					});
				}
				else
				{
					callback(true, {
						message: 'Send OTP',
						send_otp: true
					});
				}
			});
		},

		_saveRebillToken: function(loan, authorization_code, callback){

			let user = loan.user;

			user.rebill_token = authorization_code;

			//Save rebill token
			user.save(function(err, user){

				if (err !== null)
				{
					console.log(err);
					callback(false, {
						message: 'There was an error saving token'
					});
				}
				else
				{
					Obj._updateWithdrawalRequest(loan, callback);
				}
			});
		},

		_saveLoanTokenStatus: function(loan, callback){

			//loan.tokennized = true;
			loan.status = 'accepted';

			//Save rebill token
			loan.save(function(err, loan){

				if (err !== null)
				{
					console.log(err);
					callback(false, {
						message: 'There was an error saving token'
					});
				}
				else
				{
					//Obj._updateWithdrawalRequest(loan, callback);
					callback(true, {
						message: 'Token saved'
					});
				}
			});
		},

		_resetRebillToken: function(user, amplify_res, callback){

			user.rebill_token = amplify_res.reference;
			user.transaction_id = amplify_res.transactionId;

			//Save rebill token
			user.save(function(err, user){

				if (err !== null)
				{
					console.log(err);

					callback(false, {
						message: 'There was an error creating token'
					});
				}
				else
				{
					callback(false, {
						message: 'Token reset'
					});
				}
			});
		},

		_updateWithdrawalRequest: function (loan, callback) {

			let obj = {
				amount: loan.amount,
				payout_status: constant.status.PENDING,
				user: loan.user._id
			};

			loan.withdraw = loan.withdraw || new mongoose.mongo.ObjectID();

			Withdraw.update({_id: loan.withdraw}, obj, {upsert: true}, function (err, data) {
				if (err !== null)
				{
					console.log(err);
					callback(false, {
						message: 'There was an error saving token'
					});
				}
				else
				{
					let payload = {};

					payload.template = "withdraw";
					payload.subject = "Withdrawal Application";

					//formatMoney extends a prototype and is located in functions.js
					payload.amount = loan.amount.formatMoney(2, '.', ',');

					payload.email = loan.user.email;
					payload.name = loan.user.firstname + " " + loan.user.lastname;

					functions.EmailCert(payload);

					let loanMessage = 'There is a new withdrawal request to be processed.';

					let emailOptions = {
					    email: constant.LOAN_NOTIFICATION_EMAIL,
					    subject: 'Alphacredit - New withdrawal request',
					    template: 'email/messenger',
					    message: loanMessage
					};

					functions.Mailer(emailOptions);

					Obj._saveLoanTokenStatus(loan, callback);
				}
			});
		}
	};

	return Obj;
})();
