
function queryHelper(model, findCond)
{
	let paginate = 10,
		page = 1,
		projection,
		select_conditions = [],
		string_where = '',
		populate = [],
		object_id;

	let DataModel = model; //Store a reference to the model

	function argArray(args/*arguments*/)
	{
		return [].slice.call(args);
	}

	// Object that holds the crud functions
	let Obj = {
		_executeCallback: function (callback, err, data) {

			if (typeof callback === 'function')
				callback(err, data);
		},

		_queryBuilder: function(query){

			if (populate.length)
			{
				populate.forEach(function(p){
					query = query.populate(p);
				});
			}

			if (select_conditions.length)
			{
				select_conditions.forEach(function(cond){
					query = query.$where(cond);
				});
			}

			return query;
		},

		delete: function (/*id, callback*/) {
			//for cases where id is set using id()
			let args = argArray(arguments),
			
			callback = args.pop(),
			_id = args.pop(),
			id =  _id ? _id : object_id;

			return DataModel.remove({ _id: id }).exec(function(err) {

			    if (err !== null) Obj._executeCallback(callback, err, false);
			    
			    Obj._executeCallback(callback, null, true);
			});
		},

		update: function (/*[id,] obj, callback*/) {

			let args = argArray(arguments),
				callback = args.pop(),
				obj = args.pop(),
				_id = args.pop(),
				id =  _id ? _id : object_id;

		    // Update the record
		    let update = DataModel.update(select_conditions ? select_conditions : {_id: id}, obj, function(err, dataUpdated) {

		      	if (err !== null) Obj._executeCallback(callback, err, false);
		      	
		      	// Data was modified
		      	if (dataUpdated.nModified === 1)
		      	{
		      		Obj._executeCallback(callback, null, true);
		      	}
		      	//Data was not modified
		      	else
		      	{
		      		Obj._executeCallback(callback, null, false);
		      	}
		    });

			return update;
		},

		create: function (obj, callback) {

			return new DataModel(obj).save(function(err, res) {
				// Call the callback function to notify
				// on success or failure of DB query
			  	if (err !== null) Obj._executeCallback(callback, err, false);

			  	Obj._executeCallback(callback, null, res);
			});
		},

		getOne: function (/*id, callback*/) {

			//for cases where id is set using id()
			let args = argArray(arguments),
				callback = args.pop(),
				_id = args.pop(),
				id =  _id ? _id : object_id;

			// get record with a particular ID
			let query = DataModel.findOne( findCond ? findCond : {_id: id}, projection ? projection : null)
				.exec(function(err, data) {
				  	if (err !== null) Obj._executeCallback(callback, err, null);

				  	// Pass playlist to callback
				  	Obj._executeCallback(callback, null, data);
				});

			return query;
		},

		getMany: function (callback) {
			
			let perPage = paginate, tmp_query;

			page = page > 1 ? page : 1;
			//page = page > 1 ? Math.max(1, page) : 1;

			let modelProjection = { 
			    title: true,
			    _id: true
			};

			console.log(findCond);

			// get paginated
			let query = DataModel.find(findCond ? findCond : {}, projection ? projection : null);
			
			tmpQuery = DataModel.find(findCond ? findCond : {}, projection ? projection : null);

			query = Obj._queryBuilder(query);
			tmpQuery = Obj._queryBuilder(tmpQuery);

			query = query.limit(perPage)
	    	.skip(perPage * ( page - 1 ) )
	    	.exec(function(err, modelData) {
			  	
			  	if (err !== null) return Obj._executeCallback(callback, err, false);

	    		tmpQuery.count().exec(function(err, count) {

	    			// Pass data to callback
	    		  	Obj._executeCallback(callback, null, {
	                    modelData: modelData,
	                    resultCount: modelData.length,
	                    currentPage: page, // Current Page
	                    perPage: paginate,
	                    totalItems: count,
	                    pageCount: Math.ceil(count / perPage) // Number of pages
	                });
		        });
			});

			return query;
		},

		projection: function (p) {

			projection = p;

			return Obj;
		},

		populate: function (p) {

			if (Object.prototype.toString.apply(p) === '[object Object]')
			{
				populate.push(p);
			}
			else if (Object.prototype.toString.apply(p) === '[object String]')
			{
				populate.push(p);
			}
			else
			{
				throw new Error("Invalid argument: argument must be a String or Array");
			}

			return Obj;
		},

		with: function (p) {
			return this.populate(p);
		},

		$where: function (cond){
			select_conditions.push(cond);

			return this;
		},

		paginate: function (p) {

			paginate = p;

			return Obj;
		},

		page: function (p) {

			page = p;

			return Obj;
		},

		id: function(id) {

			object_id = id;

			return Obj;
		}
	};

	return Obj;
}

/* 
// Example Usage

queryHelper = require('./Utils/queryHelper');

let Playlist = require('./Database/Models/playlist');

console.log(queryHelper);

queryHelper.model(Playlist).create({title: "New Playlist"}, function (data) {

	console.log(data);
});	

queryHelper.model(Playlist).getMany(function (data) {

	console.log(data);
});	


queryHelper.model(Playlist)
.projection({})
.paginate(10)
.page(1)
.getMany(function (data) {

	console.log(data);
});*/


module.exports = queryHelper;