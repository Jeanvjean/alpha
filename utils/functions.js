let nodemailer = require('nodemailer');
let smtpTransport = require('nodemailer-smtp-transport');
let hbs = require('nodemailer-express-handlebars');
let db = require('../database/database');
let User = db.User;
let request = require('request');
let paystack = require('paystack')('sk_live_22eb5eb7630c8ffb986c19ca0ade0722da8b9005');
let path = require('path');
let appDir = path.dirname(require.main.filename);
let moment = require('moment')

const   smsusername = 'jybsus',
        smspassword = 'Abuja1234!',
        smsendpoint = 'http://www.quicksms1.com/api/sendsms.php'

        mailService = 'Mailjet',
        mailUser = 'ce2122993201e28a16e7c60205379bf2',
        mailPass = '44382ea0c480d08b65ab4fc06a39db75';
        mailSecret = 'mailjet._domainkey.payconnect.ng',
        mailDomain = 'payconnect.ng';

let options = {
    viewEngine: {
        extname: '.hbs',
        layoutsDir: appDir + '/view/email/',
        defaultLayout: 'main',
        partialsDir: 'view'
    },
    viewPath: appDir + '/view',
    extName: '.hbs'
};


Number.prototype.formatMoney = function(c, d, t){

    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d === undefined ? "." : d;
    t = t === undefined ? "," : t;

    var n = this,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };


function Confirm(ref) {}

function Charge(auth, amount, email) {
    console.log(auth);
    console.log(amount);
    console.log(email);
    let response = paystack.transaction.charge({
        "authorization_code": auth,
        "email": email,
        "amount": amount
    }, function(error, body) {
        console.log(error);
        console.log(body);
        if (error) {
            return error;
        } else {
            return body;
        }
    });
    return response;
}

function SMS(phone, otp) {


    let data = {
        username: smsusername,
        password: smspassword,
        sender: "Alphacredit",
        recipient: phone,
        message: "Your OTP confirmation code is " + otp
    };

    request.post(smsendpoint, {
        form: data
    });
}

var start = moment().format('L')
var first = moment().add(7, 'days').calendar()
var second = moment().add(10, 'days').calendar()
var third = moment().add(15, 'days').calendar()

function today() {
    let day = new Date().getDate();
    let year = new Date().getFullYear();
    let month = new Date().getMonth() + 1;

    day = (day / 10  < 1 ? `0${day}` : day);
    month = (month / 10  < 1 ? `0${month}` : month);

    let newdate = year + '-' + month + '-' + day;

    return newdate;
}
function Create() {
    let day = new Date().getDate();
    let year = new Date().getFullYear();
    let month = new Date().getMonth() + 1;

    day = (day / 10  < 1 ? `0${day}` : day);
    month = (month / 10  < 1 ? `0${month}` : month);

    let newdate = year + '-' + month + '-' + day;

    return newdate;
}

function later(month) {

    let year, day;

    day = new Date().getDate();

    if (month + new Date().getMonth() > 11) {
        let yholder = new Date().getFullYear();
        let mholder = new Date().getMonth() + month + 1;

        year = yholder + 1;
        month = mholder - 12;

    } else {
        year = new Date().getFullYear();
        let mholder = new Date().getMonth() + 1;

        month = mholder + month;
    }

    day = (day / 10  < 1 ? `0${day}` : day);
    month = (month / 10  < 1 ? `0${month}` : month);

    return year + '-' + month + '-' + day;
}

function ForgotPin(phone, pin) {
    let data = {
        username: smsusername,
        password: smspassword,
        sender: "Alphacredit",
        recipient: phone,
        message: "Your new default pin is " + pin
    };

    request.post(smsendpoint, {
        form: data
    });
}

function MemberSMS(phone, message) {
    let data = {
        username: smsusername,
        password: smspassword,
        sender: "Alphacredit",
        recipient: phone,
        message: message
    };

    request.post(smsendpoint, {
        form: data
    });
}

function messenger(phone, message, sender) {
    let data = {
        username: smsusername,
        password: smspassword,
        sender: !sender || sender.length > 11 ? 'Alphacredit' : sender,
        recipient: phone,
        message: message
    };

    request.post(smsendpoint, {
        form: data
    });
}

function Email(email, code) {

    let transporter = nodemailer.createTransport(smtpTransport({
        service: mailService,
        auth: {
            user: mailUser,
            pass: mailPass
        }
    }));

    transporter.use('compile', hbs(options));

    let mailOptions = {
        from: 'Alphacredit <no-Reply@aplhacredit.ng>',
        to: email,
        subject: 'Email Verification',
        template: 'mail',
        context: {
            code: code
        }
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log("using no-reply@alphacredit.ng");
            console.log('Message sent: ' + info.response);
        }
    });
}

function EmailCert(payload) {

    Mailer(payload);
}

function Mailer(payload) {
    console.log(payload);

    let transporter = nodemailer.createTransport(smtpTransport({
        service: mailService,
        auth: {
            user: mailUser,
            pass: mailPass
        }
    }));

    let from = payload.from || 'no-reply@alphacredit.ng';

    transporter.use('compile', hbs(options));

    let mailOptions = {
        from: `Alphacredit <${from}>`,
        bcc: payload.email,
        subject: payload.subject,
        template: payload.template,
        context: payload
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Message sent: ' + info.response);
        }
    });
}

function formatFilename(filename)
{
	return filename.trim().replace(/[^a-z0-9]/gi, '_').toLowerCase();
}

module.exports = {
    SMS: SMS,
    Email: Email,
    Mailer: Mailer,
    Confirm: Confirm,
    Charge: Charge,
    EmailCert: EmailCert,
    MemberSMS: MemberSMS,
    ForgotPin: ForgotPin,
    Create: Create,
    today: today,
    later: later,
    start:start,
    first:first,
    second:second,
    third:third,
    formatFilename: formatFilename,
    messenger: messenger
};
