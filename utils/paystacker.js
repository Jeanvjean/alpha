const https = require('https'),
	  querystring = require('querystring'),
	  parse = require('url').parse,
	  sha256 = require('sha256'),
	  merchant_id = 'SC91YCQSEKLGDGEITUMOQ',
	  crypto = require("crypto"),

	  auth_secret = process.env.APP_ENV === 'dev' ? 'sk_test_976f5e5b4594bba88bfc5281eaa6e47a57c34274' 
	  	: 'sk_live_22eb5eb7630c8ffb986c19ca0ade0722da8b9005';

console.log('Paystack: ', auth_secret.slice(0, 7));

function sendRequest(options, request_data, callback)
{
	console.log("Paystack: init request");

	//Stringify payload
	request_data = JSON.stringify(request_data);

	options.headers['Content-Length'] = Buffer.byteLength(request_data);

	return new Promise(function(resolve, reject) {
		
		let response_data = '', redirect_count = 5, req, response_obj;

		makeRequest(options);

		function req_callback(res) {

			//res.setEncoding('utf8');
			//

			if ((res.statusCode == 302 || res.statusCode == 301) && redirect_count > 0) 
			{
			    response_data = null;
			    redirect_count--;

			    console.log("Redirection to...", res.headers.location);

			    let parse_url = parse(res.headers.location);

				options.hostname = parse_url.hostname; 
				options.path = parse_url.pathname;

			    req = makeRequest(options);
			}
			else
			{
				res.on('data', (chunk) => {
				    response_data += chunk;
				});

				res.on('end', () => {
					
					console.log("Paystack: done", res.statusCode);

					try {
	                    response_obj = JSON.parse(response_data);

        				if (typeof callback === 'function')
                        	callback(null, response_obj, res.statusCode);

                        resolve(response_obj);
	                } catch(e) {
						console.log("Paystack: Unable to parse response data");

	                	console.log(e);

	                	if (typeof callback === 'function')
	                	    callback(e, response_data, res.statusCode);

	                    reject(e);
	                	//resolve(response_obj);
	                }
				});
			}
		}

		function makeRequest(options)
		{
			req = https.request(options, req_callback);

			// reject on request error
	        req.on('error', function(err) {
				
				console.log("Paystack: done with errors");

				if (typeof callback === 'function')
	        		callback(err, null);

	            // This is not a "Second reject", just a different sort of failure
	            reject(err);
	        });

	        if (request_data.length)
	        {
	        	// post the data
	            req.write(request_data);
	        }

			req.end();
		}
	});
}

let transaction = function(recurring){

	let 
		request_data = {},
		request_path,
		options = {
		    hostname: 'api.paystack.co',
		    protocol: 'https:',
		    method: 'POST',
		    path: '/transaction/initialize',
		    headers: {
		        'cache-control': 'no-cache',
		        'authorization': `Bearer ${auth_secret}`,
		        'Content-Type': 'application/json'
		    }
		};
	
	recurring = recurring === true ? recurring : false;

	function parseArgs(data, callback)
	{
		if (callback === undefined && {}.toString.call(data) === '[object Function]')
		{
			callback = data;
		}
		
		if ({}.toString(data) === '[object Object]'  && typeof data !== 'function')
		{
			request_data = data;
		}

		return {data: request_data, callback: callback};
	}

	function send(options, data, callback)
	{
		options.path = request_path ? request_path : options.path;

		return sendRequest(options, data, callback);
	}

	return {
		set urlBase(u){
			options.hostname = u;
		},
		get urlBase(){
			return options.hostname;
		},
		staging: function (hostname)
		{
			options.hostname = hostname ? hostname : 'api.paystack.co';

			return this;
		},
		headers: function(headers) {
			options.headers = headers;

			return this;
		},
		data: function (data) {
			if ({}.toString(data) !== '[object Object]')
				throw new Error('Argument must be an object {}');

			request_data = data;

			return this;
		},
		recurring: function(){
			recurring = true;
			request_path = '/transaction/charge_authorization';

			return this;
		},
		//pay bill
		pay: function (data, callback)
		{
			let args = parseArgs(data, callback);

			//Make transaction id more unique
			if ('transactionid' in args.data)
			{
				let txn_id = crypto.randomBytes(16).toString("hex");

				args.data.reference = args.data.transactionid + txn_id;
			}

			return send(options, args.data, args.callback);
		},
		initiate: function (data, callback)
		{
			let args = parseArgs(data, callback);

			return send(options, args.data, args.callback);
		},
		verifyPayment: function(data, callback){

			let args = parseArgs(data, callback);

			request_path = '/transaction/verify/' + args.data.reference;

			return send(options, {}, args.callback);
		},
		verifyCharge: function(data, callback){

			let args = parseArgs(data, callback);

			options.method = 'GET';
			
			request_path = '/charge/' + args.data.reference;

			return send(options, {}, args.callback);
		},
		verifyAccount: function(data, callback){

			request_path = '';

			//TODO: implement account verification
		},
		authorize: function (data, callback)
		{
			let req_data;
			let args = parseArgs(data, callback);

			if (recurring === false)
			{
				req_data = {
					reference: args.data.reference,
					esavalue: args.data.otp
				};

				request_path = '/api/v1/authorize';
			}
			else
			{
				req_data = args.data;
				request_path = '/transaction/charge_authorization';
			}

			return send(options, req_data, args.callback);
		},
		tokenize: function (data, callback)
		{
			let req_data;
			let args = parseArgs(data, callback);

			req_data = args.data;
			request_path = '/charge';

			return send(options, req_data, args.callback);
		},
		sendPIN: function (data, callback)
		{
			let req_data;
			let args = parseArgs(data, callback);

			req_data = args.data;
			request_path = '/charge/submit_pin';

			return send(options, req_data, args.callback);
		},
		sendPhone: function (data, callback)
		{
			let req_data;
			let args = parseArgs(data, callback);

			req_data = args.data;
			request_path = '/charge/submit_phone';

			return send(options, req_data, args.callback);
		},
		sendOTP: function (data, callback)
		{
			let req_data;
			let args = parseArgs(data, callback);

			req_data = args.data;
			request_path = '/charge/submit_otp';

			return send(options, req_data, args.callback);
		},
		chargeAuthorization: function (data, callback)
		{
			let req_data;
			let args = parseArgs(data, callback);

			req_data = args.data;
			request_path = '/transaction/charge_authorization';

			return send(options, req_data, args.callback);
		},
		getBanks: function (callback)
		{
			request_path = '/bank';

			options.method = 'GET';

			return send(options, {}, callback);
		},
		getAccountDetails: function (data, callback)
		{
			let req_data;
			let args = parseArgs(data, callback);

			options.method = 'GET';

			req_data = args.data;
			request_path = '/bank/resolve?' + querystring.stringify(req_data);

			console.log('Get Data', req_data);

			return send(options, {}, args.callback);
		},
		complete: function (data, callback)
		{
			return this.authorize(data, callback);
		}
	};
};

module.exports = transaction;
