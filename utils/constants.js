/**
 * Created by Ukemeabasi on 18/04/2017.
 */
const constant = {
    flutterwave: {
        api_key: 'tk_XxFMm1bs3udCzJEKcw46',
        merchant_key: 'tk_9uNjJZbm0h'
    },
    INITIAL_LOAN_AMOUNT: 1000,
    LOAN_INTEREST_PERCENT1: .45,
    LOAN_INTEREST_PERCENT2: .40,
    LOAN_INTEREST_PERCENT3: .30,
    LOAN_MAX_PERCENT: 0.3,
    LOAN_INCREMENT_PERCENT: 0.15,
    LOAN_EMAIL_SENDER: 'loan@payconnect.ng',
    LOAN_NOTIFICATION_EMAIL: 'process@payconnect.ng',
    status: {
    	PENDING: 'pending',
    	COMPLETE: 'complete',
    	COMPLETED: 'completed',
        DECLINED: 'declined',
        APPROVED: 'approved',
        ACCEPTED: 'accepted',
        REJECTED: 'rejected',
    	REPAID: 'repaid',
    }
};

module.exports = constant;
