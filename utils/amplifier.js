const https = require('https'),
	  querystring = require('querystring'),
	  parse = require('url').parse,
	  sha256 = require('sha256'),
	  merchant_id = 'SC91YCQSEKLGDGEITUMOQ',
	  crypto = require("crypto");


function sendRequest(options, request_data, callback)
{
	console.log("Amplify Pay: init request");

	//Stringify payload
	request_data = JSON.stringify(request_data);

	options.headers['x-digest'] = sha256(request_data + "8c7590c1-3688-4e82-876a-f83e6a1a2b7d");
	options.headers['Content-Length'] = Buffer.byteLength(request_data);

	return new Promise(function(resolve, reject) {
		
		let response_data = '', redirect_count = 5, req, response_obj;

		makeRequest(options);

		function req_callback(res) {

			//res.setEncoding('utf8');
			//

			if ((res.statusCode == 302 || res.statusCode == 301) && redirect_count > 0) 
			{
			    response_data = null;
			    redirect_count--;

			    console.log("Redirection to...", res.headers.location);

			    let parse_url = parse(res.headers.location);

				options.hostname = parse_url.hostname; 
				options.path = parse_url.pathname;

			    req = makeRequest(options);
			}
			else
			{
				res.on('data', (chunk) => {
				    response_data += chunk;
				});

				res.on('end', () => {

					try {
	                    response_obj = JSON.parse(response_data);
	                } catch(e) {
	                	console.log(e);
	                    reject(e);
	                }

					console.log("Amplify Pay: done");

					if (typeof callback === 'function')
	                	callback(null, response_obj);

	                resolve(response_obj);
				});
			}
		}

		function makeRequest(options)
		{
			req = https.request(options, req_callback);

			// reject on request error
	        req.on('error', function(err) {
				
				console.log("Amplify Pay: done with errors");

				if (typeof callback === 'function')
	        		callback(err, null);

	            // This is not a "Second reject", just a different sort of failure
	            reject(err);
	        });

	        if (request_data.length)
	        {
	        	// post the data
	            req.write(request_data);
	        }

			req.end();
		}
	});
}

let bank = function(recurring){

	let 
		request_data = {},
		request_path,
		options = {
		    hostname: 'bankpay.staging.ampslg.com',
		    protocol: 'https:',
		    method: 'POST',
		    path: '/api/v1/pay',
		    headers: {
		        'cache-control': 'no-cache',
		        'x-key': merchant_id,
		        'Content-Type': 'application/json'
		    }
		};
	
	recurring = recurring === true ? recurring : false;

	function parseArgs(data, callback)
	{
		if (callback === undefined && {}.toString.call(data) === '[object Function]')
		{
			callback = data;
		}
		
		if ({}.toString(data) === '[object Object]'  && typeof data !== 'function')
		{
			request_data = data;
		}

		return {data: request_data, callback: callback};
	}

	function send(options, data, callback)
	{
		options.path = request_path ? request_path : options.path;

		return sendRequest(options, data, callback);
	}

	return {
		set urlBase(u){
			options.hostname = u;
		},
		get urlBase(){
			return options.hostname;
		},
		staging: function (hostname)
		{
			options.hostname = hostname ? hostname : 'bankpay.staging.ampslg.com';

			return this;
		},
		headers: function(headers) {
			options.headers = headers;

			return this;
		},
		data: function (data) {
			if ({}.toString(data) !== '[object Object]')
				throw new Error('Argument must be an object {}');

			request_data = data;

			return this;
		},
		recurring: function(){
			recurring = true;
			request_path = '/api/v1/autocharge/initiate';

			return this;
		},
		//pay bill
		pay: function (data, callback)
		{
			let args = parseArgs(data, callback);

			//Make transaction id more unique
			if ('transactionid' in args.data)
			{
				let txn_id = crypto.randomBytes(16).toString("hex");

				args.data.transactionid = args.data.transactionid + txn_id;
			}

			return send(options, args.data, args.callback);
		},
		initiate: function (data, callback)
		{
			let args = parseArgs(data, callback);

			return send(options, args.data, args.callback);
		},
		verifyPayment: function(data, callback){

			request_path = '/api/v1/verify';

			let args = parseArgs(data, callback);

			return send(options, { transactionId: args.data.txn_id}, args.callback);
		},
		verifyAccount: function(data, callback){

			request_path = '/api/v1/banks/verify';

			//TODO: implement account verification
		},
		authorize: function (data, callback)
		{
			let req_data;
			let args = parseArgs(data, callback);

			if (recurring === false)
			{
				req_data = {
					reference: args.data.reference,
					esavalue: args.data.otp
				};

				request_path = '/api/v1/authorize';
			}
			else
			{
				req_data = {
					reference: args.data.reference,
				};

				request_path = '/api/v1/autocharge/complete';
			}

			return send(options, req_data, args.callback);
		},
		complete: function (data, callback)
		{
			return this.authorize(data, callback);
		}
	};
};

module.exports.bank = bank;
