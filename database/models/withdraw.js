var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var withdrawSchema = new Schema({
   	amount: {type: Number, required: true},
   	payout_status: { type: String,  enum: ['pending', 'completed', 'declined'], default: 'pending'},
   	user : {type: Schema.Types.ObjectId, required: true, ref: 'User'},
   	loan : {type: Schema.Types.ObjectId, required: true, ref: 'Loan'}
}, {
	timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

withdrawSchema.index({'$**': 'text'});

module.exports = mongoose.model('Withdraw', withdrawSchema);
