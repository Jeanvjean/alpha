var mongoose = require('mongoose'),
   Schema = mongoose.Schema;

var walletSchema = new Schema({
   bvn            : {type: String},
   bvn_status     : {type: String},
   pin            : {type: String},
   account_name   : {type: String},
   account_number : {type: String},
   bank_code 	  : {type: String},
   balance        : {type: Number, required: true}
});

module.exports = mongoose.model('Wallet', walletSchema);