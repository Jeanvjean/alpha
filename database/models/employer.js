var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var employerSchema = new Schema({
   	employer_name: {type: String, required: true, unique: true, dropDups: true}
});

employerSchema.index({'$**': 'text'});

module.exports = mongoose.model('Employer', employerSchema);
