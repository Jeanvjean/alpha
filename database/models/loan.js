var mongoose = require('mongoose');
    mongoosePaginate = require('mongoose-paginate')
	Schema = mongoose.Schema;

var loanSchema = new Schema({
   	tenure        : {type: String, required: false}, //Loan duration
   	start_date    : {type: String, required: false },//changed
   	end_date      : {type: String, required: false },//changed
   	payment_made  : {type: Number},
   	repayment_left: {type: Number},
   	status        : {type: String, required: true,
   		enum: ['pending', 'declined', 'approved', 'accepted', 'rejected', 'repaid', 'completed'] },
   	decline_message: String,
   	interest      : {type: Number, required: false},
   	amount        : {type: Number },//changed
	screenshot    : { type: String },

   	withdraw	  : {type: Schema.Types.ObjectId, ref: 'Withdraw'},
   	user          : {type: Schema.Types.ObjectId, required: true, ref: 'User'}
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

/*loanSchema.index({'$**': "text"});*/
loanSchema.index({
	start_date: "text",
	end_date: "text",
	status: "text",
	amount: "text",
	repayment_left: "text",
	'user.firstname': "text",
	'user.lastname': "text",
	'user.email': "text",
	'user.phone': "text",
}, {name:"loanSchemaIndex"});

loanSchema.plugin(mongoosePaginate)

loanSchema.post('remove', function(doc, next) {
    // 'this' is the client being removed. Provide callbacks here if you want

    this.model('Withdraw').remove({_id: this.withdraw}).exec();

    next();
});

module.exports = mongoose.model('Loan', loanSchema);
