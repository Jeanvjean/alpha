var mongoose = require('mongoose'),
   Schema = mongoose.Schema;

var userSchema = new Schema({
   	phone: {type: String, required: true},
   	pin: {type: String},

   	email_status: {type: String, enum: ['unverified', 'verified'], default: 'unverified'},
   	phone_status: {type: String, enum: ['unverified', 'verified'], default: 'unverified'},
   	account_status: {type: String, enum: ['blocked', 'active'], default: 'active'},

   	otp: {type: String, required: true},

   	email: {type: String},
   	code: {type: Number},
   	firstname: {type: String},
   	lastname: {type: String},
   	middlename: {type: String},
   	dob: {type: String},

    gender: {type: String, default: ''},

    // marital_status: {type: String, default: ''},
    // deppendants: {type: String, default: ''},
    // qualification: {type: String, default: ''},
    employee_status: {type: String, default: ''},
    employer: {type: String, default: ''},
    // experience: {type: String, default: ''},
    monthly_net_income: {type: Number, default: 0.00},

    location: {type: String, default: ''},
    address: {type: String, default: ''},
    // type_of_home: {type: String, default: ''},

   	balance: {type: Number, default: 0.00},
   	rebill_token  : {type: String, default: ''},
   	txn_reference: {type: String, default: ''},
   	bank_name: {type: String},
   	bank_code: {type: String},
   	account_name: {type: String},
   	account_number: {type: String},

   	user_alert    : {type: String, default: ''},
   	id_card       : {type: String, default: ''},

    device_id: {type: String, default: ''},
    eligible_loan_amounts: {type: [Number], default: [1000]}

   	//loans    : [{type: Schema.Types.ObjectId, required: true, ref: 'Loan'}]
   	//investments    : [{type: Schema.Types.ObjectId, required: true, ref: 'Investment'}]
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

userSchema.index({'$**': 'text'});

userSchema.post('remove', function(doc, next) {
    // 'this' is the client being removed. Provide callbacks here if you want
    console.log('Remove Doc', this._id);

    this.model('Loan').remove({user: this._id}).exec();
    this.model('Withdraw').remove({user: this._id}).exec();

    next();
});

module.exports = mongoose.model('User', userSchema);
