var express = require('express'),
	mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var investmentSchema = new Schema({

	type      : {type: String, required:true},
	code      : {type: Number, required:true, enum: [1, 2, 3, 4]},
	months    : {type: String, required: true}, //Duration
	amount    : {type: Number, required: true},
	total_interest  : {type: Number, required: true},
	monthly_interest   : {type: Number, required: true},
	rate      : {type: String, required: true},
	start     : {type: String, required: true},
	payback   : {type: Number, required: true},
	paydate   : {type: String, required: true},
	paidout   : {type: String, enum: ['0', '1'], default: '0'}, //Payment has been made to user's wallet
	user 	  : {type: Schema.Types.ObjectId, required: true, ref: 'User'}
});

module.exports = mongoose.model('Investment', investmentSchema);