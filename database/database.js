var express        = require('express');
var Datastore = require('nedb');


// Initialize nedb databases

//var User = new Datastore({filename:__dirname +'/data/User', autoload: true });
var User = require('./models/user');
var BVN = new Datastore({filename:__dirname +'/data/BVN', autoload: true });
var Card = new Datastore({filename:__dirname +'/data/Card', autoload: true });
//var Loan = new Datastore({filename:__dirname +'/data/Loan', autoload: true });
var Loan = require('./models/loan');
var Wallet = require('./models/wallet');
var Member = new Datastore({filename:__dirname +'/data/Member', autoload: true });
//var Investment = new Datastore({filename:__dirname +'/data/Investment', autoload: true });
var Investment = require('./models/investment');
var Transaction = new Datastore({filename:__dirname +'/data/Transaction', autoload: true });
var Cooperatives=new Datastore({filename:__dirname+'/data/Cooperative',autoload:true});
var CooperativeMembers=new Datastore({filename:__dirname+'/data/CooperativeMembers',autoload:true});
var Deposits=new Datastore({filename:__dirname+'/data/Deposits',autoload:true});
//var withdraw=new Datastore({filename:__dirname+'/data/withdraw',autoload:true});
var Withdraw = require('./models/withdraw');
var LoanApplications=new Datastore({filename: __dirname+'/data/Applications',autoload:true});
var Bank =new Datastore({filename: __dirname+'/data/Bank',autoload:true});

module.exports = {
    User: User,
    BVN: BVN,
    Bank: Bank,
    Wallet: Wallet,
    Card: Card,
    Loan: Loan,
    Member: Member,
    Investment: Investment,
    Transaction: Transaction,
    Cooperative: Cooperatives,
    CoopMembers: CooperativeMembers,
    Deposits:Deposits,
    Withdraw: Withdraw,
    LoanApplication:LoanApplications
};
