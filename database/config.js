
module.exports = {
	get staging() {
		return 'mongodb://127.0.0.1:27017/payconnectTest';
	}, 
	get production() {
		return 'mongodb://localhost:27017/payconnectLoan';
	}
};