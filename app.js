var express  = require('express');
var app      = express();
var passport = require('passport');
var argv = require('minimist')(process.argv.slice(2));
var bodyParser = require('body-parser');
var subpath = express();
let path = require('path');
let appDir = path.dirname(require.main.filename);

var mongoose = require('mongoose'),
    dbconfig = require('./database/config');

/*fs.open('./test-fs.txt', 'r', function(err, fd){

    let buf = new Buffer(1024);

    fs.read(fd, buf, 0, buf.length, 0, function(error, bytes){
        console.log(`data: ${buf.slice(0, bytes)}`);

        fs.close(fd, function(args){
            // body
        });
    });

});*/
mongoose.Promise = global.Promise

// APP_ENV is set by running "npm run local" command
mongoose.connect(process.env.APP_ENV === 'dev' ? dbconfig.staging : dbconfig.production, {useMongoClient: true}, function(err){
    if (err) {
        console.log('database connection error', err);
    } else {
        console.log('database connection successful');
    }
});

app.use(bodyParser.json());
app.use("/api", subpath);

var swagger = require('swagger-node-express').createNew(subpath);

app.use('/api', express.static(appDir + '/dist'));

// var port     = process.env.PORT || 80;
var port     = process.env.PORT || 3000;

require('./config')(app);
require('./app/auth/Auth')(passport);
require('./routes/routes')(app,passport);
require('./apis/Api')(app,passport);

swagger.setApiInfo({
    title: "Payconnect API",
    description: "API for mobile application calls",
    termsOfServiceUrl: "",
    contact: "itaukemeabasi@gmail.com",
    license: "",
    licenseUrl: ""
});

app.get('/api', function (req, res) {
    res.sendFile(__dirname + '/dist/index.html');
});

// Set api-doc path
swagger.configureSwaggerPaths('', 'api-docs', '');

// Configure the API domain
var domain = 'localhost';
if(argv.domain !== undefined)
    domain = argv.domain;
else
    console.log('No --domain=xxx specified, taking default hostname "localhost".');
// Configure the API port
if(argv.port !== undefined)
    port = argv.port;
else
    console.log('No --port=xxx specified, taking default port ' + port + '.');
// Set and display the application URL
var applicationUrl = 'http://' + domain + ':' + port;

console.log('Payconnect API Doc running on ' + applicationUrl+'/api');

swagger.configure(applicationUrl, '1.0.0');

// launch ======================================================================
app.listen(port);
console.log(`listening on ${domain}:${port}`);
