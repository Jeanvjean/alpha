//During the test the env letiable is set to test
process.env.NODE_ENV = 'test';

//let mongoose = require('mongoose').mongoose;
//let mongoose = new Mongoose();

let Mongoose = require('mongoose').Mongoose;
let mongoose = new Mongoose();
let mpromise = require('mpromise');
 
let mockgoose = require('mockgoose');

//let mongoose = require('mongoose'),
let Schema   = mongoose.Schema;

//let Mockgoose = require('mockgoose').Mockgoose;
//let mockgoose = new Mockgoose(mongoose);

let testDBConfig = 'mongodb://127.0.0.1:27017/TestingDB';
let connection, MockModel, queryHelperObj;

before(function(done){

    connection = mongoose.connect(testDBConfig, {useMongoClient: true}, function (err) {

        mongoose.connection.db.dropDatabase();

        if (err) {
            console.log('Test database connection error', err);
        } else {

            let modelSchema = new Schema({
                Id           : String,
                fieldA       : {type: String, required: true},
                fieldB       : {type: String},
                created_at   : String,
                updated_at   : String
            });

            // on every save, add the date
            modelSchema.pre('save', function(next) {

              // get the current date
              let currentDate = new Date();

              // change the updated_at field to current date
              this.updated_at = currentDate;

              // if created_at doesn't exist, add to that field
              if (!this.created_at)
                this.created_at = currentDate;

              next();
            });

            MockModel = mongoose.model('MockModel', modelSchema);

            console.log('Test database connection successful');
        }

        done(err);
    });

	//mockgoose(mongoose);	
});

after(function(done) {

	// Drop database after test
    mongoose.connection.db.dropDatabase();

    done();
});

let queryHelper = require('../utils/query-helper');

//let Playlist = require('../Database/Models/playlist');

//Require the dev-dependencies
let chai = require('chai');
//let chaiHttp = require('chai-http');
let should = chai.should();
let expect = chai.expect;

//chai.use(chaiHttp);

//Our parent block
describe('CRUD Helper', () => {

	let mockData = {
		fieldA: 'fieldA',
		fieldB: 'fieldB'
	};

	let obj_id, MockModelResult;

	/*
  	* Test the /GET route
  	*/
  	describe('queryHelper Test create.', () => {

	  	it('it creates a new record', (done) => {
			
	  		MockModelResult = queryHelper(MockModel).create(mockData, function (err, res) {

                obj_id = res._id;

	  			expect(!!res).to.equal(true);
	  			expect(err).to.equal(null);

	  			done();
	  		});
	  	});
  	});

  	/*
  	* Test data update
  	*/
  	describe('queryHelper Test update.', () => {

	  	it('it updates a record', (done) => {
			
			MockModelResult.then(function(obj) 
			{ 
				obj.fieldA = 'fieldA Update';
				obj.fieldB = 'fieldB Update';

		  		queryHelper(MockModel).update(obj._id, obj, function (err, res) {

		  			expect(res).to.equal(true);
	  				expect(err).to.equal(null);

                    done();
		  		});
			});
	  	});
  	});

    describe('queryHelper Test update by setting id.', () => {

        it('it updates a record, using the id() setter method', (done) => {

            MockModelResult.then(function(obj) {

                obj.fieldA = 'fieldA Update Again';
                obj.fieldB = 'fieldB Update Again';

                queryHelper(MockModel).id(obj._id).update(obj, function (err, res) {

                    expect(res).to.equal(true);
                    expect(err).to.equal(null);

                    done();
                });
            });
        });
    });

  	describe('queryHelper.getOne test', function (){

  		it('it should get one record', function (done){
			
  			MockModelResult = queryHelper(MockModel).getOne(obj_id, function (err, res){

  				expect(res.fieldA).to.equal('fieldA Update Again');
  				expect(res.fieldB).to.equal('fieldB Update Again');
	  			expect(err).to.equal(null);

	  			done();
  			});
  		});
  	});

  	describe('queryHelper.getOne projection test', function (){

  		it('it should get one record with fields omitted', function (done){

  			MockModelResult = queryHelper(MockModel).projection({fieldA: 0}).getOne(obj_id, function (err, res){

  				expect(res.fieldA).to.equal(undefined);
  				expect(res.fieldB).to.equal('fieldB Update Again');
	  			expect(err).to.equal(null);

	  			done();
  			});
  		});
  	});

  	describe('queryHelper.getMany test', function (){

  		it('it should get many records', function (done){
  			//Populate Database

    		queryHelper(MockModel).create(mockData)
                .then(function(){ //Add new record

    		        queryHelper(MockModel).create(mockData)
                        .then(function(){ //Add new record

    		  			    MockModelResult = queryHelper(MockModel).paginate(4).getMany(function (err, res){

        		  				expect(res.modelData).to.be.an('array');
        		  				expect(res.resultCount).to.equal(3);
        		  				expect(res.currentPage).to.equal(1);
        		  				expect(res.pageCount).to.equal(1);

        			  			expect(err).to.equal(null);

        			  			done();
        		  			});
    			        });
    		    }); 
  		});
  	});

    describe('queryHelper.getMany returns promise.', () => {
        it('it checks for return of promise object', (done) => {
        
            expect(MockModelResult instanceof mpromise).to.equal(true);

            done();
        });
    });

  	describe('queryHelper.getMany paginated test', function (){

  		it('it should 2 records per page', function (done){
  			//Populate Database
  			
  			MockModelResult = queryHelper(MockModel).paginate(2).getMany(function (err, res){

  				expect(res.modelData).to.be.an('array');
  				expect(res.resultCount).to.equal(2);
  				expect(res.currentPage).to.equal(1);
  				expect(res.pageCount).to.equal(2);

  				//expect(res.page).to.equal(1);
	  			expect(err).to.equal(null);

	  			done();
  			});
  		});
  	});

  	describe('queryHelper.getMany page test', function (){

  		it('it should records from page 2', function (done){
  			//Populate Database
  			
  			MockModelResult = queryHelper(MockModel).paginate(1).page(2).getMany(function (err, res){

  				expect(res.modelData).to.be.an('array');
  				expect(res.resultCount).to.equal(1);
  				expect(res.currentPage).to.equal(2);
  				expect(res.pageCount).to.equal(3);

  				//expect(res.page).to.equal(1);
	  			expect(err).to.equal(null);

	  			done();
  			});
  		});
  	});

  	describe('queryHelper delete returns promise.', () => {
	  	it('it deletes a record with specified _id', (done) => {
			
	  		MockModelResult = queryHelper(MockModel).delete(obj_id, function (err, res) {

	  			expect(res).to.equal(true);
  				expect(err).to.equal(null);

	  			queryHelper(MockModel).paginate(4).getMany(function (err, res){

	  				expect(res.modelData).to.be.an('array');
	  				expect(res.resultCount).to.equal(2); //After delete records should now be 2
	  				expect(res.currentPage).to.equal(1);
	  				expect(res.pageCount).to.equal(1);

	  				//expect(res.page).to.equal(1);
		  			expect(err).to.equal(null);

		  			done();
	  			});

	  		});
	  	});
  	});
    
    describe('queryHelper delete returns promise.', () => {
        it('it checks for return of promise object', (done) => {
        
            expect(MockModelResult instanceof mpromise).to.equal(true);

            done();
        });
    });
});