//During the test the env letiable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
let testDBConfig = 'mongodb://127.0.0.1:27017/TestingDB';
let connection;

let User = require('../database/database').User;
let investmodule = require('../app/investment/invest');

before(function(done){

	let Schema   = mongoose.Schema;

    connection = mongoose.connect(testDBConfig, {useMongoClient: true}, function (err) {

        mongoose.connection.db.dropDatabase();

        if (err) {
            console.log('Test database connection error', err);
        } else {
            console.log('Test database connection successful');
        }

        done();
    });

	//mockgoose(mongoose);	
});

after(function(done) {

	// Drop database after test
    mongoose.connection.db.dropDatabase();

    done();
});

//Require the dev-dependencies
let chai = require('chai');
//let chaiHttp = require('chai-http');
let should = chai.should();
let expect = chai.expect;

//chai.use(chaiHttp);

describe('Investment Test [investment]', () => {


	let obj_id, u, bal = 80000;


  	describe('Monthly Investment.', () => {

	  	it('it should make an monthly investment', (done) => {
			
			u = User.create({
				phone: '08137605681',
				email_status: '',
				phone_status: '',
				otp: '1234',
				balance: bal,
				pin: '1234',
				email: 'sobeo17@gmail.com',
				code: 1234,
				firstname: 'Darlington',
				lastname: 'Sobio',
				middlename: '',
				dob: new Date(),
				rebill_token   : '',
				transaction_id   : '',
			}, function (err, user) {
				
				loan_amount = 10000;
				loan_interest = 0.0325;
				loan_duration = 1; //In months

				investmodule(user._id, loan_amount, 'wallet').monthly().invest(function(status, data){

				    expect(status).to.equal(true);
				    expect(data.investment.months).to.equal('1 Month');
				    expect(data.investment.amount).to.equal(loan_amount);
				    expect(data.investment.code).to.equal(1);
				    expect(data.investment.rate).to.equal(`${loan_interest * 100}%`);
				    expect(data.investment.monthly_interest).to.equal(loan_amount * loan_interest);
				    expect(data.investment.total_interest).to.equal((loan_amount * loan_interest) * loan_duration);
				    expect(data.investment.payback).to.equal(((loan_amount * loan_interest) * loan_duration) + loan_amount);

				    User.findOne(user._id, function(err, user){

				    	expect(user.balance).to.equal(data.balance);
				    	expect(user.balance).to.equal(bal - loan_amount);

				    	bal = user.balance; //Update balance

				    	done();
				    });
				});
			});	  		
	  	});
  	});

  	describe('Quarterly Investment.', () => {

	  	it('it should make an quarterly investment', (done) => {
			
			u.then(function (user) {

				loan_amount = 14000;
				loan_interest = 0.0385;
				loan_duration = 3; //In months
				
				investmodule(user._id, loan_amount, 'wallet').quarterly().invest(function(status, data){

				    expect(status).to.equal(true);
				    expect(data.investment.months).to.equal('3 Months');
				    expect(data.investment.amount).to.equal(loan_amount);
				    expect(data.investment.code).to.equal(2);
				    expect(data.investment.rate).to.equal(`${loan_interest * 100}%`);
				    expect(data.investment.monthly_interest).to.equal(loan_amount * loan_interest);
				    expect(data.investment.total_interest).to.equal((loan_amount * loan_interest) * loan_duration);
				    expect(data.investment.payback).to.equal(((loan_amount * loan_interest) * loan_duration) + loan_amount);

				    User.findOne(user._id, function(err, user){

				    	expect(user.balance).to.equal(data.balance);
				    	expect(user.balance).to.equal(bal - loan_amount);

				    	bal = user.balance;

				    	done();
				    });
				});
			});	  		
	  	});
  	});

  	describe('Bi-Annual Investment.', () => {

	  	it('it should make an bi-annual investment', (done) => {
			
			u.then(function (user) {

				loan_amount = 30000;
				loan_interest = 0.05;
				loan_duration = 6; //In months
				
				investmodule(user._id, loan_amount, 'wallet').biannually().invest(function(status, data){

				    expect(status).to.equal(true);
				    expect(data.investment.months).to.equal('6 Months');
				    expect(data.investment.amount).to.equal(loan_amount);
				    expect(data.investment.code).to.equal(3);
				    expect(data.investment.rate).to.equal(`${loan_interest * 100}%`);
				    expect(data.investment.monthly_interest).to.equal(loan_amount * loan_interest);
				    expect(data.investment.total_interest).to.equal((loan_amount * loan_interest) * loan_duration);
				    expect(data.investment.payback).to.equal(((loan_amount * loan_interest) * loan_duration) + loan_amount);

				    User.findOne(user._id, function(err, user){

				    	expect(user.balance).to.equal(data.balance);
				    	expect(user.balance).to.equal(bal - loan_amount);

				    	bal = user.balance;

				    	done();
				    });
				});
			});	  		
	  	});
  	});

  	describe('Yearly Investment.', () => {

	  	it('it should make an yearly investment', (done) => {
			
			u.then(function (user) {

				loan_amount = 20000;
				loan_interest = 0.065;
				loan_duration = 12; //In months
				
				investmodule(user._id, loan_amount, 'wallet').annually().invest(function(status, data){

				    expect(status).to.equal(true);
				    expect(data.investment.months).to.equal('12 Months');
				    expect(data.investment.amount).to.equal(loan_amount);
				    expect(data.investment.code).to.equal(4);
				    expect(data.investment.rate).to.equal(`${loan_interest * 100}%`);
				    expect(data.investment.monthly_interest).to.equal(loan_amount * loan_interest);
				    expect(data.investment.total_interest).to.equal((loan_amount * loan_interest) * loan_duration);
				    expect(data.investment.payback).to.equal(((loan_amount * loan_interest) * loan_duration) + loan_amount);

				    User.findOne(user._id, function(err, user){

				    	expect(user.balance).to.equal(data.balance);
				    	expect(user.balance).to.equal(bal - loan_amount);

				    	bal = user.balance;

				    	done();
				    });
				});
			});	  		
	  	});
  	});

  	
  	let u2;

  	describe('Monthly Investment with .type() method.', () => {
  		
	  	it('it should make an monthly investment', (done) => {
			
			bal = 80000;// Reset balance

			u2 = User.create({
				phone: '08137605681',
				email_status: '',
				phone_status: '',
				otp: '1234',
				balance: bal,
				pin: '1234',
				email: 'sobeo17@gmail.com',
				code: 1234,
				firstname: 'Darlington',
				lastname: 'Sobio',
				middlename: '',
				dob: new Date(),
				rebill_token   : '',
				transaction_id   : '',
			}, function (err, user) {
				
				loan_amount = 10000;
				loan_interest = 0.0325;
				loan_duration = 1; //In months

				investmodule(user._id, loan_amount, 'wallet').type(1).invest(function(status, data){

				    expect(status).to.equal(true);
				    expect(data.investment.months).to.equal('1 Month');
				    expect(data.investment.amount).to.equal(loan_amount);
				    expect(data.investment.code).to.equal(1);
				    expect(data.investment.rate).to.equal(`${loan_interest * 100}%`);
				    expect(data.investment.monthly_interest).to.equal(loan_amount * loan_interest);
				    expect(data.investment.total_interest).to.equal((loan_amount * loan_interest) * loan_duration);
				    expect(data.investment.payback).to.equal(((loan_amount * loan_interest) * loan_duration) + loan_amount);

				    User.findOne(user._id, function(err, user){

				    	expect(user.balance).to.equal(data.balance);
				    	expect(user.balance).to.equal(bal - loan_amount);

				    	bal = user.balance; //Update balance

				    	done();
				    });
				});
			});	  		
	  	});
  	});

  	describe('Quarterly Investment with .type(2) method.', () => {

	  	it('it should make an quarterly investment', (done) => {
			
			u2.then(function (user) {

				loan_amount = 14000;
				loan_interest = 0.0385;
				loan_duration = 3; //In months
				
				investmodule(user._id, loan_amount, 'wallet').type(2).invest(function(status, data){

				    expect(status).to.equal(true);
				    expect(data.investment.months).to.equal('3 Months');
				    expect(data.investment.amount).to.equal(loan_amount);
				    expect(data.investment.code).to.equal(2);
				    expect(data.investment.rate).to.equal(`${loan_interest * 100}%`);
				    expect(data.investment.monthly_interest).to.equal(loan_amount * loan_interest);
				    expect(data.investment.total_interest).to.equal((loan_amount * loan_interest) * loan_duration);
				    expect(data.investment.payback).to.equal(((loan_amount * loan_interest) * loan_duration) + loan_amount);

				    User.findOne(user._id, function(err, user){

				    	expect(user.balance).to.equal(data.balance);
				    	expect(user.balance).to.equal(bal - loan_amount);

				    	bal = user.balance;

				    	done();
				    });
				});
			});	  		
	  	});
  	});

  	describe('Bi-Annual Investment .type(3) method', () => {

	  	it('it should make an bi-annual investment', (done) => {
			
			u2.then(function (user) {

				loan_amount = 30000;
				loan_interest = 0.05;
				loan_duration = 6; //In months
				
				investmodule(user._id, loan_amount, 'wallet').type(3).invest(function(status, data){

				    expect(status).to.equal(true);
				    expect(data.investment.months).to.equal('6 Months');
				    expect(data.investment.amount).to.equal(loan_amount);
				    expect(data.investment.code).to.equal(3);
				    expect(data.investment.rate).to.equal(`${loan_interest * 100}%`);
				    expect(data.investment.monthly_interest).to.equal(loan_amount * loan_interest);
				    expect(data.investment.total_interest).to.equal((loan_amount * loan_interest) * loan_duration);
				    expect(data.investment.payback).to.equal(((loan_amount * loan_interest) * loan_duration) + loan_amount);

				    User.findOne(user._id, function(err, user){

				    	expect(user.balance).to.equal(data.balance);
				    	expect(user.balance).to.equal(bal - loan_amount);

				    	bal = user.balance;

				    	done();
				    });
				});
			});	  		
	  	});
  	});

  	describe('Yearly Investment .type(4) method', () => {

	  	it('it should make an yearly investment', (done) => {
			
			u2.then(function (user) {

				loan_amount = 20000;
				loan_interest = 0.065;
				loan_duration = 12; //In months
				
				investmodule(user._id, loan_amount, 'wallet').type(4).invest(function(status, data){

				    expect(status).to.equal(true);
				    expect(data.investment.months).to.equal('12 Months');
				    expect(data.investment.amount).to.equal(loan_amount);
				    expect(data.investment.code).to.equal(4);
				    expect(data.investment.rate).to.equal(`${loan_interest * 100}%`);
				    expect(data.investment.monthly_interest).to.equal(loan_amount * loan_interest);
				    expect(data.investment.total_interest).to.equal((loan_amount * loan_interest) * loan_duration);
				    expect(data.investment.payback).to.equal(((loan_amount * loan_interest) * loan_duration) + loan_amount);

				    User.findOne(user._id, function(err, user){

				    	expect(user.balance).to.equal(data.balance);
				    	expect(user.balance).to.equal(bal - loan_amount);

				    	bal = user.balance;

				    	done();
				    });
				});
			});	  		
	  	});
  	});
});