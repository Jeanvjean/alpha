const amplifypay = require('../utils/amplifier.js').bank,
	  mocha = require('mocha'),

	  //Require the dev-dependencies
	  chaiHttp = require('chai-http'),

	  chai = require('chai'),
	  should = chai.should(),
	  expect = chai.expect;

var payload = {
    "transactionid": 'TSTER12332108-12234',
    "amount": '2000.00',
    "accountNumber": '0690000000',
    "bankCode": '044',
    "currency": "NGN",
    "narration": "Testing Bank Payment",
    "customerInfo": {
        "firstname": 'John',
        "lastname": 'Doe',
        "email": 'john@gmail.com',
        "phone": '08137605681',
        "address": "lagos",
        "dob": '',
        "bvn": ''
    }
};

/*
{ transactionId: 'TSTER12332108-12234',
  reference: 'AMP265',
  statusCode: '0',
  statusMsg: 'Pending OTP Validation' }
*/

describe('Amplifier.data().pay() Payment test', () => {

	it('Should return true', (done) => {

		amplifypay().staging().data(payload).pay(function(err, res){

            expect(err).to.equal(null);
			expect(res.statusCode).to.equal('0');

			done();
		});

	});
});