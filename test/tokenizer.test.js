//During the test the env letiable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
let testDBConfig = 'mongodb://127.0.0.1:27017/TestingDB';
let connection;

let queryHelper = require('../utils/query-helper');
let User = require('../database/database').User;
let Loan = require('../database/database').Loan;
let tokenizer = require('../app/payments/tokenizer');

before(function(done){

	let Schema   = mongoose.Schema;

    connection = mongoose.connect(testDBConfig, {useMongoClient: true}, function (err) {

        mongoose.connection.db.dropDatabase();

        if (err) {
            console.log('Test database connection error', err);
        } else {
            console.log('Test database connection successful');
        }

        done();
    });

	//mockgoose(mongoose);	
});

after(function(done) {

	// Drop database after test
    mongoose.connection.db.dropDatabase();

    done();
});

//Require the dev-dependencies
let chai = require('chai');
//let chaiHttp = require('chai-http');
let should = chai.should();
let expect = chai.expect;

//chai.use(chaiHttp);

describe('Tokenizer Test [tokennizer]', () => {


	let obj_id;

	/*
  	* Test the /GET route
  	*/
  	describe('Initiate Tokenization.', () => {

	  	it('it iniatiates a small payment for tokenization', (done) => {
			
			let u = User.create({
				phone: '08137605681',
				otp: '1234',
				balance: '2000',
				pin: '1234',
				email: 'sobeo17@gmail.com',
				code: 1234,
				firstname: 'Darlington',
				lastname: 'Sobio',
				middlename: '',
				dob: new Date()
			}).then(function (user) {

				obj_id = user._id;

				tokenizer.initiate(user._id, function(status, data) {

				    expect(status).to.equal(true);
				    expect(data.message).to.equal('Token saved');

				   done();
				});
				
			}).catch(function(err){
				console.log(err);

				done();
			});	  		
	  	});
  	});

  	describe('Complete Tokenization.', () => {

	  	it('it should fail as token authorisation does not work in staging', (done) => {
			
			tokenizer.complete(obj_id, '12345' /*OTP*/, function(status, data) {

			    expect(status).to.equal(true);
			    expect(data.message).to.equal('Token created');

			    done();
			});
	  	});
  	});
});