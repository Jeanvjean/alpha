User{
    id:,
    phone:,
    phone_status:(unverified | verified),
    email_status:(unverified | verified),
    password:,
    email:,
    bvn:,
    bvn_status:(validated),
    occupation: (0->Admin
                 1->Student
                 2->Employee
                 3->Biz man
                 4->N-power),
}

Loan{
    phone:,
    amount:,
    status:(pending|approved|declined),
    *tenure:,
    *repayment_left:,
    *payment_made:,
    *start_date:,
    *end_date:,
}

Investment{
    name:,
    code:,
    rate:,
    period:,
}

Transaction{
    card/phone:,
    amount:,
    status:,
    type:(debit|credit),
}

lateRepayment{
    endPoint:'/latePayment/penalty/:loanId',
    body: rate i.e, 0.05 = 5%, 0.50 = 50%
}
uploadScreenshot{
    endPoint:'/alert',
    parameters:loanId from body,screenshot:file
}
