var Payconnect = angular.module('Payconnect', ["ngRoute", 'ui.bootstrap', '720kb.datepicker', 'toastr']);

Payconnect.config(['$routeProvider',  function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: '../views/dashboard.html',
        controller: 'dashboardCtrl'
    })
/*    .when('/loans', {
        templateUrl: '../views/loan.html',
        controller: 'loanCtrl'
    })
    .when('/loans/:page', {
        templateUrl: '../views/loan.html',
        controller: 'loanCtrl'
    })*/
    .when('/loans/declined', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/declined/:page', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/approved', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/approved/:page', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/pending', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/pending/:page', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })

    .when('/loans/no-doc', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/no-doc/:page', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })

    .when('/loans/accepted', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/accepted/:page', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })

    .when('/loans/completed', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/completed/:page', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })

    .when('/loans/rejected', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/rejected/:page', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/outstanding', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })
    .when('/loans/outstanding/:page', {
        templateUrl: '../views/loans.html',
        controller: 'loanCtrl'
    })

    .when('/withdrawals/pending', {
        templateUrl: '../views/withdrawals.html',
        controller: 'withdrawalCtrl'
    })
    .when('/withdrawals/pending/:page', {
        templateUrl: '../views/withdrawals.html',
        controller: 'withdrawalCtrl'
    })

    .when('/withdrawals/paidout', {
        templateUrl: '../views/withdrawals.html',
        controller: 'withdrawalCtrl'
    })
    .when('/withdrawals/paidout/:page', {
        templateUrl: '../views/withdrawals.html',
        controller: 'withdrawalCtrl'
    })

    .when('/withdrawals/declined', {
        templateUrl: '../views/withdrawals.html',
        controller: 'withdrawalCtrl'
    })
    .when('/withdrawals/declined/:page', {
        templateUrl: '../views/withdrawals.html',
        controller: 'withdrawalCtrl'
    })

    .when('/card', {
        templateUrl: '../views/card.html',
        controller: 'cardCtrl'
    })
    .when('/members', {
        templateUrl: '../views/member.html',
        controller: 'memberCtrl'
    })
    .when('/members/:page', {
        templateUrl: '../views/member.html',
        controller: 'memberCtrl'
    })

    .when('/employers', {
        templateUrl: '../views/employers.html',
        controller: 'employerCtrl'
    })
    .when('/employers/:page', {
        templateUrl: '../views/employers.html',
        controller: 'employerCtrl'
    })

    .when('/investment', {
        templateUrl: '../views/investment.html',
        controller: 'investmentCtrl'
    })
    .when('/Borrowers', {
        templateUrl: '../views/Borrowers.html',
        controller: 'memberCtrl'
    })
    .when('/Lenders', {
        templateUrl: '../views/Lenders.html',
        controller: 'memberCtrl'
    })
    .when('/transaction', {
        templateUrl: '../views/transaction.html',
        controller: 'transactionCtrl'
    })
    .otherwise({
        redirectTo: '/'
    });
}]);

var loc = document.location,
	server_url =  loc.protocol + '//' + loc.host;

Payconnect.value('api', server_url);