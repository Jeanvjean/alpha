(function(angular){
    angular.module('Payconnect')
    .controller('withdrawalCtrl', function(api, $uibModal, $scope, $routeParams, $http, $location) {

        var requestPath = '/withdrawals/';

        var currentPage = ($routeParams.page || 1);

        $scope.success = $scope.error = "";

        if ($location.path().search('withdrawals/pending') !== -1)
        {
            requestPath += 'pending/';
            $scope.pageName = 'Pending Withdrawals';
        }
        else if($location.path().search('withdrawals/paidout') !== -1)
        {
            requestPath += 'paidout/';
            $scope.pageName = 'Paidout Withdrawals';
        }
        else if($location.path().search('withdrawals/declined') !== -1)
        {
            requestPath += 'declined/';
            $scope.pageName = 'Declined Withdrawals';
        }

        console.log($location.path(), api + requestPath + currentPage);

        $http({
            method: 'GET',
            url: api + requestPath + currentPage
        }).success(function(data) {
            if (data.status === true)
            {
                $scope.withdrawals = data.data;

                $scope.currentPage = data.currentPage;
                $scope.totalItems = data.totalItems;
                $scope.itemsPerpage = data.itemsPerpage;
                $scope.rowId = (data.resultCount * data.currentPage);
            }
        }).error(function(error) {
            $scope.error = data.message;
        });

        $scope.pageChanged = function() {
            console.log('Page changed to: ' + $scope.currentPage);
            $scope.params.page = $scope.currentPage;
            $location.url('loans/' + $scope.currentPage);
            //$scope.getLoans($scope.currentPage);
        };

        $scope.process = function(phone, status) {
            $http({
                method: 'GET',
                url: api + '/loan/process/' + phone + '/' + status
            }).success(function(data) {
                if (data.status === true) {
                    window.location.reload();
                    $scope.success = data.message;
                } else {
                    $scope.error = data.message;
                }
            }).error(function(error) {});
        };

        $scope.open_warning = function(withdrawal) {

        	$scope.withdrawal = withdrawal;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/warning.html',
                scope: $scope,
                controller: ProcessCtrl,
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {});
        };
    });

    function ProcessCtrl(api, $scope, $http, toastr, $uibModalInstance) {

        $scope.approve = function() {

            $http.post(api + '/processwith', {
                w_id: $scope.withdrawal._id
            }).success(function(data) {

                if (data.status === true) {
                    toastr.success(data.message, 'Success!');

                    $scope.withdrawal.payout = data.payout;

                    $uibModalInstance.dismiss("cancel");
                } else {
                    toastr.error(data.message, 'Error!');
                    $uibModalInstance.dismiss("cancel");
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.decline = function() {

            $http.post(api + '/declinewithdrawal', {
                w_id: $scope.withdrawal._id
            }).success(function(data) {

                if (data.status === true) {
                    toastr.success(data.message, 'Success!');

                    $scope.withdrawal.payout = data.payout;

                    $uibModalInstance.dismiss("cancel");
                } else {
                    toastr.error(data.message, 'Error!');
                    $uibModalInstance.dismiss("cancel");
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss("cancel");
        };
    }
})(window.angular);
