Payconnect.controller('loanCtrl', function(api, $scope, $rootScope, $http, toastr, $uibModal, $routeParams) {

    $scope.params = $routeParams;


    $scope.success = $scope.error = "";
    $http({
        method: 'GET',
        url: api + '/getLoans/' + ($scope.params.page || 1)
    }).success(function(data) {
        if (data.status === true) {
            console.log(data);
            $scope.loans = data.data;
        } else {
            $scope.error = data.message;
        }
    }).error(function(error) {
        $scope.error = data.message;
    });

    $scope.open_loan = function(member, loan_id, index) {
        $rootScope.user = member;
        $rootScope.index = index;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/create_loan.html',
            controller: 'UpdateLoanModalCtrl',
            size: '',
            resolve: {
                member: function() {
                    return member;
                },
                index: function() {
                    return index;
                },
                loanId: function() {
                    return loan_id;
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem;
        }, function() {});
    };

    $scope.open_charge = function(member, index) {
        $rootScope.user = member;
        $rootScope.index = index;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/charge_loan.html',
            controller: 'ModalCtrl',
            size: '',
            resolve: {
                member: function() {
                    return member;
                },
                index: function() {
                    return index;
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem;
        }, function() {});
    };

    $scope.open_doc = function(v) {
        $rootScope.id_card = v.id_card;
        $rootScope.user_alert = v.user_alert;
        $rootScope.loan = v;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/doc.html',
            controller: 'LoanDocModalCtrl',
            size: '',
            resolve: {
                items: function() {
                    return $scope.items;
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem;
        }, function() {});
    };

    $scope.open_upload = function(loan_id, i) {
        $rootScope.index = i;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/document.html',
            controller: 'ModalCtrl',
            size: 'lg',
            resolve: {
                items: function() {
                    return $scope.items;
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem;
        }, function() {});
    };
}).controller('UpdateLoanModalCtrl', function(api, $rootScope, $scope, $http, toastr, $uibModalInstance, loanId) {

    $scope.Submit = function(Form_data) {
        Form_data.phone = $rootScope.user.phone;
        Form_data.loan_id = loanId;
        //$scope.member = $rootScope.user;
        Form_data.occupation = "1";
        var url = api + '/updateLoan';
        $http.post(url, Form_data).success(function(data) {
            if (data.status === true) {
                toastr.success(data.message, 'Success!');
                $uibModalInstance.dismiss("cancel");
            } else {
                toastr.error(data.message, 'Error!');
                $uibModalInstance.dismiss("cancel");
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };
}).controller('LoanDocModalCtrl', function(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {
    
    $scope.decline_message = '';

    $scope.Approve = function(loan_id) {

        $http.post(api + '/Approveloan', {
            loan_id: loan_id
        }).success(function(data) {

            if (data.status === true) {
                toastr.success(data.message, 'Success!');
                $timeout(function() {
                    window.location.href = EndPoint + '/dashboard#/loan';
                }, 1000);
            } else {
                toastr.error(data.message, 'Error!');
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

    $scope.Decline = function(loan_id) {

        $http.post(api + '/Declineloan', {
            loan_id: loan_id,
            decline_message: $scope.decline_message
        }).success(function(data) {

            if (data.status === true) {
                toastr.success(data.message, 'Success!');
                $timeout(function() {
                    window.location.href = EndPoint + '/dashboard#/loan';
                }, 1000);
            } else {
                toastr.error(data.message, 'Error!');
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

}).controller('ModalCtrl', function(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {
    $scope.Charge = function(charge) {
        charge.phone = $rootScope.user.phone;
        $http.post(api + '/Charge', charge).success(function(data) {

            if (data.status === true) {
                toastr.success(data.message, 'Success!');
                $timeout(function() {
                    window.location.href = EndPoint + '/dashboard#/loan';
                }, 1000);
            } else {
                toastr.error(data.message, 'Error!');
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss("cancel");
    };
});