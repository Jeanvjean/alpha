Payconnect.controller('cardCtrl', ['api', '$scope', '$http', function(api, $scope, $http) {

    $scope.success = $scope.error = "";
    $http({method: 'GET', url: api+'/getCards'})
        .success( function( data )
        {
            if (data.status == true){
                $scope.cards = data.data;
            } else {
                $scope.error = data.message;
            }
        })
        .error( function( error )
        {
            $scope.error = data.message;
        });

}]);