Payconnect.controller('transactionCtrl', ['api', '$scope', '$http', function(api, $scope, $http) {

    $scope.success = $scope.error = "";

    $http({method: 'GET', url: api+'/getTransactions'})
        .success( function( data )
        {
            if (data.status == true){
                $scope.transactions = data.data;
            } else {
                $scope.error = data.message;
            }
        })
        .error( function( error )
        {
            $scope.error = data.message;
        });

    $scope.process = function (phone, status) {
        $http({method: 'GET', url: api+'/loan/process/'+phone+'/'+status})
            .success( function( data )
            {
                if (data.status == true){
                    window.location.reload();
                    $scope.success = data.message;
                } else {
                    $scope.error = data.message;
                }
            })
            .error( function( error )
            {
                $scope.error = data.message;
            });
    }
}]);