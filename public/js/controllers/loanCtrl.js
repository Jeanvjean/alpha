(function(angular){

angular.module('Payconnect')
.controller('loanCtrl', function(api, $scope, $rootScope, $http, toastr, $uibModal, $routeParams, $location) {

    //$scope.params = $routeParams;
    //$scope.search = '';

    var requestPath = '/loans/';

    var currentPage = ($routeParams.page || 1);

    $scope.success = $scope.error = "";

    if ($location.path().search('loans/pending') !== -1)
    {
    	$scope.loan_status = 'pending';
        requestPath += 'pending/';
        $scope.pageName = 'Pending Loans Requests';
    }
    else if($location.path().search('loans/approved') !== -1)
    {
    	$scope.loan_status = 'approved';
        requestPath += 'approved/';
        $scope.pageName = 'Approved Loans Requests';
    }
    else if($location.path().search('loans/declined') !== -1)
    {
    	$scope.loan_status = 'declined';
        requestPath += 'declined/';
        $scope.pageName = 'Declined Loans Requests';
    }
    else if($location.path().search('loans/accepted') !== -1)
    {
    	$scope.loan_status = 'accepted';
        requestPath += 'accepted/';
        $scope.pageName = 'Accepted Loans Requests';
    }
    else if($location.path().search('loans/rejected') !== -1)
    {
    	$scope.loan_status = 'rejected';
        requestPath += 'rejected/';
        $scope.pageName = 'Rejected Loans Requests';
    }
    else if($location.path().search('loans/outstanding') !== -1)
    {
    	$scope.loan_status = 'outstanding';
        requestPath += 'outstanding/';
        $scope.pageName = 'Outstanding Loans Requests';
    }
    else if($location.path().search('loans/no-doc') !== -1)
    {
    	$scope.loan_status = 'no-doc';
        requestPath += 'no-doc/';
        $scope.pageName = 'Pending Loans With No Docs Requests';
    }
    else if($location.path().search('loans/completed') !== -1)
    {
    	$scope.loan_status = 'completed';
        requestPath += 'completed/';
        $scope.pageName = 'Completed (Repaid) Loans';
    }
    else
    {
    	$scope.loan_status = 'pending';
        requestPath += 'pending/';
        $scope.pageName = 'Pending Loans Requests';
    }

    $scope.getLoans = function(currentPage){

        $http({
            method: 'GET',
            url: api + requestPath + (currentPage || $routeParams.page),
            params: {q: $scope.search}
        }).success(function(data) {
            // console.log(data)
            if (data.status === true) {

                $scope.loans = data.loans;

                $scope.currentPage = data.currentPage;
                $scope.totalItems = data.totalItems;
                $scope.itemsPerPage = data.itemsPerPage;
                $scope.rowId = (data.resultCount * data.currentPage);

            } else {
                $scope.error = data.message;
            }
        }).error(function(error) {
            $scope.error = data.message;
        });
    };

    $scope.getLoans(currentPage);

    $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);

        $location.url(requestPath + $scope.currentPage);
    };

    $scope.searchQueryChange = function() {

        $scope.getLoans(1);
        console.log("message");
    };


    $http({
        method: 'GET',
        url: api + '/employers'
    }).success(function(data) {
        // console.log(data)
        if (data.status === true)
        {
            $scope.employers = data.data;

        } else {
            $scope.employers = [];
        }
    }).error(function(error) {
        $scope.error = data.message;
    });

    $scope.editMode = false;
    $scope.maritalStatus = ['Single', 'Married', 'Divorced', 'Widowed'];
    $scope.qualification = ['School Cert', 'NCE', 'OND', 'HND', 'BSC', 'MSC'];
    $scope.dependants = ['2 and Below', '3-4', '5 and Above'];
    $scope.experience = ['0-2', '2-4', '5 and Above'];
    $scope.residency = ['Rented', 'Owned', 'Family house', 'Employer provided', 'Temporary', 'Squating'];

	$scope.states = [
	    'Abia',
	    'Adamawa',
	    'Akwa Ibom',
	    'Anambra',
	    'Bauchi',
	    'Bayelsa',
	    'Benue',
	    'Borno',
	    'CrossRivers',
	    'Delta',
	    'Ebonyi',
	    'Edo',
	    'Ekiti',
	    'Enugu',
	    'Gombe',
	    'Imo',
	    'Jigawa',
	    'Kaduna',
	    'Kano',
	    'Katsina',
	    'Kebbi',
	    'Kogi',
	    'Kwara',
	    'Lagos',
	    'Nasarawa',
	    'Niger',
	    'Ogun',
	    'Ondo',
	    'Osun',
	    'Oyo',
	    'Plateau',
	    'Rivers',
	    'Sokoto',
	    'Taraba',
	    'Yobe',
	    'Zamafara',
	];

    $scope.open_loan = function(loan) {

        $scope.loan = loan;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/create_loan.html',
            controller: 'UpdateLoanModalCtrl',
            size: '',
            scope: $scope
        });
    };

    $scope.open_charge = function(loan) {

        $scope.loan = loan;
        $scope.payment_amount = loan.repayment_left;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/modals/charge_loan.html',
            controller: 'RepayLoanModalCtrl',
            scope: $scope,
            size: ''
        });
    };
    $scope.open_late = function(loan) {

        $scope.loan = loan;
        $scope.payment_amount = loan.repayment_left;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/modals/loan_latepen.html',
            controller: 'RepayLoanModalCtrl',
            scope: $scope,
            size: ''
        });
    };
    $scope.block = function(user) {
        $http.post(api + '/blockuser', {
            user_id: user._id
        }).success(function(data) {

            if (data.status == true) {
                toastr.success(data.message, 'Success!');

                user.account_status = 'blocked';

            } else {
                toastr.error(data.message, 'Error!');
            }

        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };
    $scope.delete = function(user) {
        $http.post(api + '/user/delete', {
            user_id: user._id
        }).success(function(data) {

            if (data.status == true) {
                toastr.success(data.message, 'Success!');

                $scope.getUsers();

                $uibModalInstance.dismiss("cancel");

            } else {
                toastr.error(data.message, 'Error!');
            }

        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

    $scope.open_doc = function(loan) {
        $scope.id_card = loan.user.id_card;
        $scope.user_alert = loan.user.user_alert;
        $scope.loan = loan;
        $scope.user = loan.user;
        $scope.decline_message = '';

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/doc.html',
            controller: 'LoanDocModalCtrl',
            scope: $scope,
            size: ''
        });
    };

    $scope.open_upload = function(loan) {

    	$scope.loan = loan;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/document.html',
            //controller: 'ModalCtrl',
            scope: $scope,
            size: 'lg'
        });
    };

    $scope.bulkMessage = function() {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/modals/bulk-loan-message.html',
            controller: MessageCtrl,
            size: '',
            scope: $scope
        });
    };

    $scope.message = function(user) {

        $scope.msg_user_id = user._id;
        $scope.msg_firstname = user.firstname;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/modals/message.html',
            controller: MessageCtrl,
            size: '',
            scope: $scope,
            resolve: {
                items: function() {
                    return $scope.items;
                }
            }
        });
    };

    $scope.deleteLoan = function(loan_id) {

        $scope.loan_id = loan_id;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/modals/delete-loan.html',
            controller: 'DeleteLoanModalCtrl',
            size: 'sm',
            scope: $scope
        });
    };
}).controller('UpdateLoanModalCtrl', function(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

    $scope.Submit = function(Form_data) {
        Form_data.loan_id = $scope.loan._id;

        var url = api + '/updateLoan';
        $http.post(url, Form_data).success(function(data) {
            if (data.status === true) {

                $scope.loan.end_date = data.data.end_date;
                $scope.loan.repayment_left = data.data.repayment_left;
                $scope.loan.amount = data.data.amount;

                toastr.success(data.message, 'Success!');
                $uibModalInstance.dismiss("cancel");
            } else {
                toastr.error(data.message, 'Error!');
                $uibModalInstance.dismiss("cancel");
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };
}).controller('DeleteLoanModalCtrl', function(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

    $scope.delete = function() {
        $http.post(api + '/user/loan/delete', {
            loan_id: $scope.loan_id
        }).success(function(data) {

            if (data.status == true) {
                toastr.success(data.message, 'Success!');

                $scope.getLoans();

                $uibModalInstance.dismiss("cancel");

            } else {
                toastr.error(data.message, 'Error!');
            }

        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };
}).controller('LoanDocModalCtrl', function(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

    $scope.updateUserData = function ()
    {
    	//console.log('User Data: ', $scope.user);
    	$http.post(api + '/user/update', {
    	    user: $scope.user
    	}).success(function(data) {

    	    if (data.status === true) {
    	        toastr.success(data.message, 'User data updated!');
    	    } else {
    	        toastr.error(data.message, 'Error updating data!');
    	    }
    	}).error(function(data) {
    	    toastr.error('error in connection', 'Error!');
    	});
    }

    $scope.Approve = function() {

        $http.post(api + '/Approveloan', {
            loan_id: $scope.loan._id
        }).success(function(data) {

            if (data.status === true) {
                toastr.success(data.message, 'Success!');

                $scope.loan.status = 'approved';

                $scope.getLoans(currentPage);
            } else {
                toastr.error(data.message, 'Error!');
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

    $scope.Decline = function() {

        $http.post(api + '/Declineloan', {
            loan_id: $scope.loan._id,
            decline_message: $scope.decline_message
        }).success(function(data) {

            if (data.status === true) {
                toastr.success(data.message, 'Success!');

                $scope.loan.status = 'declined';

                $scope.getLoans(currentPage);

            } else {
                toastr.error(data.message, 'Error!');
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

}).controller('RepayLoanModalCtrl', function(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

    $scope.charge = function(loan) {

        $http.post(api + '/loan/repayment/' + loan._id, {amount: $scope.payment_amount}).success(function(data) {

            if (data.status === true) {
                toastr.success(data.message, 'Success!');

                loan.repayment_left -= $scope.payment_amount;
                loan.payment_made += $scope.payment_amount;
            } else {
                toastr.error(data.message, 'Error!');
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };
    $scope.increase = function(Form_data) {
        // console.log(Form_data)
        const loanId = Form_data._id
        const rate = $scope.rate
        // console.log(loanId, rate)
        $http.post(api + '/latePayment/penalty/',{loanId:loanId,rate:rate}).success(function(data) {
            console.log(data)
            if (data.success === true) {
                toastr.success(data.message, 'Success!');
            } else {
                toastr.error(data.message, 'Error!');
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss("cancel");
    };
});

function MessageCtrl(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

	$scope.bulkMsg = '';
	$scope.bulkEmailTitle = 'Payconnect';
	$scope.bulkEmail = true;
	$scope.bulkSMS = false;

    $scope.userMsg = '';
    $scope.userEmailTitle = 'Payconnect';
    $scope.userEmail = true;
    $scope.userSMS = false;

    $scope.sendMessage = function() {

    	if (!$scope.userEmail && !$scope.userSMS)
    	{
    		toastr.error('Please select message type', 'Error!');
    	}

        $http.post(api + '/user/send-message', {
            message: $scope.userMsg,
            title: $scope.userEmailTitle,
            send_email: $scope.userEmail,
            send_sms: $scope.userSMS,
            user_id: $scope.msg_user_id
        }).success(function(data) {

            if (data.status == true) {
                toastr.success(data.message, 'Success!');
            } else {
                toastr.error(data.message, 'Error!');
            }

        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

    $scope.sendBulkMessage = function()
    {
    	if (!$scope.bulkEmail && !$scope.bulkSMS)
    	{
    		toastr.error('Please select message type', 'Error!');
    	}

        $http.post(api + '/loans/send-message', {
            message: $scope.bulkMsg,
            title: $scope.bulkEmailTitle,
            send_email: $scope.bulkEmail,
            send_sms: $scope.bulkSMS,
            loan_status: $scope.loan_status
        }).success(function(data) {

            if (data.status == true)
            {
                toastr.success(data.message, 'Success!');

            } else {
                toastr.error(data.message, 'Error!');
            }
        }).error(function(data) {
            toastr.error('error in connection', 'Error!');
        });
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss("cancel");
    };
}
})(window.angular);
