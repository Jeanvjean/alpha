(function(angular){
    angular.module('Payconnect')
    .controller('investmentCtrl', ['api', '$scope', '$http', function(api, $scope, $http) {

        $scope.success = $scope.error = "";
        $http({method: 'GET', url: api+'/getInvestments'})
            .success( function( data )
            {
                if (data.status == true){
                    $scope.investments = data.data;
                } else {
                    $scope.error = data.message;
                }
            })
            .error( function( error )
            {
                $scope.error = data.message;
            });

        $scope.addInvestment = function (investment) {
            $scope.success = $scope.error = "";
            $http({
                method : 'POST',
                url : api+'/createInvestment',
                data : investment
            })
                .success(function (response){
                    if(response.status == true){
                        $scope.investments.push(investment);
                        $("#add_investment").modal("hide");
                        $scope.success = response.message;
                    }
                    else{
                        $scope.error = response.message;
                    }
                })
                .error(function  (response) {
                    $scope.error = response.message;
                });
        };

        $scope.modalRate = function (investment) {
            $scope.set = investment;
            $("#set_rate").modal("show");
        };

        $scope.setRate = function (set_rate) {
            $scope.success = $scope.error = "";
            set_rate.code = $scope.set.code;
            $http({
                method : 'POST',
                url : api+'/setRate',
                data : set_rate
            })
            .success(function (response){
                if(response.status == true){
                    $("#set_rate").modal("hide");
                    $scope.success = response.message;
                }
                else{
                    $scope.error = response.message;
                }
            })
            .error(function  (response) {
                $scope.error = response.message;
            });
        };
    }]);
})(window.angular);
