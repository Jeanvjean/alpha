Payconnect.controller('declinedLoansCtrl', function(api, $scope, $rootScope, $http, toastr, $uibModal, $routeParams, $location) {

    $scope.params = $routeParams;


    $scope.success = $scope.error = "";

    var currentPage = ($scope.params.page || 1);

    $scope.getLoans = function(currentPage){

        $http({
            method: 'GET',
            url: api + '/loans/declined/' + currentPage
        }).success(function(data) {
            if (data.status === true) {
                console.log(data);
                $scope.loans = data.data;

                $scope.currentPage = data.currentPage;
                $scope.totalItems = data.totalItems;
                $scope.itemsPerpage = data.itemsPerpage;
                $scope.rowId = (data.resultCount * data.currentPage);

            } else {
                $scope.error = data.message;
            }
        }).error(function(error) {
            $scope.error = data.message;
        });
    };

    $scope.getLoans(currentPage);

    $scope.openDeclineReason = function(decline_message) {

    	$scope.decline_message = decline_message;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: api + '/views/modals/decline-message.html',
            scope: $scope
        });

        $scope.close = function () {
       		modalInstance.close();
      	};
    };


    $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);
        $scope.params.page = $scope.currentPage;
        $location.url('declined-loans/' + $scope.currentPage);
        //$scope.getLoans($scope.currentPage);
    };
});