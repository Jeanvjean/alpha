Payconnect.controller('memberCtrl', function(api, $scope, $http, $uibModal, $rootScope) {

    $scope.success = $scope.error = "";
    $http({method: 'GET', url: api+'/members'})
        .success( function( data )
        {
             console.log(data);
            if (data.status == true){

                $rootScope.members = data.data;
            } else {
                $scope.error = data.message;
            }
        })
        .error( function( error )
        {
            $scope.error = "error in connectivity";
        });





      $scope.open_more = function (v,i) {

      $rootScope.phone    = v;
      $rootScope.index = i;
    
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: api + '/views/more.html',
      controller: 'ProcessCtrl',
      size: '',
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {

    });

  };

  $scope.open_upload = function (v,i) {

      $rootScope.phone    = v;
      $rootScope.index = i;
    
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: api + '/views/document.html',
      controller: 'ProcessCtrl',
      size: 'lg',
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {

    });

  };

}).controller('ProcessCtrl', function(api, $rootScope, $scope, $http,toastr, $rootScope, $uibModalInstance) {


$scope.Submit =  function(Form_data){

     Form_data.phone = $rootScope.phone;
     index           = $rootScope.index;

     console.log(Form_data);

    $http.post(api+'/updateProfile/loan',Form_data)
      .success(function (Data) {

        if (Data.status == true) {
           
           toastr.success(Data.message, 'Success!');
           $rootScope.members.splice(index,1);
           $rootScope.members.push(Form_data);
           $uibModalInstance.dismiss("cancel");

        } else {

          toastr.error(Data.message, 'Error!');
          $uibModalInstance.dismiss("cancel");
        }
      })
      .error(function (data) {
       toastr.error('error in connection', 'Error!');

      });
  }







        $.getJSON("/js/Json/State.json", function(json) {

            $("#association-state").prop('disabled', false);
            $scope.states = json.results;

            $scope.states.sort(function(a, b) {
                var nameA = a.name.toUpperCase(); // ignore upper and lowercase
                var nameB = b.name.toUpperCase(); // ignore upper and lowercase
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }

                return 0;
            });
        });


        $scope.getLga = function(state) {

            console.log(state)
            var identify = $.grep($scope.states, function(e) {

                return (e.name) == state;
            });

            $.getJSON("/js/Json/LGA.json", function(json) {

                $("#association-lga").prop('disabled', false);
                $scope.lgas = $.grep(json.results, function(e) {

                    return (e.state.objectId) == identify[0].objectId;
                });

                $scope.lgas.sort(function(a, b) {
                    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }

                    return 0;
                });


            });


        };



      
         $http({method: 'GET', url: api+'/nig-banks'})
        .success( function( data )
        {
             console.log(data);
            if (data.status == true){

                $scope.Banks = data.data;
            } else {
                $scope.error = "error";
            }
        })
        .error( function( error )
        {
            $scope.error = "error";
        });






$scope.cancel = function(){

  $uibModalInstance.dismiss("cancel");
}

});