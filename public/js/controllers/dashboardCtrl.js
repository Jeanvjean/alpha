(function(angular){
    angular.module('Payconnect')
    .controller('dashboardCtrl', function(api, $uibModal, $rootScope, $scope, $http) {
        $scope.success = $scope.error = "";
/*        $http({
            method: 'GET',
            url: api + '/getInvestments'
        }).success(function(data) {
            if (data.status === true) {
                $scope.investments = data.data;
                if (data.data[0] !== undefined) {
                    $scope.invest_amount = 0;
                    (data.data).forEach(function(invest) {
                        $scope.invest_amount += parseInt(invest.amount);
                    });
                } else {
                    $scope.loan_total = 0;
                }
            } else {
                $scope.error = data.message;
            }
        }).error(function(error) {});*/

/*        $http({
            method: 'GET',
            url: api + '/getCards'
        }).success(function(data) {
            if (data.status === true) {
                $scope.cards = data.data;
            } else {
                $scope.error = data.message;
            }
        }).error(function(error) {});*/

/*        $http({
            method: 'GET',
            url: api + '/members'
        }).success(function(data) {
            if (data.status === true) {
                $scope.members = data.data;
            } else {
                $scope.error = data.message;
            }
        }).error(function(error) {});*/

/*        $http({
            method: 'GET',
            url: api + '/getTransactions'
        }).success(function(data) {
            if (data.status === true) {
                if (data.data[0] !== undefined) {
                    $scope.transactions = data.data;
                } else {
                    $scope.transactions = 0;
                }
            } else {
                $scope.error = data.message;
            }
        }).error(function(error) {});*/

        //$scope.transactions = 0;
        $scope.investment_count = 0;
        $scope.member_count = 0;
        $scope.loan_count = 0;
        $scope.total_loan_amount = $scope.total_invest_amount = 0.00;

        $http({
            method: 'GET',
            url: api + '/dashboard/data'
        }).success(function(data) {
            if (data.status === true) {

                $scope.loan_count = data.data.loanCount;
                $scope.member_count = data.data.memberCount;
                $scope.investment_count = data.data.investmentCount;
                $scope.total_loan_amount = data.data.totalLoanAmount;
                $scope.total_invest_amount = data.data.totalInvestmentAmount;

            } else {
                $scope.error = data.message;
            }
        }).error(function(error) {});

        $scope.process = function(phone, status) {
            $http({
                method: 'GET',
                url: api + '/loan/process/' + phone + '/' + status
            }).success(function(data) {
                // console.log(data)

                if (data.status === true)
                {
                    window.location.reload();
                    $scope.success = data.message;

                } else {
                    $scope.error = data.message;
                }
            }).error(function(error) {});
        };

        $scope.getWithdrawals = function() {

            $http({
                method: 'GET',
                url: api + '/allwithdraw'
            }).success(function(data) {
                console.log(data.data)
                if (data.status === true) {
                    $scope.withdrawal = data.data;
                }
            }).error(function(error) {
                $scope.error = data.message;
            });

        };
        // $scope.message = function(user) {
        //
        //     $scope.msg_user_id = user._id;
        //     $scope.msg_firstname = user.firstname;
        //
        //     var modalInstance = $uibModal.open({
        //         animation: $scope.animationsEnabled,
        //         templateUrl: api + '/views/modals/message.html',
        //         controller: MessageCtrl,
        //         size: '',
        //         scope: $scope,
        //         resolve: {
        //             items: function() {
        //                 return $scope.items;
        //             }
        //         }
        //     });
        // };
        $scope.getWithdrawals();

        $scope.open_doc = function(data) {
            $scope.id_card = data.user.id_card;
            $scope.user_alert = data.user.user_alert;
            $scope.loan = data.loan;
            $scope.user = data.user;
            $scope.decline_message = '';

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/doc.html',
                controller: 'LoanDocModalCtrl',
                scope: $scope,
                size: ''
            });
        };

        $scope.open_warning = function(withdrawal) {

        	$scope.withdrawal = withdrawal;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/warning.html',
                scope: $scope,
                controller: ProcessCtrl,
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {});
        };
        $scope.message = function(user) {

        	$scope.msg_user_id = user._id;
            $scope.msg_firstname = user.firstname;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/modals/message.html',
                scope: $scope,
                controller: MessageCtrl,
                size: 'lg',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {});
        };
    });

    function ProcessCtrl(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

        $scope.approve = function() {

            $http.post(api + '/processwith', {
                w_id: $scope.withdrawal._id
            }).success(function(data) {

                if (data.status === true) {
                    toastr.success(data.message, 'Success!');

                    $scope.withdrawal.payout_status = data.payout_status;

                    $scope.getWithdrawals();

                    //$rootScope.withdrawal.splice(index, 1);
                    $uibModalInstance.dismiss("cancel");
                } else {
                    toastr.error(data.message, 'Error!');
                    $uibModalInstance.dismiss("cancel");
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.decline = function() {

            $http.post(api + '/declinewithdrawal', {
                w_id: $scope.withdrawal._id
            }).success(function(data) {

                if (data.status === true) {
                    toastr.success(data.message, 'Success!');

                    $scope.withdrawal.payout_status = data.payout_status;

                    $scope.getWithdrawals();

                    //$rootScope.withdrawal.splice(index, 1);
                    $uibModalInstance.dismiss("cancel");
                } else {
                    toastr.error(data.message, 'Error!');
                    $uibModalInstance.dismiss("cancel");
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss("cancel");
        };
    }
    function MessageCtrl(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

    	$scope.bulkMsg = '';
    	$scope.bulkEmailTitle = 'Payconnect';
    	$scope.bulkEmail = true;
    	$scope.bulkSMS = false;

        $scope.userMsg = '';
        $scope.userEmailTitle = 'Payconnect';
        $scope.userEmail = true;
        $scope.userSMS = false;

        $scope.sendMessage = function() {

        	if (!$scope.userEmail && !$scope.userSMS)
        	{
        		toastr.error('Please select message type', 'Error!');
        	}

            $http.post(api + '/user/send-message', {
                message: $scope.userMsg,
                title: $scope.userEmailTitle,
                send_email: $scope.userEmail,
                send_sms: $scope.userSMS,
                user_id: $scope.msg_user_id
            }).success(function(data) {

                if (data.status == true) {
                    toastr.success(data.message, 'Success!');
                } else {
                    toastr.error(data.message, 'Error!');
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.sendBulkMessage = function()
        {
        	if (!$scope.bulkEmail && !$scope.bulkSMS)
        	{
        		toastr.error('Please select message type', 'Error!');
        	}

            $http.post(api + '/loans/send-message', {
                message: $scope.bulkMsg,
                title: $scope.bulkEmailTitle,
                send_email: $scope.bulkEmail,
                send_sms: $scope.bulkSMS,
                loan_status: $scope.loan_status
            }).success(function(data) {

                if (data.status == true)
                {
                    toastr.success(data.message, 'Success!');

                } else {
                    toastr.error(data.message, 'Error!');
                }
            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss("cancel");
        };
    }
})(window.angular);
