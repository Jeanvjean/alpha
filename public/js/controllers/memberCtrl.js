(function(angular){
    angular.module('Payconnect')
    .controller('memberCtrl', function(api, $scope, $http, $uibModal, $rootScope, $routeParams, $location) {

        $scope.success = $scope.error = "";

        var currentPage = ($routeParams.page || 1);

        $scope.getUsers = function (currentPage){
        	$http({
        	    method: 'GET',
        	    url: api + '/members/' + (currentPage || $routeParams.page),
        	    params: {q: $scope.search}
        	}).success(function(data) {

        	    console.log(data);
        	    if (data.status === true)
        	    {

        	        $scope.members = data.users;

        	        $scope.currentPage = data.currentPage;
        	        $scope.totalItems = data.totalItems;
        	        $scope.itemsPerPage = data.itemsPerPage;
        	        $scope.rowId = (data.resultCount * data.currentPage);

        	    } else {
        	        $scope.error = data.message;
        	    }
        	}).error(function(error) {
        	    $scope.error = "error in connectivity";
        	});
        }

        $scope.getUsers(currentPage);

        $scope.pageChanged = function() {
            console.log('Page changed to: ' + $scope.currentPage);

            $location.url('/members/' + $scope.currentPage);
        };

        $scope.searchQueryChange = function() {

            $scope.getUsers(1);
            console.log("message");
        };

        $scope.bulkMessage = function() {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/modals/bulk-message.html',
                controller: MessageCtrl,
                size: ''
            });
        };

        $scope.message = function(user) {

            $scope.msg_user_id = user._id;
            $scope.msg_firstname = user.firstname;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/modals/message.html',
                controller: MessageCtrl,
                size: '',
                scope: $scope,
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });
        };

        $scope.open_doc = function(v) {
            $rootScope.id_card = v.id_card;
            $rootScope.user_alert = v.user_alert;
            $rootScope.loan = v;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/doc.html',
                controller: 'ModalCtrl',
                size: '',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });
            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {});
        };

        $scope.accountAction = function(user) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/account-action.html',
                controller: 'AccountActionCtrl',
                size: 'sm',
                scope: $scope,
                resolve: {
                    user: function () {
                    	return user
                    }
                }
            });
        };

        $scope.deleteUser = function(user) {

            $scope.user_id = user._id;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/modals/delete-user.html',
                controller: 'AccountActionCtrl',
                size: 'sm',
                scope: $scope,
                resolve: {
                    user: function () {
                    	return user
                    }
                }
            });
        };

        $scope.openDetails = function(user) {

            $scope.id_card = user.id_card;
            $scope.user_alert = user.user_alert;
            $scope.user = user;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/modals/user-details.html',
                controller: 'AccountActionCtrl',
                scope: $scope,
                size: '',
                resolve: {
                    user: function () {
                    	return user
                    }
                }
            });
        };

        $http({
            method: 'GET',
            url: api + '/employers'
        }).success(function(data) {
            if (data.status === true)
            {
                $scope.employers = data.data;

            } else {
                $scope.employers = [];
            }
        }).error(function(error) {
            $scope.error = data.message;
        });

        $scope.editMode = false;
        $scope.maritalStatus = ['Single', 'Married', 'Divorced', 'Widowed'];
        $scope.qualification = ['School Cert', 'NCE', 'OND', 'HND', 'BSC', 'MSC'];
        $scope.dependants = ['2 and Below', '3-4', '5 and Above'];
        $scope.experience = ['0-2', '2-4', '5 and Above'];
        $scope.residency = ['Rented', 'Owned', 'Family house', 'Employer provided', 'Temporary', 'Squating'];

    	$scope.states = [
    	    'Abia',
    	    'Adamawa',
    	    'Akwa Ibom',
    	    'Anambra',
    	    'Bauchi',
    	    'Bayelsa',
    	    'Benue',
    	    'Borno',
    	    'CrossRivers',
    	    'Delta',
    	    'Ebonyi',
    	    'Edo',
    	    'Ekiti',
    	    'Enugu',
    	    'Gombe',
    	    'Imo',
    	    'Jigawa',
    	    'Kaduna',
    	    'Kano',
    	    'Katsina',
    	    'Kebbi',
    	    'Kogi',
    	    'Kwara',
    	    'Lagos',
    	    'Nasarawa',
    	    'Niger',
    	    'Ogun',
    	    'Ondo',
    	    'Osun',
    	    'Oyo',
    	    'Plateau',
    	    'Rivers',
    	    'Sokoto',
    	    'Taraba',
    	    'Yobe',
    	    'Zamafara',
    	];
    })
    .controller('AccountActionCtrl', function (api, $rootScope, $scope, $http, toastr, $uibModalInstance, user) {

        $scope.activate = function() {

            $http.post(api + '/activateuser', {
                user_id: user._id
            }).success(function(data) {

                if (data.status == true) {
                    toastr.success(data.message, 'Success!');

                    user.account_status = 'active';

                } else {
                    toastr.error(data.message, 'Error!');
                }
            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });

        };

        $scope.block = function() {
            $http.post(api + '/blockuser', {
                user_id: user._id
            }).success(function(data) {

                if (data.status == true) {
                    toastr.success(data.message, 'Success!');

                    user.account_status = 'blocked';

                } else {
                    toastr.error(data.message, 'Error!');
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.delete = function() {
            $http.post(api + '/user/delete', {
                user_id: $scope.user_id
            }).success(function(data) {

                if (data.status == true) {
                    toastr.success(data.message, 'Success!');

                    $scope.getUsers();

                    $uibModalInstance.dismiss("cancel");

                } else {
                    toastr.error(data.message, 'Error!');
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.updateUserData = function ()
        {
        	$http.post(api + '/user/update', {
        	    user: user
        	}).success(function(data) {

        	    if (data.status === true) {
        	        toastr.success(data.message, 'User data updated!');
        	    } else {
        	        toastr.error(data.message, 'Error updating data!');
        	    }
        	}).error(function(data) {
        	    toastr.error('error in connection', 'Error!');
        	});
        }
    })

    function ProcessCtrl(api, $rootScope, $scope, $http, toastr, $uibModalInstance, member, index) {
        $scope.member = member;

        $scope.Submit = function(Form_data) {
            Form_data.occupation = "1";
            var url = "";
            if ($scope.member.loan_status) {
                url = api + '/updateLoan';
            } else {
                url = api + '/createLoan';
            }
            $http.post(url, Form_data).success(function(Data) {
                if (Data.status == true) {
                    toastr.success(Data.message, 'Success!');
                    $uibModalInstance.dismiss("cancel");
                } else {
                    toastr.error(Data.message, 'Error!');
                    $uibModalInstance.dismiss("cancel");
                }
            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.Approve = function(phone) {
            $http.post(api + '/Approveloan', {
                phone: phone
            }).success(function(Data) {
                if (Data.status == true) {
                    toastr.success(Data.message, 'Success!');
                    $timeout(function() {
                        window.location.href = EndPoint + '/dashboard#/loan';
                    }, 1000);
                } else {
                    toastr.error(Data.message, 'Error!');
                }
            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.Decline = function(phone) {
            $http.post(api + '/Declineloan', {
                phone: phone
            }).success(function(Data) {

                if (Data.status == true) {
                    toastr.success(Data.message, 'Success!');
                    $timeout(function() {
                        window.location.href = EndPoint + '/dashboard#/loan';
                    }, 1000);
                } else {
                    toastr.error(Data.message, 'Error!');
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss("cancel");
        };
    }

    function MessageCtrl(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

        $scope.bulkMsg = '';
        $scope.bulkEmailTitle = 'Payconnect';
        $scope.bulkEmail = true;
        $scope.bulkSMS = false;

        $scope.userMsg = '';
        $scope.userEmailTitle = 'Payconnect';
        $scope.userEmail = true;
        $scope.userSMS = false;

        $scope.sendBulkMessage = function()
        {
        	if (!$scope.bulkEmail && !$scope.bulkSMS)
        	{
        		toastr.error('Please select message type', 'Error!');
        	}

            $http.post(api + '/users/send-message', {
                message: $scope.bulkMsg,
                title: $scope.bulkEmailTitle,
                send_email: $scope.bulkEmail,
                send_sms: $scope.bulkSMS,
            }).success(function(data) {

                if (data.status == true)
                {
                    toastr.success(data.message, 'Success!');

                } else {
                    toastr.error(data.message, 'Error!');
                }
            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.sendMessage = function() {

        	if (!$scope.userEmail && !$scope.userSMS)
        	{
        		toastr.error('Please select message type', 'Error!');
        	}

            $http.post(api + '/user/send-message', {
                message: $scope.userMsg,
                title: $scope.userEmailTitle,
                send_email: $scope.userEmail,
                send_sms: $scope.userSMS,
                user_id: $scope.msg_user_id
            }).success(function(data) {

                if (data.status == true) {
                    toastr.success(data.message, 'Success!');
                } else {
                    toastr.error(data.message, 'Error!');
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss("cancel");
        };
    }
})(window.angular);
