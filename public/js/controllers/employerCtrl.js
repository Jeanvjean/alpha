(function(angular){
    angular.module('Payconnect')
    .controller('employerCtrl', function(api, $scope, $http, $uibModal, $rootScope, $routeParams, $location) {

        $scope.success = $scope.error = "";

        var currentPage = ($routeParams.page || 1);

        $scope.getEmployers = function (currentPage) {

        	$http({
        	    method: 'GET',
        	    url: api + '/admin/employers/'+ (currentPage || $routeParams.page)
        	}).success(function(data) {

        	    // console.log(data);
        	    if (data.status === true)
        	    {

        	        $scope.employers = data.employers;

        	        $scope.currentPage = data.currentPage;
        	        $scope.totalItems = data.totalItems;
        	        $scope.itemsPerPage = data.itemsPerPage;
        	        $scope.rowId = (data.resultCount * data.currentPage);

        	    } else {
        	        $scope.error = data.message;
        	    }
        	}).error(function(error) {
        	    $scope.error = "error in connectivity";
        	});
        };

        $scope.getEmployers(currentPage);

        $scope.pageChanged = function() {
            console.log('Page changed to: ' + $scope.currentPage);

            $location.url('/employers/' + $scope.currentPage);
        };

        $scope.employerName = '';

        $scope.newEmployer = function() {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/modals/new-employer.html',
                controller: EmployerModalCtrl,
                size: '',
                scope: $scope
            });
        };

        $scope.editEmployer = function(employer) {

            $scope.employer = employer;
            $scope.employer_id = employer._id;
            $scope.employerName = employer.employer_name;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/modals/edit-employer.html',
                controller: EmployerModalCtrl,
                size: '',
                scope: $scope
            });
        };

        $scope.open_doc = function(v) {
            $rootScope.id_card = v.id_card;
            $rootScope.user_alert = v.user_alert;
            $rootScope.loan = v;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/doc.html',
                controller: 'ModalCtrl',
                size: '',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });
            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {});
        };

        $scope.employerAction = function(employer_id) {

            $scope.employer_id = employer_id;

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: api + '/views/modals/employer-action.html',
                controller: 'EmployerActionCtrl',
                size: 'sm',
                scope: $scope
            });
            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {});
        };
    })
    .controller('EmployerActionCtrl', function (api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

        $scope.delete = function() {
            $http.post(api + '/admin/employer/delete', {
                employer_id: $scope.employer_id
            }).success(function(data) {

                if (data.status == true) {
                    toastr.success(data.message, 'Success!');

                    $scope.pageChanged();

                    $uibModalInstance.dismiss("cancel");

                } else {
                    toastr.error(data.message, 'Error!');
                }

            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss("cancel");
        };
    })

    function EmployerModalCtrl(api, $rootScope, $scope, $http, toastr, $uibModalInstance) {

        $scope.addEmployer = function()
        {
        	if (!$scope.employerName)
        	{
        		toastr.error('Please enter employer name', 'Error!');

        		return;
        	}

            $http.post(api + '/employers/add', {
                employer_name: $scope.employerName
            }).success(function(data) {

                if (data.status == true)
                {
                    toastr.success(data.message, 'Success!');

                    $scope.getEmployers();

                } else {
                    toastr.error(data.message, 'Error!');
                }
            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.updateEmployer = function()
        {
        	if (!$scope.employerName)
        	{
        		toastr.error('Please enter employer name', 'Error!');

        		return;
        	}

            $http.post(api + '/employers/update', {
            	employer_id: $scope.employer_id,
                employer_name: $scope.employerName
            }).success(function(data) {

                if (data.status == true)
                {
                	$scope.employer.employer_name = $scope.employerName;

                    toastr.success(data.message, 'Success!');

                } else {
                    toastr.error(data.message, 'Error!');
                }
            }).error(function(data) {
                toastr.error('error in connection', 'Error!');
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss("cancel");
        };
    }
})(window.angular);
