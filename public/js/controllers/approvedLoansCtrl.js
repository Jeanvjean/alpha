Payconnect.controller('approvedLoansCtrl', function(api, $scope, $rootScope, $http, toastr, $uibModal, $routeParams, $location) {

    $scope.params = $routeParams;


    $scope.success = $scope.error = "";

    var currentPage = ($scope.params.page || 1);

    $scope.getLoans = function(currentPage){

        $http({
            method: 'GET',
            url: api + '/loans/approved/' + currentPage
        }).success(function(data) {
            if (data.status === true) {
                console.log(data);
                $scope.loans = data.data;

                $scope.currentPage = data.currentPage;
                $scope.totalItems = data.totalItems;
                $scope.itemsPerpage = data.itemsPerpage;
                $scope.rowId = (data.resultCount * data.currentPage);

            } else {
                $scope.error = data.message;
            }
        }).error(function(error) {
            $scope.error = data.message;
        });
    };

    $scope.getLoans(currentPage);

    $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);
        $scope.params.page = $scope.currentPage;

        $location.url('approved-loans/' + $scope.currentPage);
        //$scope.getLoans($scope.currentPage);
    };
});