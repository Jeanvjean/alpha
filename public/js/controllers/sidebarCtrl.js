/**
 * Created by Ukemeabasi on 04/11/2016.
 */
Payconnect.controller('sidebarCtrl', function($scope, api, $http, $location) {

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };

    $('ul.gn-menu > li > .sub-menu-title').on('click', function(e){
    	e.stopPropagation();

    	$(this).next().slideToggle(300);
    });
});