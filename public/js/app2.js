var Payconnect = angular.module('Payconnect', ["ngRoute",'ui.bootstrap','720kb.datepicker','toastr']);

Payconnect.config(['$routeProvider',  function ($routeProvider) {

    $routeProvider.
    when('/', {
        templateUrl: '../views/back_home.html',
        controller: 'memberCtrl'
    }).
    when('/loan', {
        templateUrl: '../views/loan.html',
        controller: 'loanCtrl'
    }).
    otherwise({
        redirectTo: '/'
    });
}]);

 Payconnect.value('api','http://localhost:8080');
//Payconnect.value('api','http://46.101.83.13:8080');
