Dropzone.options.myDropzone = {
  init: function() {
    var self = this;
    var Endpoint = 'http://localhost:8000';
    // config
    self.options.addRemoveLinks = true;
    self.options.dictRemoveFile = "Delete";

    // load already saved files
    $.get(Endpoint+'/upload', function(data) {
      var files = JSON.parse(data).files;
      for (var i = 0; i < files.length; i++) {
             
   console.log(files[i]);

        var mockFile = {
          name: files[i].name,
          size: files[i].size,
          type: 'image/jpeg'
        };


        self.options.addedfile.call(self, mockFile);
        self.options.thumbnail.call(self, mockFile, 'https://img.clipartfest.com/96f0927516610a77a5f4ea1235e43fef_image-result-for-adobe-acrobat-adobe-acrobat-clipart_300-300.jpeg');

      };

    });

    // bind events

    //New file added
    self.on("addedfile", function(file) {
      console.log('new file added ', file);
    });

    // Send file starts
    self.on("sending", function(file) {
      console.log('upload started', file);
      $('.meter').show();
    });

    // File upload Progress
    self.on("totaluploadprogress", function(progress) {
      console.log("progress ", progress);
      $('.roller').width(progress + '%');
    });

    self.on("queuecomplete", function(progress) {
      $('.meter').delay(999).slideUp(999);
    });

    // On removing file
    self.on("removedfile", function(file) {
      console.log(file);
      $.ajax({
        url: Endpoint+'/uploads/mou/' + file.name,
        type: 'DELETE',
        success: function(result) {
          console.log(result);
        }
      });
    });

  }
};
