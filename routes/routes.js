const cors = require('cors'),
    express = require('express'),
    app = express(),
    passport = require('passport'),
    bcrypt = require('bcrypt-nodejs'),
    request = require('request'),
    db = require('../database/database'),
    // multer = require('multer'),
    superagent = require('superagent'),
    withdrawmodule = require('../app/withdraw/withdraw'),
    loanmodule = require('../app/loan/loan'),
    usermodule = require('../app/user/users.js'),
    employermodule = require('../app/employer/employers.js'),
    moment = require('moment')

    path = require('path'),
    mime = require('mime'),
    fs = require('fs'),
    queryHelper = require('../utils/query-helper'),
    //Models
    User = db.User,
    Bvn = db.BVN,
    Bank = db.Bank,
    Card = db.Card,
    Loan = db.Loan,
    Member = db.Member,
    Investment = db.Investment,
    Transaction = db.Transaction,
    Cooperative = db.Cooperative,
    withdraw = db.withdraw,
    ////////////////////////
    async = require('async'),
    constant = require('../utils/constants'),
    functions = require('../utils/functions'),
    Flutterwave = require('flutterwave'),
    flutterwave = new Flutterwave(constant.flutterwave.api_key, constant.flutterwave.merchant_key),
    Async = require('async');

module.exports = function(app, passport) {
    app.get('/', function(req, res) {
        if (req.isAuthenticated()) {
            res.redirect('/dashboard');
        } else {
            res.redirect('/login');
        }
    });

    app.get('/login', function(req, res) {
        res.render('index', {
            info: req.flash('info')
        });
    });

    app.get('/payconnect/:Id', function(req, res) {
        var Id = req.params.Id;
        Loan.findOne({
            _id: Id
        }, function(err, loan) {
            if (loan) {
                User.findOne({
                    phone: loan.phone
                }, function(err, user) {
                    res.render('pay', {
                        name: loan.name,
                        email: user.email,
                        phone: user.phone,
                        Id: Id
                    });
                });
            } else {
                res.send('access declined');
            }
        });
    });

    app.get('/cron', function(req, res) {
        Investment.find({}, function(err, invest) {
            invest.forEach(function(data) {
                if (data.paydate == functions.Create()) {
                    console.log('done');
                    User.update({
                        phone: data.phone
                    }, {
                        $inc: {
                            balance: +data.payback
                        }
                    });

                    invest.paidout = '1';

                    invest.save();

                    /*Investment.remove({
                        reference: data.reference
                    }, function(err, num) {});*/
                } else {
                    console.log("none");
                }
            });
            res.send("today");
        });
    });

    app.get('/paysuccess/:Id', function(req, res) {
        var Id = req.params.Id;
        Loan.findOne({
            _id: Id
        }, function(err, loan) {
            if (loan) {
                User.findOne({
                    phone: loan.phone
                }, function(err, user) {
                    res.render('fastpay', {
                        name: loan.name,
                        email: user.email,
                        phone: user.phone
                    });
                });
            }
        });
    });

    app.get('/payfailed/:Id', function(req, res) {
        var Id = req.params.Id;
        Loan.findOne({
            _id: Id
        }, function(err, loan) {
            if (loan) {
                User.findOne({
                    phone: loan.phone
                }, function(err, user) {
                    res.render("Failed", {
                        name: loan.name,
                        email: user.email,
                        phone: user.phone
                    });
                });
            }
        });
    });

    app.post('/fastpay', function(req, res) {
        console.log(req.body.Ref);
        var confirm = functions.Confirm(req.body.Ref);
        console.log(confirm);
        //  if (confirm) {
        User.findOne({
            phone: req.body.phone
        }, function(err, user) {
            console.log(user);
            payload = req.body;
            payload.status = "pending";
            payload.amount = user.Balance;
            User.update({
                phone: req.body.phone
            }, {
                $set: {
                    Balance: 0,
                    ref: req.body.Ref
                }
            });
            withdraw.create(req.body);
            payload.template = "withdraw";
            payload.subject = "Loan Withdrawal";
            payload.email = user.email;
            payload.name = user.firstname + " " + user.lastname;
            functions.EmailCert(payload);
            res.send({
                status: true,
                message: "Loan withdraw process initiated"
            });
        });
        //  }
    });

    app.get('/dashboard', isLoggedIn, function(req, res) {
        if (req.user.email == 'hello@payconnect.ng') {
            res.render('dashboard');
        } else if (req.user.email == 'admin@payconnect.ng') {
            res.render('backoffice');
        }else if (req.user.email == 'admin@alphacredit.ng') {
            res.render('dashboard')
        } else {
            res.render('agent');
        }
    });

    app.get('/dashboard/data', isLoggedIn, function(req, res) {

        var multi_queries = {

            loanCount: function (callback) {
                Loan.count(callback);
            },

            memberCount: function (callback) {
                Loan.count(callback);
            },

            investmentCount: function (callback) {
                Investment.count(callback);
            },

            totalLoanAmount: function (callback) {

                Loan.aggregate([{
                    $group : {
                        _id : null,
                        total : {
                            $sum : "$amount"
                        }
                    }
                }], callback);
            },
            totalInvestmentAmount: function (callback) {

                Investment.aggregate([{
                    $group : {
                        _id : null,
                        total : {
                            $sum : "$amount"
                        }
                    }
                }], callback);
            }
        };

        Async.parallel(multi_queries, function (err, results) {

            if (err)
                throw err;

            //results holds the data in object form
            console.log(results);

            res.json({
                status: true,
                data: {
                    loanCount: results.loanCount,
                    memberCount: results.memberCount,
                    investmentCount: results.investmentCount,
                    totalLoanAmount: results.totalLoanAmount[0] ? results.totalLoanAmount[0].total : 0.00,
                    totalInvestmentAmount: results.totalInvestmentAmount[0] ? results.totalInvestmentAmount[0].total : 0.00,
                },
                message: ""
            });
        });
    });

    app.get('/user/profile/:phone', function(req, res) {
        var phone = req.params.phone;
        User.findOne({
            'phone': phone
        }, function(err, user) {
            if (user) {
                res.json({
                    status: true,
                    data: user,
                    message: "User logged in successfully"
                });
            } else {
                res.json({
                    status: false,
                    message: "User not logged in"
                });
            }
        });
    });

    app.post('/login', function(req, res) {
        User.findOne({
            'phone': req.body.phone
        }, function(err, user) {

            if (err) res.json({
                status: false,
                message: "An error occurred"
            });

            if (!user) {
                res.json({
                    status: false,
                    message: "Phone number does not exist"
                });
            } else {
                if (user.pin) {
                    if (bcrypt.compareSync(req.body.pin, user.pin)) {

                        if (user.phone_status !== "verified") {
                            res.json({
                                status: false,
                                message: "Log in declined! Phone number not verified"
                            });
                        }
                        else if (user.account_status === "blocked")
                        {
                            res.json({
                                status: false,
                                message: "Your account has been blocked"
                            });
                        }
                        else {
                            res.json({
                                status: true,
                                data: user,
                                message: "User logged in successfully"
                            });
                        }
                    } else {
                        res.json({
                            status: false,
                            message: "Pin is incorrect"
                        });
                    }
                } else {
                    res.json({
                        status: false,
                        message: "Pin not created for account"
                    });
                }
            }
        });
    });

    app.post('/web/login', passport.authenticate('local-login', {
        successRedirect: '/dashboard',
        failureRedirect: '/login',
        failureFlash: true
    }));

    app.post('/web/signup', passport.authenticate('local-signup', {
        successRedirect: '/signup/true',
        failureRedirect: '/signup/false',
        failureFlash: true
    }));

    app.get('/signup/:status', function(req, res) {
        var status = req.params.status;
        if (status == 'true') {
            res.json({
                status: true,
                data: req.user,
                message: "User created successfully"
            });
        } else if (status == 'false') {
            res.json({
                status: false,
                message: "phone number already exist"
            });
        } else {
            res.json({
                status: false,
                message: "An error occurred!"
            });
        }
    });

    app.get('/phone/send_otp/:phone', function(req, res) {
        var phone = req.params.phone;
        if (phone) {
            User.findOne({
                'phone': phone
            }, function(err, user) {
                if (!user) {
                    res.json({
                        status: false,
                        message: "User does not exist"
                    });
                } else {
                    if (user.phone_status == "verified") {
                        res.json({
                            status: false,
                            message: phone + " is already verified"
                        });
                    } else {
                        //Send OTP SMS
                        if (user.otp) {

                             functions.SMS(user.phone, user.otp);

                               res.json({
                            status: true,
                            data: {
                                otp: otp
                            },
                            message: "OTP sent to " + phone
                        });

                        }else{

                        var otp = Math.round((Math.random() * 10000));
                        console.log(otp, "otp");
                        functions.SMS(user.phone, otp);

                        User.update({
                            'phone': user.phone
                        }, {
                            $set: {
                                otp: otp
                            }
                        });
                        res.json({
                            status: true,
                            data: {
                                otp: otp
                            },
                            message: "OTP sent to " + phone
                        });

                        }

                    }
                }
            });
        } else {
            res.json({
                status: false,
                message: "Invalid phone number"
            });
        }
    });

    app.get('/phone/get_status/:phone', function(req, res) {
        var phone = req.params.phone;
        if (phone) {
            User.findOne({
                'phone': phone
            }, function(err, user) {
                if (!user) {
                    res.json({
                        status: false,
                        message: "User does not exist"
                    });
                } else {
                    if (user.phone_status == "verified") {
                        res.json({
                            status: true,
                            message: phone + " is verified"
                        });
                    } else {
                        res.json({
                            status: false,
                            message: phone + " is not verified"
                        });
                    }
                }
            });
        } else {
            res.json({
                status: false,
                message: "Invalid phone number"
            });
        }
    });

    app.post('/signup/confirmPhone', function(req, res) {
        var phone = req.body.phone;
        var otp = req.body.otp;
        console.log(otp)
        User.findOne({
            'phone': phone
        }, function(err, user) {
            if (user) {
                if (user.otp == otp) {
                    User.update({
                        'phone': phone
                    }, {
                        $set: {
                            phone_status: "verified"
                        }
                    }, function(err, response) {
                        if (err === null) {
                            res.json({
                                status: true,
                                message: "Phone No. confirmed successfully"
                            });
                        } else {
                            res.json({
                                status: false,
                                message: "Error updating profile"
                            });
                        }
                    });
                } else {
                    res.json({
                        status: false,
                        message: "Invalid OTP"
                    });
                }
            } else {
                res.json({
                    status: false,
                    message: "User does not exist"
                });
            }
        });
    });

    app.post('/signup/confirmEmail', function(req, res) {
        var phone = req.body.phone;
        var code = req.body.code;
        User.findOne({'phone': phone}, function(err, user) {
            if (user) {
                if (user.code == code) {
                    User.update({
                        'phone': phone
                    }, {
                        $set: {
                            email_status: "verified"
                        }
                    }, function(err, response) {
                        if (err === null) {
                            res.json({
                                status: true,
                                message: "Email confirmed successfully"
                            });
                        } else {
                            res.json({
                                status: false,
                                message: "Error updating profile"
                            });
                        }
                    });
                } else {
                    res.json({
                        status: false,
                        message: "Invalid Code"
                    });
                }
            } else {
                res.json({
                    status: false,
                    message: "User does not exist"
                });
            }
        });
    });

    app.post('/signup',async (req,res)=>{
        try {
            var user = req.body
            var otp = Math.round((Math.random() * 10000));
            var u = await User.findOne({phone:user.phone})
            console.log(otp)
            if (u) {
                if (u.phone_status == "unverified") {
                    if (u.otp != "") {
                        console.log('user was found and has otp')
                        functions.SMS(user.phone, u.otp);
                        res.json({status: true,message: "OTP sent successfully"});
                    }else {
                        console.log('user found and has no otp')
                        u.otp = otp
                        await u.save()
                        functions.SMS(user.phone,otp)
                        res.json({status: true,message: "OTP sent successfully"});
                    }
                }else {
                    res.json({status: false,message: "Phone number is already registered",data: u });
                }
            }else {
                const newUser = new User({
                    phone:user.phone,
                    otp:otp,
                    balance:0.00,
                    email_status: "unverified",
                    phone_status:"unverified"
                })
                await newUser.save()
                functions.SMS(user.phone, otp);
                res.json({status: true,message: "OTP sent successfully"});
                }
        } catch (e) {
            res.json({status: false,message: "an error occured try again"});
        }
    })

    // app.post('/signup', function(req, res) {
    //
    //     var user = req.body;
    //
    //     User.findOne({'phone': user.phone}, function(err, user_obj) {
    //         var otp = Math.round((Math.random() * 10000));
    //         console.log(otp)
    //         if (user_obj){
    //             if (user_obj.phone_status === "unverified"){
    //                 user_obj.otp = otp;
    //                 user_obj.save(function(err, udata){
    //                     if (err === null){
    //                         console.log('Account unverified and otp resent.' + otp);
    //
    //                         functions.SMS(user.phone, otp);
    //
    //                         res.json({status: true,message: "OTP sent successfully"});
    //                     }else{
    //                         res.json({status: false,message: "There was a problem sending otp"});
    //                     }
    //                 });
    //             }
    //             else
    //             {
    //                 res.json({
    //                     status: false,
    //                     message: "Phone number is already registered",
    //                     data: user_obj
    //                 });
    //             }
    //         } else {
    //
    //             let udata = {};
    //
    //             udata.phone_status = udata.email_status = "unverified";
    //
    //             udata.phone = user.phone;
    //             udata.otp = otp;
    //             udata.balance = 0.00;
    //
    //             User.create(udata, function(err, user){
    //
    //                 if (err === null)
    //                 {
    //                     console.log(otp, "otp sent");
    //                     functions.SMS(user.phone, otp);
    //
    //                     res.json({
    //                         status: true,
    //                         message: "OTP sent successfully"
    //                     });
    //                 }
    //                 else
    //                 {
    //                     res.json({
    //                         status: false,
    //                         message: "There was a problem sending otp"
    //                     });
    //                 }
    //             });
    //         }
    //     });
    // });

    app.post('/createPin', function(req, res) {
        var user = req.body;

        var pin = bcrypt.hashSync(user.pin);

        User.update({
            'phone': user.phone
        }, {pin: pin}, function(err, user){
            if(err === null)
            {
                res.json({
                    status: true,
                    message: "Pin created successfully"
                });
            }
            else
            {
                res.json({
                    status: false,
                    message: "There was a problem creating pin."
                });
            }
        });
    });

    app.post('/updateEmail', async function(req, res) {
        try {
            var user = req.body,
                email = user.email;
            var code = Math.round((Math.random() * 1000000));
            console.log(code)
            const u = await User.findOne({phone:user.phone})
            // console.log(u)
            if (u.code != null) {
                console.log('user has a code' + u.code)
                functions.Email(email,u.code)
                res.json({status: true,message: "Email awaiting confirmation"});
            }else {
                console.log('user has no code')
                u.code = code
                u.email = user.email
                await u.save()
                functions.Email(email,code)
                res.json({status: true,message: "Email awaiting confirmation"});
            }
        } catch (e) {
            // console.log(e)
            res.json({status:false,message:'Email not saved'})
        }
        // User.update({'phone': user.phone}, {$set: {email: user.email,code: code}},function(err, user){
        //     if(err === null){
        //         functions.Email(email, code);
        //         res.json({status: true,message: "Email awaiting confirmation"});
        //     }else{
        //         res.json({status: false,message: "Email not saved"});
        //     }
        // });
    });
    // app.post('/updateEmail', function(req, res) {
    //     var user = req.body,
    //         email = user.email;
    //     var code = Math.round((Math.random() * 1000000));
    //     console.log(code)
    //     User.update({'phone': user.phone}, {$set: {email: user.email,code: code}},function(err, user){
    //         if(err === null){
    //             functions.Email(email, code);
    //             res.json({status: true,message: "Email awaiting confirmation"});
    //         }else{
    //             res.json({status: false,message: "Email not saved"});
    //         }
    //     });
    // });

    app.post('/updateOccupation', function(req, res) {
        var user = req.body;
        User.findOne({
            'phone': user.phone
        }, function(err, user_obj) {
            if (user_obj) {
                var occupation = user.occupation;
                User.update({
                    'phone': user.phone
                }, {
                    $set: {
                        occupation: occupation
                    }
                });
                res.json({
                    status: true,
                    message: "Occupation updated successfully"
                });
            } else {
                res.json({
                    status: false,
                    message: "User does not exist"
                });
            }
        });
    });

    app.post('/updateProfile/loan', function(req, res) {
        var user = req.body;

        User.update({
            'phone': user.phone
        }, {
            firstname: user.firstname,
            lastname: user.lastname,
            middlename: user.middlename,
            dob: user.dob,
            gender: user.gender,

            // marital_status: user.marital_status,
            // deppendants: user.deppendants,
            // qualification: user.high_qualification,
            employee_status: user.employee_status,
            employer: user.employer,
            // experience: user.experience,
            monthly_net_income: parseFloat(user.monthly_net_income, 2),

            location: user.location,
            address: user.address,
            type_of_home: user.type_of_home,

            bank_name: user.bank_name,
            account_name: user.name_on_account,
            account_number: user.account_number,
            bank_code: user.bank_code,
        }, function(err, user_obj) {

            if (user_obj) {
                res.json({
                    status: true,
                    message: "Profile updated successfully"
                });

            } else {
                res.json({
                    status: false,
                    message: "User profile not updated"
                });
            }
        });
    });

    app.post('/forgotPin', function(req, res) {
        var user = req.body;
        console.log(user.phone);

        //Send OTP SMS
        var pin = Math.round((Math.random() * 10000));
        user.pin = bcrypt.hashSync(pin);

        User.update({
            'phone': user.phone
        }, {
            $set: {
                pin: user.pin
            }
        }, function(err, user_obj) {

            if (err === null) {

                functions.ForgotPin(user.phone, pin);

                res.json({
                    status: true,
                    message: "New pin request successful"
                });
            } else {
                res.json({
                    status: false,
                    message: "User does not exist"
                });
            }
        });
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) return next();
        res.redirect('/');
    }

    app.post('/updateProfile', function(req, res) {
        var user = req.body;
        console.log(user);

        user.pin = bcrypt.hashSync(user.pin);

        User.update({
            'phone': user.phone
        }, {
            $set: user
        }, function(err, user) {

            if (err !== null) {
                res.json({
                    status: false,
                    message: 'An error occurred'
                });
            }
            else {

                res.json({
                    status: true,
                    message: 'User updated successfully'
                });
            }
        });
    });

    app.post('/verifyBVN', function(req, res) {
        req.body.merchantId = "SC91YCQSEKLGDGEITUMOQ";
        req.body.apiKey = "76a13876-737b-4f6d-bd55-cdef2e25aa0a";
        var data = req.body;
        superagent.post("https://api.amplifypay.com/bvn/sendcode").send(data).set('Accept', 'application/x-www-form-urlencoded').end(function(err, response) {
            if (err) {
                console.log(err);
            }
            console.log(response.body);
            if (response.body.ResponseCode == "B02") {
                res.json({
                    status: false,
                    data: response.body,
                    message: "Invalid BVN"
                });
            } else {
                res.json({
                    status: true,
                    data: response.body,
                    message: "OTP sent successfully"
                });
            }
        });
    });

    app.post('/validateBVN', function(req, res) {
        req.body.merchantId = "SC91YCQSEKLGDGEITUMOQ";
        req.body.apiKey = "76a13876-737b-4f6d-bd55-cdef2e25aa0a";

        superagent.post("https://api.amplifypay.com/bvn/validate").send(req.body).set('Accept', 'application/x-www-form-urlencoded').end(function(err, response) {
            if (err) {
                console.log(err);
            }
            console.log(response.body);
            if (response.body.ResponseCode == 'RR') {
                res.send({
                    status: false,
                    message: "False OTP"
                });
            } else {
                User.findOne({
                    "phone": req.body.phone
                }, function(err, user) {
                    if (!user) {
                        res.json({
                            status: false,
                            message: "User does not exist"
                        });
                    } else {
                        Loan.update({
                            "phone": req.body.phone
                        }, {
                            $set: {
                                bvn_status: "2"
                            }
                        });

                        User.update({
                            "phone": req.body.phone
                        }, {
                            $set: {
                                bvn_status: "validated"
                            }
                        });

                        res.json({
                            status: true,
                            message: "BVN validation was successful"
                        });
                    }
                });
            }
        });
    });

    app.get('/getInvestments', function(req, res) {

        Investment.find({}).populate('user', ['firstname', 'lastname', 'phone']).exec(function(err, invest) {
            if (!invest) {
                res.json({
                    status: false,
                    message: "No investment plan found"
                });
            } else {
                res.json({
                    status: true,
                    data: invest,
                    message: "All investments fetched successfully"
                });
            }
        });
    });

    app.post('/createInvestment', function(req, res) {

        var investment = {};
        investment.name = req.body.name;
        investment.code = (req.body.code).toLowerCase();
        investment.rate = req.body.rate;
        investment.period = req.body.period;

        Investment.findOne({
            'code': investment.code
        }, function(err, invest) {
            if (invest) {
                res.json({
                    status: false,
                    message: "Investment code already exists"
                });
            } else {
                Investment.create(investment);
                res.json({
                    status: true,
                    data: investment,
                    message: "Investment created successfully"
                });
            }
        });
    });

    app.post('/setRate', function(req, res) {
        var code = req.body.code;
        var rate = req.body.rate;

        Investment.findOne({
            'code': code
        }, function(err, invest) {
            if (!invest) {
                res.json({
                    status: false,
                    message: "Investment does not exist"
                });
            } else {
                Investment.update({
                    'code': code
                }, {
                    $set: {
                        rate: rate
                    }
                }, function(err, response) {
                    if (err === null) {
                        res.json({
                            status: true,
                            message: "Investment rate set successfully"
                        });
                    } else {
                        res.json({
                            status: false,
                            message: "Error occurred setting investment rate"
                        });
                    }
                });
            }
        });
    });

    app.post('/createLoan', function(req, res) {
        loanmodule.createLoan(req.body, function(status, data){
            // console.log(req.body)

            res.send({
                status: status,
                data: data.data,
                message: data.message
            });
        });
    });

    app.post('/updateLoan', function(req, res) {

        let loan_id = req.body.loan_id,
            amount = req.body.amount,
            end_date = req.body.end_date,
            start_date = req.body.start_date

        Loan.findOne({
            _id: loan_id
        }, function(err, loan) {

            if (err){
                res.json({status: false,message: "User does not exist"
                });
            }else {
                if (amount){

                    if (amount >=1000 && amount <= 3000) {
                        var result = parseFloat(amount) * (45 / 100);
                        loan.tenure = "7"
                    }else if (amount >= 3001 && amount<=5000) {
                        var result = parseFloat(amount) * (40 / 100);
                        loan.tenure = "10"
                    }else if (amount >=5001) {
                        var result = parseFloat(amount) * (30 / 100);
                        loan.tenure = "15"
                    }
                    var total = result + parseFloat(amount);

                    loan.repayment_left = total;
                    loan.interest = result.toFixed(2);
                    loan.amount = parseFloat(amount);
                }

                if (end_date,start_date)
                {
                    loan.end_date = end_date;
                    loan.start_date = start_date;
                }

                /*if (req.body.occupation) {
                    switch (req.body.occupation) {
                        case "1":
                            {
                                loan.tenure = "1";
                                loan.start_date = functions.Create();
                                loan.end_date = functions.later(1);
                                loan.payment_made = 0;
                                loan.repayment_left = total;
                                break;
                            }
                        case "2":
                            {
                                loan.tenure = "1";
                                loan.start_date = functions.Create();
                                loan.end_date = functions.later(1);
                                loan.payment_made = 0;
                                loan.repayment_left = total;
                                break;
                            }
                        case "3":
                            {
                                loan.tenure = "1";
                                loan.start_date = functions.Create();
                                loan.end_date = functions.later(1);
                                loan.payment_made = 0;
                                loan.repayment_left = total;
                                break;
                            }
                        case "4":
                            {
                                loan.tenure = "1";
                                loan.start_date = functions.Create();
                                loan.end_date = functions.later(1);
                                loan.payment_made = 0;
                                loan.repayment_left = total;
                                break;
                            }
                    }
                } else {
                    return res.json({
                        status: false,
                        message: "User occupation does not exist"
                    });
                }*/

                loan.save(function(err, response) {

                    if (err)
                    {
                        return res.json({
                            status: false,
                            message: "Error updating loan"
                        });
                    }

                    return res.json({
                        status: true,
                        data: loan,
                        message: "Loan updated successfully"
                    });
                });
            }
        });
    });

    app.get('/getCards', function(req, res) {
        Card.find({}, function(err, cards) {
            if (!cards) {
                res.json({
                    status: false,
                    message: "No card found"
                });
            } else {
                res.json({
                    status: true,
                    data: cards,
                    message: "All cards fetched successfully"
                });
            }
        });
    });

    app.get('/getMembers', function(req, res) {
        Member.find({}, function(err, members) {
            if (!members) {
                res.json({
                    status: false,
                    message: "No member found"
                });
            } else {
                res.json({
                    status: true,
                    data: members,
                    message: "All members fetched successfully"
                });
            }
        });
    });

    app.get('/loans', function(req, res) {
      res.redirect('loans/1');
    });

    app.get('/loans/:page', function(req, res) {

        queryHelper(Loan)
            //.$where("this.status === 'pending'")
            .with({path: 'user', select:'firstname lastname phone'})
            .page(req.params.page)
            .paginate(20)
            .getMany(function(err, data) {
            if (err !== null) {
                res.json({
                    status: false,
                    message: "No loan found"
                });
            } else {
                console.log(data.modelData);
                res.json({
                    status: true,
                    data: data.modelData,
                    resultCount: data.resultCount,
                    currentPage: data.currentPage,
                    pageCount: data.pageCount,
                    totalItems: data.totalItems,
                    itemsPerPage: 20,
                    message: "All loans fetched successfully"
                });
            }
        });
    });

    app.get('/loans/pending', function(req, res) {
      res.redirect('/loans/pending/1');
    });

    app.get('/loans/pending/:page', function(req, res) {

        let q = req.query.q;

        loanmodule.q(q).getPendingLoans(req.params.page, function(status, data){
            res.send(data);
        });
    });

    app.get('/loans/no-doc', function(req, res) {
      res.redirect('/loans/no-doc/1');
    });

    app.get('/loans/no-doc/:page', function(req, res) {

        let q = req.query.q;

        loanmodule.q(q).getNoDocLoans(req.params.page, function(status, data){

            res.send(data);
        });
    });

    app.get('/loans/approved', function(req, res) {
      res.redirect('/loans/approved/1');
    });

    app.get('/loans/approved/:page', function(req, res) {

        let q = req.query.q;

        loanmodule.q(q).getApprovedLoans(req.params.page, function(status, data){

            res.send(data);
        });
    });

    app.get('/loans/declined', function(req, res) {
      res.redirect('/loans/declined/1');
    });

    app.get('/loans/declined/:page', function(req, res) {

        let q = req.query.q;

        loanmodule.q(q).getDeclinedLoans(req.params.page, function(status, data){

            res.send(data);
        });
    });

    app.get('/loans/accepted', function(req, res) {
      res.redirect('/loans/accepted/1');
    });

    app.get('/loans/accepted/:page', function(req, res) {

        let q = req.query.q;

        loanmodule.q(q).getAcceptedLoans(req.params.page, function(status, data){

            res.send(data);
        });
    });

    app.get('/loans/completed', function(req, res) {
      res.redirect('/loans/completed/1');
    });

    app.get('/loans/completed/:page', function(req, res) {

        let q = req.query.q;

        loanmodule.q(q).getCompletedLoans(req.params.page, function(status, data){

            res.send(data);
        });
    });

    // app.get('/unpaid-due-loans',function(req,res){
    //     var new_date = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).format('DD'))
    //     var first = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).add(7,'days').format('DD'))
    //     var second = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).add(10,'days').format('DD'))
    //     var third = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).add(30,'days').format('DD'))
    //     var notify = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).add(4,'days').format('DD'))
    //     // console.log(new_date,first,second,third)
    //     var page = req.query.page || 1
    //     var perPage = 20
    //     Loan.paginate({},{page:page,limit:perPage}).then((loan)=>{
    //         console.log(loan.docs.user)
    //         for (var i = 0; i < loan.length; i++) {
    //             console.log(loan[i].docs)
    //         }
    //         if (loan.docs.withdraw && loan.docs.status == "accepted") {
    //             return res.json({status:true,message:"Due loans",loan})
    //         }else {
    //             return res.json({status:false,message:'no available due loans'})
    //         }
    //     })
    // })
    // app.get('/sort-loan-by-date',(req,res)=>{
    //     var perPage = 20
    //     var page = req.query.page || 1
    //     Loan.find({}).sort({start_date: 1}).populate({
    //         path:'user',
    //         select: '-_id'
    //     }).skip((perPage * page) - perPage).limit(perPage).then((loan)=>{
    //         Loan.count().then((count)=>{
    //             res.json({
    //                 status:true,
    //                 data:loan,
    //                 currentPage:page,
    //                 pages: Math.ceil(count / perPage)
    //             })
    //         })
    //     }).catch((e)=>{
    //         console.log(e)
    //     })
    // })

    app.get('/loans/history', function(req, res) {
      res.redirect('/loans/history/1');
    });

    app.get('/loans/history/:page', function(req, res) {

        let body = req.query;

        loanmodule.getLoanHistory(body.user_id, req.params.page, function(status, data){

            res.send(data);
        });
    });

    app.get('/loans/rejected', function(req, res) {
      res.redirect('/loans/rejected/1');
    });

    app.get('/loans/rejected/:page', function(req, res) {

        let q = req.query.q;

        loanmodule.q(q).getRejectedLoans(req.params.page, function(status, data){

            res.send(data);
        });
    });

    app.get('/loans/outstanding', function(req, res) {
      res.redirect('/loans/outstanding/1');
    });

    app.get('/loans/outstanding/:page', function(req, res) {

        let q = req.query.q;

        loanmodule.q(q).getOutstandingLoans(req.params.page, function(status, data){

            res.send(data);
        });
    });

    app.get('/getTransactions', function(req, res) {
        Transaction.find({}, function(err, transactions) {
            if (!transactions) {
                res.json({
                    status: false,
                    message: "No transaction found"
                });
            } else {
                res.json({
                    status: true,
                    data: transactions,
                    message: "All transactions fetched successfully"
                });
            }
        });
    });

    app.get('/getLoans/:phone', function(req, res) {
        var phone = req.params.phone;
        Loan.find({
            'phone': phone
        }).populate('user', ['firstname', 'lastname', 'phone']).exec(function(err, loans) {
            if (!loans) {
                res.json({
                    status: false,
                    message: "No loan found"
                });
            } else {
                res.json({
                    status: true,
                    data: loans,
                    message: "All loans for " + phone + " fetched successfully"
                });
            }
        });
    });

    app.get('/getWallet/:phone', function(req, res) {
        var phone = req.params.phone;

        Transaction.find({
            'phone': phone,
            'type': "credit"
        }, function(err, transactions) {
            if (!transactions) {
                res.json({
                    status: true,
                    data: "",
                    message: "No loan found"
                });
            } else {
                var wallet = 0;
                async.forEach(transactions, function(transaction, callback) {
                    wallet += parseFloat(transaction.amount);
                    callback();
                }, function(data) {
                    res.json({
                        status: true,
                        data: wallet,
                        message: "Wallet amount fetched for " + phone
                    });
                });
            }
        });
    });

    app.post('/updateWallet', function(req, res) {
        var phone = req.body.phone;
        var type = req.body.type;
        var amount = req.body.amount;

        Transaction.create({
            phone: phone,
            type: type,
            amount: amount
        });

        Transaction.find({
            'phone': phone,
            'type': "credit"
        }, function(err, transactions) {
            if (!transactions) {
                res.json({
                    status: true,
                    data: "",
                    message: "No transactions found"
                });
            } else {
                var wallet = 0;
                async.forEach(transactions, function(transaction, callback) {
                    wallet += parseFloat(transaction.amount);
                    callback();
                }, function(data) {
                    res.json({
                        status: true,
                        data: wallet,
                        message: "Wallet " + type + "ed successfully"
                    });
                });
            }
        });
    });

    app.get('/getInvestment/:phone', function(req, res) {
        var phone = req.params.phone;
        Transaction.find({
            'phone': phone,
            'type': "credit"
        }, function(err, transactions) {
            if (!transactions) {
                res.json({
                    status: true,
                    data: "",
                    message: "No investment found"
                });
            } else {
                res.json({
                    status: true,
                    data: transactions,
                    message: "All investments fetched for " + phone
                });
            }
        });
    });

    app.get('/getCards/:phone', function(req, res) {
        var phone = req.params.phone;
        Card.find({
            'phone': phone
        }, function(err, cards) {
            if (cards.length > 0) {
                return res.json({
                    status: true,
                    data: cards,
                    message: "All cards for " + phone + " fetched successfully"
                });
            }
            return res.json({
                status: false,
                message: "No cards found"
            });
        });
    });

    app.get('/download/:filename', function(req, res) {
        var download = req.params.filename;
        var file = __dirname + '/public/app/' + download;
        var filename = path.basename(file);
        var mimetype = mime.lookup(file);
        res.setHeader('Content-disposition', 'attachment; filename=' + filename);
        res.setHeader('Content-type', mimetype);
        var filestream = fs.createReadStream(file);
        filestream.pipe(res);
    });

    app.post('/users/send-message', function(req, res) {

        usermodule.sendBulkMessage(req.body, function (status, data){

            res.send({
                status: status,
                message: data.message
            });
        });
    });

    app.post('/loans/send-message', function(req, res) {

        loanmodule.sendReminder(req.body, function (status, data){

            res.send({
                status: status,
                message: data.message
            });
        });
    });

    app.post('/user/send-message', function(req, res) {

        usermodule.sendMessage(req.body, function (status, data){

            res.send({
                status: status,
                message: data.message
            });
        });
    });

    app.post('/user/delete', function(req, res) {

        let user_id = req.body.user_id;

        usermodule.delete(user_id, function(status, data){
            res.send({
                status: status,
                message: data.message
            })
        });
    });

    app.post('/user/loan/delete', function(req, res) {

        let loan_id = req.body.loan_id;

        loanmodule.deleteLoan(loan_id, function(status, data){
            res.send({
                status: status,
                message: data.message
            })
        });
    });

    app.post('/user/update', function(req, res) {

        let user = req.body.user;

        usermodule.update(user, function(status, data){
            res.send({
                status: status,
                message: data.message
            })
        });
    });

    app.post('/employers/add', function(req, res) {

        employermodule.addEmployer(req.body.employer_name, function (status, data){

            res.send({
                status: status,
                message: data.message
            })
        });
    });

    app.post('/employers/update', function(req, res) {

        let employer_name = req.body.employer_name,
            employer_id = req.body.employer_id;

        employermodule.updateEmployer(employer_id, employer_name, function (status, data){

            res.send({
                status: status,
                message: data.message
            })
        });
    });

    app.post('/admin/employer/delete', function(req, res) {

        let employer_id = req.body.employer_id;

        employermodule.removeEmployer(employer_id, function (status, data){

            res.send({
                status: status,
                message: data.message
            })
        });
    });

    app.get('/admin/employers', function(req, res) {
      res.redirect('employers/1');
    });

    app.get('/admin/employers/:page', function(req, res) {

        employermodule.getAdminEmployers(req.params.page, function(status, data){

            data.status = status;

            res.send(data);
        });
    });
    app.post('/latePayment/penalty',async(req,res)=>{
    try {
            const loanId = req.body.loanId
            const rate = req.body.rate

            const loan = await Loan.findById(loanId)

            loan.repayment_left = (loan.amount * rate) + loan.repayment_left
            loan.interest = loan.repayment_left - loan.amount

            await loan.save()

            res.status(200).json({
                success:true,
                loan
            })
        } catch (e) {
            res.status(500).json({
                success:false,
                message:"an error occured trying to update this loan"
            })
        }
    })
};
