const cors = require('cors'),
    express = require('express'),
    app = express(),
    passport = require('passport'),
    bcrypt = require('bcrypt-nodejs'),
    request = require('request'),
    db = require('../database/database'),
    multer = require('multer'),
    superagent = require('superagent'),
    sha256 = require('sha256'),
    tokenizer = require('../app/payments/tokenizer'),
    loanmodule = require('../app/loan/loan'),
    usermodule = require('../app/user/users.js'),
    employermodule = require('../app/employer/employers.js'),
    moment    = require('moment')

    withdrawmodule = require('../app/withdraw/withdraw'),
    investmodule = require('../app/investment/invest'),
    bankmodule = require('../app/bank/bank'),
    queryHelper = require('../utils/query-helper'),
    phantom = require('phantom'),
    fs = require('fs'),

    crypto = require("crypto");

//DB models
var User = db.User;
var Bvn = db.BVN;
var Bank = db.Bank;
var Card = db.Card;
var Loan = db.Loan;
var Member = db.Member;
var Investment = db.Investment;
var Transaction = db.Transaction;
var Transaction = db.Transaction;
var Cooperative = db.Cooperative;
var Withdraw = db.Withdraw;
var async = require('async');
var constant = require('../utils/constants');
var functions = require('../utils/functions');
var Flutterwave = require('flutterwave');
var flutterwave = new Flutterwave(constant.flutterwave.api_key, constant.flutterwave.merchant_key);
var paystack = require('paystack')('sk_live_22eb5eb7630c8ffb986c19ca0ade0722da8b9005');

/*User.create({
   phone: '08137605681',
   email_status: '',
   phone_status: '',
   otp: '1234',
   balance: '2000',
   pin: '1234',
   email: 'sobeo17@gmail.com',
   code: 1234,
   firstname: 'Darlington',
   lastname: 'Sobio',
   middlename: '',
   dob: new Date(),
   rebill_token   : '',
   transaction_id   : '',
}, function(err, res){
    console.log(err);
});*/


module.exports = function(app, passport) {

    app.post('/verify', function(req, res) {
        var options = {
            method: 'GET',
            url: 'https://api.paystack.co/transaction/verify/' + req.body.ref,
            headers: {
                'postman-token': 'f7ee9d1c-f2a8-e426-2623-fd8cd09ec35d',
                'cache-control': 'no-cache',
                authorization: 'Bearer sk_live_22eb5eb7630c8ffb986c19ca0ade0722da8b9005'
            }
        };

        request(options, function(error, response, body) {
            if (error) throw new Error(error);
            data = JSON.parse(body);
            if (data.status === false) {
                res.send({
                    status: false,
                    message: data.message
                });
            } else {
                Loan.update({
                    _id: req.body.ref
                }, {
                    $set: {
                        auth: data.data.authorization.authorization_code
                    }
                });
                res.send({
                    status: true,
                    message: data.message,
                    data: data
                });
            }
        });
    });

    app.post('/tokenize/complete/:loan_id', function(req, res) {

        let loan_id = req.params.loan_id,
            otp = req.body.otp;

        tokenizer.complete(loan_id, otp, function(status, data) {
            res.send({
                status: status,
                data: {},
                message: data.message
            });
        });
    });

    app.post('/tokenize/complete-auth/:loan_id', function(req, res) {

        let loan_id = req.params.loan_id;

        tokenizer.completeAuth(loan_id, function(status, data) {
            res.send({
                status: status,
                data: {},
                message: data.message
            });
        });
    });

    app.post('/tokenize/:loan_id', function(req, res) {

        let loan_id = req.params.loan_id;
        var year = moment(new Date()).format('YY')
        var month = moment(new Date()).format('MM')
        // console.log(moment().format('dddd'))
        // console.log(moment(new Date()).format('DD MM YY'))
        if (req.body.expiry_year <= year && req.body.expiry_month <= month) {
            return res.json({status:false,meaasge:'Card expiration too close!! please use another card'})
        }else {
            tokenizer.initialize(loan_id, req.body, function(status, data) {
                res.send({
                    status: status,
                    data: data,
                    send_otp: data.send_otp === true ? true : false,
                    open_url: data.open_url === true ? true : false,
                    message: data.message
                });
            });
        }
    });

    /*app.post('/tokenize/authorize/:user_id', function(req, res) {
        let otp = req.body.otp,
            user_id = req.params.user_id;

        tokenizer.complete(user_id, otp, function(status, data) {
            res.send({
                status: status,
                data: {},
                message: data.message
            });
        });
    });*/

    app.post('/user/acceptloan', function(req, res) {
        Loan.findOne({_id: req.body.loan_id,user: req.body.user_id}).populate('user').exec(function(err, loan) {
            console.log(loan)

            if (err || !loan){
                res.json({status: false,message: "There was an error"});

                return false;
            }

            let user = loan.user;

            loan.status = 'accepted';

            if (user.rebill_token && !loan.withdraw){
                withdrawmodule.createWithdrawalRequest(loan, user, function(status, withdrawal){

                    loan.withdraw = withdrawal._id;

                    loan.save(function(err, loan){
                        console.log(err);

                        res.json({status: true,message: "Loan accepted"});
                    });
                });
            }else{
        	    res.json({status: true,message: "Loan accepted"});
            }
        });
    });

    app.post('/user/declineloan', function(req, res) {

        let body = req.body;

        console.log(body);

        loanmodule.rejectLoan(body.loan_id, body.user_id, function(status, data){
            res.send({
                status: status,
                message: data.message
            });
        });
    });

    app.get('/employers', function(req, res) {

        employermodule.getEmployers(function(status, data){
            res.send({
                status: status,
                data: data.employers
            });
        });
    });

    /*app.post('/loan/repayment/:user_id', function(req, res) {
        let otp = req.body.otp,
            user_id = req.body.user_id;

        loanmodule.repayLoan(user_id, function(status, data) {

            res.send({
                status: status,
                data: data,
                message: data.message
            });
        });
    });*/

    app.post('/loan/repayment/:loan_id', function(req, res) {

        let amount = req.body.amount,
            loan_id = req.params.loan_id;

        console.log(req.body);

        loanmodule.repayLoan(loan_id, amount, function(status, data) {

            res.send({
                status: status,
                data: {},
                message: data.message
            });
        });
    });

    app.get('/cron/repay-loan', function(req, res) {

        let amt,
            today = new Date(functions.today()).getTime(),
            cnt = 0;

        console.log("Date " + functions.today());

        Loan.find({
                withdraw: {$ne: null},
                repayment_left: {$gt: 0},
                status: constant.status.ACCEPTED,
                //end_date: today
            })
            //.$where('this.repayment_left > 0')
            //.$where("this.status === " + "'" + constant.status.ACCEPTED + "'")
            .$where('new Date(this.end_date).getTime() <= ' + "'" + today + "'" )
            //.where("withdraw").ne(null)//filter out the ones that don't have a category
            .populate('withdraw', null, { payout_status: constant.status.COMPLETED })
            .exec(function(err, loans) {

            loans = loans.filter(function(d){ return d.withdraw; });

            let loan_length = loans ? loans.length : 0;

            if (loan_length === 0)
            {
                console.log("no loan to be repayed");

                res.send({
                    status: true,
                    data: {},
                    message: 'No loan to be repayed'
                });

                return false;
            }

            console.log( "Number of loans: " + loans.length );
            console.log( "Today's date: " + today );

            loans.forEach(function(loan) {

                console.log("Loan repayment date: " + loan.end_date);
                console.log("Repayment left: " + loan.repayment_left);

                amt = loan.repayment_left;

                loanmodule.repayLoan(loan._id, amt, function(status, data) {

                    cnt++;

                    console.log({
                        status: status,
                        data: data,
                        message: data.message
                    });

                    if (cnt === loan_length)
                    {
                        res.send({
                            status: true,
                            data: {},
                            message: 'Auto loan repayment processed'
                        });
                    }
                });
            });
        });
    });

    /*app.post('/loan/repayment/authorize/:user_id', function(req, res) {
        let otp = req.body.otp,
            user_id = req.body.user_id;

        loanmodule.authorizePayment(user_id, function(err, data) {
            if (err !== null)
            {
                res.send({
                    status: false,
                    error: true,
                    data: {},
                    message: "There was a problem completing action"
                });
            }

            res.send({
                status: true,
                data: data,
                message: "Loan payment successful"
            });
        });
    });*/

    app.post('/test', function(req, res) {
        Loan.findOne({
            _id: req.body.Id
        }, function(err, loan) {
            User.findOne({
                phone: loan.phone
            }, function(err, user) {
                var payload = {
                    "transactionid": req.body.Id,
                    "amount": req.body.amount,
                    "accountNumber": req.body.account_number,
                    "bankCode": req.body.bank_code,
                    "currency": "NGN",
                    "narration": "Testing Bank Payment",
                    "customerInfo": {
                        "firstname": user.firstname,
                        "lastname": user.lastname,
                        "email": user.email,
                        "phone": user.phone,
                        "address": "lagos",
                        "dob": req.body.dob,
                        "bvn": req.body.bvn
                    }
                };
                var string_payload = JSON.stringify(payload);
                var marchant_Id = 'SC91YCQSEKLGDGEITUMOQ';
                var x_digest = sha256(string_payload + "8c7590c1-3688-4e82-876a-f83e6a1a2b7d");
                console.log(x_digest);
                var options = {
                    method: 'POST',
                    url: 'https://bankpay.ampslg.com/api/v1/pay',
                    headers: {
                        'cache-control': 'no-cache',
                        'x-digest': x_digest,
                        'x-key': marchant_Id,
                        'content-type': 'application/json'
                    },
                    form: payload
                };
                request(options, function(error, response, body) {
                    if (error) throw new Error(error);
                    console.log(body);
                    res.send(body);
                });
            });
        });
    });

    app.post('/invest/:source', function(req, res) {

        let source = req.params.source,
            amount = req.body.amount,
            user_id = req.body.user_id;

        investmodule(user_id, amount, source).type(req.body.type).invest(function(status, data){

            res.send({
                status: status === false ? status : true,
                data: data,
                message: data.message
            });
        });
    });

    app.get("/invest/check/:transactionRef", function(req, res) {
        var transactionRef = req.params.transactionRef;
        Investment.find({
            transactionRef: transactionRef
        }, function(err, res) {
            if (trans) {
                return res.json({
                    status: true,
                    transaction: trans
                });
            }
            return res.json({
                status: false
            });
        });
    });

    app.post('/invest/success', function(req, res) {
        let response = req.query.resp,
            transactionRef;
        if (response.responsecode === "0") {
            transactionRef = response.merchtransactionreference.split("-")[0];
            Transaction.find({
                identifier: transactionRef
            }, function(err, trans) {
                if (trans) {
                    trans.responsecode = response.responsecode;
                    trans.transactionRef = transactionRef;
                    Investment.create(trans);
                    return res.send("Transaction successful, Close the page");
                }
            });
        } else {
            transactionRef = response.merchtransactionreference.split("-")[0];
            Transaction.find({
                identifier: transactionRef
            }, function(err, trans) {
                if (trans) {
                    trans.responsecode = response.responsecode;
                    trans.transactionRef = transactionRef;
                    Investment.create(trans);
                    return res.send("Transaction unsuccessful, Close the page");
                }
            });
        }
    });

    app.get('/loan/process/:phone/:status', function(req, res) {

        var status = req.params.status;
        var phone = req.params.phone;

        if (status) {
            if (status == "approve" || status == "decline") {
                Loan.findOne({
                    'phone': phone,
                    'status': "0"
                }, function(err, loan) {

                    if (loan) {
                        Loan.update({
                            'phone': phone,
                            'status': "pending"
                        }, {
                            $set: {
                                status: status
                            }
                        }, function(err, response) {
                            if (response == 1) {

                                var transaction = {};
                                transaction.phone = phone;
                                transaction.amount = amount;
                                transaction.type = "debit";
                                Transaction.create(transaction);

                                res.json({
                                    status: true,
                                    message: "Loan " + status + "d successfully"
                                });
                            } else {
                                res.json({
                                    status: false,
                                    message: "Error on loan " + status
                                });
                            }
                        });
                    } else {
                        res.json({
                            status: false,
                            message: "No pending loan for " + phone
                        });
                    }
                });
            } else {
                res.json({
                    status: false,
                    message: "Invalid status! Format is 'approve' or 'decline'"
                });
            }
        } else {
            res.json({
                status: false,
                message: "Invalid loan status"
            });
        }
    });

    app.get('/loan/process/:phone/:status', function(req, res) {

        var status = req.params.status;
        var phone = req.params.phone;

        if (status) {
            if (status == "approve" || status == "decline") {
                Loan.findOne({
                    'phone': phone,
                    'status': "0"
                }, function(err, loan) {

                    if (loan) {
                        Loan.update({
                            'phone': phone,
                            'status': "pending"
                        }, {
                            $set: {
                                status: status
                            }
                        }, function(err, response) {
                            if (response == 1) {

                                var transaction = {};
                                transaction.phone = phone;
                                transaction.amount = amount;
                                transaction.type = "debit";
                                Transaction.create(transaction);

                                res.json({
                                    status: true,
                                    message: "Loan " + status + "d successfully"
                                });
                            } else {
                                res.json({
                                    status: false,
                                    message: "Error on loan " + status
                                });
                            }
                        });
                    } else {
                        res.json({
                            status: false,
                            message: "No pending loan for " + phone
                        });
                    }
                });
            } else {
                res.json({
                    status: false,
                    message: "Invalid status! Format is 'approve' or 'decline'"
                });
            }
        } else {
            res.json({
                status: false,
                message: "Invalid loan status"
            });
        }
    });

    //Pay Loan
    //TODO document loan
    app.post('/charge', function(req, res) {

        Loan.findOne({
            _id: req.body.loan_id
        }).populate('user').exec(function(err, loan) {

            console.log('Rebill token: ', loan.rebill_token);

            if (loan.user.rebill_token) {

                paystack.transaction.charge({
                    "authorization_code": loan.user.rebill_token,
                    "email": loan.user.email,
                    "amount": req.body.amount
                }, function(error, body) {

                    console.log(error);
                    console.log(body);

                    if (body) {
                        if (body.data.status == "failed") {
                            res.send({
                                status: false,
                                message: body.data.gateway_response
                            });
                        } else {
                            Loan.update({
                                phone: user.phone
                            }, {
                                $inc: {
                                    payment_made: +parseFloat(req.body.amount),
                                    repayment_left: -parseFloat(req.body.amount)
                                }
                            });

                            res.send({
                                status: true,
                                message: body.data.gateway_response
                            });
                        }
                    } else {
                        res.send(error);
                    }
                });

            } else {
                res.send({
                    status: false,
                    message: "Account not tokenized"
                });
            }
        });
    });

    app.get('/resendOTP/:phone', function(req, res) {
        var phone = req.params.phone;
        User.findOne({
            'phone': phone
        }, function(err, user) {
            if (0 && !user) {
                res.json({
                    status: false,
                    message: "User does not exist"
                });
            } else {
                // var otp = Math.round((Math.random() * 10000));
                var otp = user.otp
                User.update({
                    'phone': phone
                });
                functions.SMS(phone, otp);

                res.json({
                   status: true,
                   message: "OTP sent successfully"
                });

            }
        });
    });

    app.get('/resendEmail/:email', function(req, res) {
        var email = req.params.email;
        User.findOne({
            'email': email
        }, function(err, user) {
            if (!user) {
                res.json({
                    status: false,
                    message: "User does not exist"
                });
            } else {
                // var code = Math.round((Math.random() * 10000));
                var code = user.code
                User.update({
                    'email': email
                });
                functions.Email(email, code);
                res.json({
                    status: true,
                    message: "Code sent successfully"
                });
            }
        });
    });

    app.post('/fundwallet', function(req, res) {

        User.update({
            _id: req.body.user_id
        }, {
            $inc: {
                balance: parseFloat(req.body.amount)
            }
        }, function(err, num) {

            res.send({
                status: true,
                message: "wallet Funded Successfully"
            });
        });
    });

    app.get('/getdashboard/:phone', function(req, res) {
        phone = req.params.phone;
        console.log(phone);

        User.findOne({
            'phone': phone
        }).select('id_card user_alert rebill_token _id balance eligible_loan_amounts').exec(function(err, user) {

            if (user) {
                Loan.find({
                    user: user._id,
                    repayment_left: {$gt: 0.00},
                    status: {
                        $nin: ['rejected']
                    }
                }).lean().populate('withdraw').exec(function(err, loan) {

                    /*console.log({
                        status: true,
                        loan: loan,
                        wallet: user.balance
                    });*/

                    if (loan && loan.length)
                    {
                        loan[0].paidout = loan[0].withdraw
                         && constant.status.COMPLETED === loan[0].withdraw.payout_status ? true : false;
                    }

                    res.send({
                        status: true,
                        loan: loan ? loan : [],
                        wallet: user.balance,
                        user: user
                    });
                });
            } else {
                res.send("user doesn't exist");
            }
        });
    });

    function getWallet(phone, callback) {
        User.find({
            'phone': req.body.phone
        }, 'Wallet', function(err, transactions) {
            if (!transactions) {
                callback("");
            } else {
                var wallet = 0;
                async.forEach(transactions, function(transaction, callback) {
                    wallet += parseFloat(transaction.Wallet);
                    callback();
                }, function(data) {
                    callback(wallet);
                });
            }
        });
    }

    app.get('/allwithdraw', function(req, res) {

    	let q = req.query.q;

        Withdraw.find({payout_status: constant.status.PENDING})
        .populate({path: 'user loan', select: '-pin'})
        .exec(function(err, data) {
            res.send({status: true,data: data});
        });
    });

    app.get('/withdrawal', function(req, res) {
        Withdraw.find({status: constant.status.PENDING})
        .populate({path: 'user loan', select: '-pin'})
        .exec(function(err, data) {
            res.send({
                status: true,
                data: data
            });
        });
    });

    app.get('/withdrawals/pending', function(req, res) {
      res.redirect('withdrawals/1');
    });

    app.get('/withdrawals/pending/:page', function(req, res) {

        withdrawmodule.getPendingWithdrawals(req.params.page, function(status, data){

            res.json({
                status: true,
                data: data.withdrawals,
                resultCount: data.resultCount,
                currentPage: data.currentPage,
                pageCount: data.pageCount,
                totalItems: data.totalItems,
                message: ""
            });
        });

    });

    app.get('/withdrawals/paidout', function(req, res) {
      res.redirect('withdrawals/1');
    });

    app.get('/withdrawals/paidout/:page', function(req, res) {

        withdrawmodule.getCompletedWithdrawals(req.params.page, function(status, data){

            res.json({
                status: true,
                data: data.withdrawals,
                resultCount: data.resultCount,
                currentPage: data.currentPage,
                pageCount: data.pageCount,
                totalItems: data.totalItems,
                message: ""
            });
        });
    });

    app.get('/withdrawals/declined', function(req, res) {
      res.redirect('withdrawals/1');
    });

    app.get('/withdrawals/declined/:page', function(req, res) {

        withdrawmodule.getDeclinedWithdrawals(req.params.page, function(status, data){

            res.json({
                status: true,
                data: data.withdrawals,
                resultCount: data.resultCount,
                currentPage: data.currentPage,
                pageCount: data.pageCount,
                totalItems: data.totalItems,
                message: ""
            });
        });
    });

    app.post('/withdraw', function(req, res) {

        let payload = req.body;
        req.body.Id = Math.floor(Math.random() * 10000) + 1;
        payload.status = "pending";

        console.log(payload);

        if (req.body) {
            User.findOne({
                phone: req.body.phone
            }, function(err, user) {

                if (user.balance >= parseFloat(req.body.amount)) {

                    /*User.update({
                        phone: req.body.phone
                    }, {
                        $inc: {
                            Balance: -parseFloat(req.body.amount)
                        }
                    });*/

                    withdraw.create(req.body);

                    payload.template = "withdraw";
                    payload.subject = "Withdrawal Application";
                    payload.email = user.email;
                    payload.name = user.firstname + " " + user.lastname;

                    functions.EmailCert(payload);

                    res.send({
                        status: true,
                        message: "Withdraw process initiated."
                    });

                } else {
                    res.send({
                        status: false,
                        message: "insufficient funds"
                    });
                }
            });
        } else {
            res.send({
                status: false,
                message: "error processing withdrawal"
            });
        }
    });

    app.post('/processwith', function(req, res) {
        var new_date = functions.start
        var first = functions.first
        var second = functions.second
        var third = functions.third
        var payload = {};
        Withdraw.findOne({
            _id: req.body.w_id
        }).populate('user').exec(function(err, withdrawal) {
            var loanId = withdrawal.loan
            Loan.findOne({
                _id:loanId
            }).exec(function(err,loan){
                if (loan.amount >= 1000 && loan.amount <=3000) {
                    loan.start_date = new_date
                    loan.end_date = first
                }else if (loan.amount >= 3001 && loan.amount <= 5000) {
                    loan.start_date = new_date
                    loan.end_date = second
                }else if (loan.amount >= 5001 ) {
                    loan.start_date = new_date
                    loan.end_date = third
                }
                loan.save()
            })
            if (withdrawal.payout_status === constant.status.COMPLETED){
                res.send({status: false,payout_status: withdrawal.payout_status,message: "Loan withdrawal already processed."});

                return;
            }

            let user = withdrawal.user;

            payload.template = "processedwith";
            payload.subject = "Loan Withdrawal Completed";
            payload.email = user.email;
            payload.name = user.firstname + " " + user.lastname;
            //formatMoney extends a prototype and is located in functions.js
            payload.amount = withdrawal.amount.formatMoney(2, '.', ',');

            functions.EmailCert(payload);

            withdrawal.payout_status = constant.status.COMPLETED;

            console.log(withdrawal)
            withdrawal.save(function(err, w) {

                console.log(err);

                res.send({
                    status: true,
                    payout_status: withdrawal.payout_status,
                    message: "Withdrawal processed successfully"
                });
            });
        });
    });

    app.post('/declinewithdrawal', function(req, res) {

        withdrawmodule.decline(req.body.w_id, function(status, data){

            res.send({
                status: status,
                payout_status: data.payout_status,
                message: data.message
            });
        });
    });

    app.post('/changepin/:phone', function(req, res) {
        phone = req.params.phone;
        User.findOne({
            phone: phone
        }, function(err, user) {
            if (user) {
                if (bcrypt.compareSync(req.body.oldpin, user.pin)) {
                    User.update({
                        'phone': phone
                    }, {
                        $set: {
                            pin: bcrypt.hashSync(req.body.newpin)
                        }
                    }, function(err, num) {
                        res.send({
                            status: true,
                            message: "pin changed successfully"
                        });
                    });
                } else {
                    res.send({
                        status: false,
                        message: "wrong pin"
                    });
                }
            } else {
                res.send({
                    status: false,
                    message: "user doesnt exist"
                });
            }
        });
    });

    var mult = multer({
        dest: './public/Doc/',
        rename: function(fieldname, filename) {
            return filename + Date.now();
        },
        onFileUploadStart: function(file) {
            //console.log(file.originalname + ' is starting ...')
        },
        onFileUploadComplete: function(file) {
            console.log(file.fieldname + ' uploaded to  ' + file.path);
            uploaded = file.name;
            done = true;
        }
    });

    app.post('/upload', mult, function(req, res) {

        if (done === true) {
            var upload = req.files;

            console.log(upload);
            console.log(req.body);

            let data = {};

            if (upload.user_alert)
                data.user_alert = upload.user_alert.name;

            if (upload.id_card)
                data.id_card = upload.id_card.name;

            User.update({
                _id: req.body.user_id
            }, {
                $set: data
            }, function(err, num) {
                console.log(err);

                Loan.update({
                    user: req.body.user_id,
                    status: constant.status.DECLINED
                }, {
                    $set: {status: constant.status.PENDING}
                }, function() {

                	res.send("done");
                });
            });

        } else {
            console.log('Upload failed');

            res.send("failed");
        }
    });

    app.post('/web/upload', mult, function(req, res) {

        if (done === true)
        {
            var upload = req.files;
            console.log(upload);
            console.log(req.body);

            let data = {};

            if (upload.user_alert)
                data.user_alert = upload.user_alert.name;

            if (upload.id_card)
                data.id_card = upload.id_card.name;

            User.update({
                _id: req.body.user_id
            }, {
                $set: data
            }, function(err, num) {});

            res.redirect("/dashboard");
        } else {
            res.send("failed");
        }
    });

    app.post('/addbank', function(req, res) {
        let body = req.body;

        User.update({
            _id: body.user_id
        },{
            bank_name: body.bank_name,
            bank_code: body.bank_code,
            account_name: body.account_name,
            account_number: body.account_number
        }, function(err, data){

            res.send({
                status: true,
                message: "Bank account added",
                data: req.body
            });
        });
    });

    app.get('/GetBank/:Phone', function(req, res) {

        if (req.params.Phone) {
            Bank.find({
                Phone: req.params.Phone
            }, function(err, bank) {
                res.send({
                    status: true,
                    data: bank
                });
            });
        } else {
            res.send({
                status: false,
                message: "Phone required"
            });
        }
    });
    app.post('/alert',mult,async(req,res)=>{
        try {
            var {loanId} = req.body
            var screenshot = req.files.screenshot.name

            var loan = await Loan.findById(loanId)
                loan.screenshot = screenshot
                await loan.save()
                res.send({message:'success'})
        } catch (e) {
            console.log(e)
            return res.json({message:'an error occured trying to update loan'})
        }
    })

    app.post('/Approveloan', function(req, res) {
        var new_date = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).format('DD')+' '+moment(new Date().getTime()).format("H:mm:ss"))
        console.log(new_date)
        var first = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).add(7,'days').format('DD')+' '+moment(new Date().getTime()).format("H:mm:ss"))
        var second = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).add(10,'days').format('DD')+' '+moment(new Date().getTime()).format("H:mm:ss"))
        var third = (moment(new Date()).format('YYYY') +'-'+ moment(new Date()).format('MM') +'-' + moment(new Date()).add(15,'days').format('DD')+' '+moment(new Date().getTime()).format("H:mm:ss"))
        let payload = {},
            Id = Math.floor((Math.random() * 1000000000) + 1);

        Loan.findOne({
            _id: req.body.loan_id
        }).populate('user').exec(function(err, loan) {
            // console.log(functions.today())
            // console.log(moment().format('L'))
            // console.log(moment().add(30, 'days').calendar())
            loan.status = 'approved';

            if (loan.amount >= 1000 && loan.amount <=3000) {
                loan.start_date = new_date
                loan.end_date = first
            }else if (loan.amount >= 3001 && loan.amount <= 5000) {
                loan.start_date = new_date
                loan.end_date = second
            }else if (loan.amount >= 5001 ) {
                loan.start_date = new_date
                loan.end_date = third
            }
            loan.save(function(err, saved) {

                let user = loan.user;

                payload.email = user.email;
                payload.name = user.firstname + " " + user.lastname;
                payload.status = "Approved";
                payload.amount = loan.amount;
                payload.payback = loan.repayment_left;
                payload.interest = loan.interest;
                payload.tenure = loan.tenure;
                payload.date = loan.end_date;
                payload.subject = "Alphacredit Loan Certificate";
                payload.template = "loanmail";
                payload.Id = loan._id;

                functions.EmailCert(payload);

                user.balance += loan.amount;

                user.save(function(err, saved){

                   res.send({
                       status: true,
                       message: "Loan Approved!"
                   });
                });
            });
        });
    });

    app.post('/Declineloan', function(req, res) {

        let payload = {};

        Loan.findOne({
            _id: req.body.loan_id
        }).populate('user').exec(function(err, loan) {

            loan.status = 'declined';

            fs.unlink('./public/Doc/' + loan.user.id_card, function() {});
            fs.unlink('./public/Doc/' + loan.user.user_alert, function() {});

            loan.user.id_card = "";
            loan.user.user_alert = "";

            if (req.body.decline_message)
                loan.decline_message = req.body.decline_message;

            loan.user.save(function(err, data){

            	loan.save(function(err, num) {

            	    let user = loan.user;

            	    payload.email = user.email;
            	    payload.name = user.firstname + " " + user.lastname;
            	    payload.status = "Declined";
            	    payload.amount = loan.amount.formatMoney(2, '.', ',');
            	    payload.payback = loan.repayment_left;
            	    // payload.rate = "30%";
            	    payload.interest = loan.interest;
                    payload.tenure = loan.tenure;
            	    payload.date = loan.end_date;
            	    payload.decline_message = loan.decline_message;
            	    payload.subject = "Loan Request Declined";
            	    payload.template = "declinedloan";

            	    functions.Mailer(payload);

            	    res.send({
            	        status: true,
            	        message: "Loan Declined!"
            	    });
            	});
            });
        });
    });

    app.get('/members', function(req, res) {
      res.redirect('members/1');
    });

    app.get('/members/:page', function(req, res) {

    	let q = req.query.q;

        usermodule.q(q).getUsers(req.params.page, function(status, data){

        	data.status = status;

            res.send(data);
        });
    });

    /*app.get('/members', function(req, res) {
      res.redirect('members/1');
    });

    app.get('/members/:page', function(req, res) {

        queryHelper(User)
            //.$where("this.status === 'pending'")
            .page(req.params.page)
            .paginate(20)
            .getMany(function(err, data) {
            if (err !== null) {
                res.json({
                    status: false,
                    message: "No User found"
                });
            } else {
                console.log(data.modelData);
                res.json({
                    status: true,
                    data: data.modelData,
                    resultCount: data.resultCount,
                    currentPage: data.currentPage,
                    pageCount: data.pageCount,
                    totalItems: data.totalItems,
                    itemsPerPage: 20,
                    message: ""
                });
            }
        });
    });*/

    app.post('/blockuser', function(req, res) {

        User.update({
            _id: req.body.user_id
        }, {account_status: 'blocked'}, function(err, user) {

            res.send({
               status: true,
               message: "Account blocked!"
            });
        });
    });

    app.post('/activateuser', function(req, res) {

        User.update({
            _id: req.body.user_id
        }, {account_status: 'active'}, function(err, user) {

            res.send({
               status: true,
               message: "Account activated!"
            });
        });
    });

    app.get('/Borrowers', function(req, res) {
        User.find({}, function(err, member) {
            Loan.find({}, function(err, loans) {
                var result = loans.map(function(a) {
                    return a.phone;
                });
                Borrower = [];
                for (var i = 0; i < member.length; i++) {
                    var a = result.indexOf(member[i].phone);
                    if (a == -1) {} else {
                        Borrower.push(member[i]);
                    }
                }
                res.send({
                    status: true,
                    data: Borrower
                });
            });
        });
    });

    app.get('/Lenders', function(req, res) {
        User.find({}, function(err, member) {
            Investment.find({}, function(err, invest) {
                var result = invest.map(function(a) {
                    return a.phone;
                });
                console.log(result);
                Lenders = [];
                for (var i = 0; i < member.length; i++) {
                    var a = result.indexOf(member[i].phone);
                    if (a == -1) {
                        console.log("no Investment");
                    } else {
                        b = Lenders.indexOf(member[i]);
                        console.log(Lenders);
                        if (b == -1) {
                            Lenders.push(member[i]);
                        } else {
                            console.log("Invest already exist");
                        }
                    }
                }
                console.log(Lenders);
                res.send({
                    status: true,
                    data: Lenders
                });
            });
        });
    });

    app.get('/nig-banks', function(req, res) {

        bankmodule.getBanks(function(status, data) {

            res.send({
                status: status,
                data: data.banks,
                message: data.message ? data.message : ''
            });
        });

        /*superagent.get("https://bankpay.ampslg.com/api/v1/banks")
        .set('Accept', 'application/x-www-form-urlencoded')
        .end();*/
    });

    app.get('/bank/account-details', function(req, res) {

        let acc_num = req.query.account_number,
            bank_code = req.query.bank_code;

        bankmodule.getAccountDetails(acc_num, bank_code, function(status, data) {

            res.send({
                status: status,
                data: data.data,
                message: data.message ? data.message : ''
            });
        });

        /*superagent.get("https://bankpay.ampslg.com/api/v1/banks")
        .set('Accept', 'application/x-www-form-urlencoded')
        .end(function(err, response) {
            if (err) {
                console.log(err);
            }
            data = JSON.parse(response.text);
            res.send({
                status: true,
                data: data.banks
            });
        });*/
    });

    app.get('/user/data/pdf', function(req, res){

        console.log("User data to pdf");
        User.findOne({_id: req.query.user_id}).exec(function(err, user){

            console.log(err, user);

            res.render('user-data-pdf', {title: 'Payconnect - User Data', user: user}, function(err, html) {

                res.send(html);
            });
        });
    });

    app.get('/user/data/pdf/download', function(req, res){

        let user_id = req.query.user_id,
            host = req.headers.host;

        host = req.secure ? 'https://' + host : 'http://' + host;

        console.log(host)

        User.findOne({_id: user_id}).exec(function(err, user){

            if (err || !user)
            {
                console.log(err, 'User details could not be retrieved');

                res.send('User details could not be retrieved');
                return false;
            }

            phantom.create().then(function(ph) {
                ph.createPage().then(function(page) {

                    console.log(host + '/user/data/pdf?user_id='+user_id);

                    page.open(host + '/user/data/pdf?user_id='+user_id).then(function(status) {

                        let file = 'public/pdf/user_data.pdf';

                        page.render(file).then(function(err, html) {
                            console.log(err, 'Page Rendered');

                            let newFilename = functions.formatFilename(user.firstname + '_' + user.lastname) + '.pdf';

                            console.log(newFilename);

                            res.download(file, newFilename);

                            ph.exit();
                        });
                    });
                });
            });
        });
    });
};
