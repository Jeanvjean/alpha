const cors = require('cors'),
    express = require('express'),
    app = express(),
    passport = require('passport'),
    bcrypt = require('bcrypt-nodejs'),
    request = require('request'),
    db = require('../database/database'),
    multer = require('multer'),
    superagent = require('superagent'),
    sha256 = require('sha256'),
    tokenizer = require('../app/payments/tokenizer'),
    loanmodule = require('../app/loan/loan'),
    withdrawmodule = require('../app/withdraw/withdraw'),
    investmodule = require('../app/investment/invest'),
    bankmodule = require('../app/bank/bank'),
    queryHelper = require('../utils/query-helper'),
    phantom = require('phantom'),  

    crypto = require("crypto");

//DB models
var User = db.User;
var Bvn = db.BVN;
var Bank = db.Bank;
var Card = db.Card;
var Loan = db.Loan;
var Member = db.Member;
var Investment = db.Investment;
var Transaction = db.Transaction;
var Transaction = db.Transaction;
var Cooperative = db.Cooperative;
var Withdraw = db.Withdraw;
var async = require('async');
var constant = require('../utils/constants');
var functions = require('../utils/functions');
var Flutterwave = require('flutterwave');
var flutterwave = new Flutterwave(constant.flutterwave.api_key, constant.flutterwave.merchant_key);
var paystack = require('paystack')('sk_live_22eb5eb7630c8ffb986c19ca0ade0722da8b9005');

module.exports = function(app, passport) {

    app.post('/test', function(req, res) {
        Loan.findOne({
            _id: req.body.Id
        }, function(err, loan) {
            User.findOne({
                phone: loan.phone
            }, function(err, user) {
                var payload = {
                    "transactionid": req.body.Id,
                    "amount": req.body.amount,
                    "accountNumber": req.body.account_number,
                    "bankCode": req.body.bank_code,
                    "currency": "NGN",
                    "narration": "Testing Bank Payment",
                    "customerInfo": {
                        "firstname": user.firstname,
                        "lastname": user.lastname,
                        "email": user.email,
                        "phone": user.phone,
                        "address": "lagos",
                        "dob": req.body.dob,
                        "bvn": req.body.bvn
                    }
                };
                var string_payload = JSON.stringify(payload);
                var marchant_Id = 'SC91YCQSEKLGDGEITUMOQ';
                var x_digest = sha256(string_payload + "8c7590c1-3688-4e82-876a-f83e6a1a2b7d");
                console.log(x_digest);
                var options = {
                    method: 'POST',
                    url: 'https://bankpay.ampslg.com/api/v1/pay',
                    headers: {
                        'cache-control': 'no-cache',
                        'x-digest': x_digest,
                        'x-key': marchant_Id,
                        'content-type': 'application/json'
                    },
                    form: payload
                };
                request(options, function(error, response, body) {
                    if (error) throw new Error(error);
                    console.log(body);
                    res.send(body);
                });
            });
        });
    });

    app.post('/invest/:source', function(req, res) {

        let source = req.params.source,
            amount = req.body.amount,
            user_id = req.body.user_id;

        investmodule(user_id, amount, source).type(req.body.type).invest(function(status, data){

            res.send({
                status: status === false ? status : true,
                data: data,
                message: data.message
            });
        });
    });

    app.get("/invest/check/:transactionRef", function(req, res) {
        var transactionRef = req.params.transactionRef;
        Investment.find({
            transactionRef: transactionRef
        }, function(err, res) {
            if (trans) {
                return res.json({
                    status: true,
                    transaction: trans
                });
            }
            return res.json({
                status: false
            });
        });
    });

    app.post('/invest/success', function(req, res) {
        let response = req.query.resp,
            transactionRef;
        if (response.responsecode === "0") {
            transactionRef = response.merchtransactionreference.split("-")[0];
            Transaction.find({
                identifier: transactionRef
            }, function(err, trans) {
                if (trans) {
                    trans.responsecode = response.responsecode;
                    trans.transactionRef = transactionRef;
                    Investment.create(trans);
                    return res.send("Transaction successful, Close the page");
                }
            });
        } else {
            transactionRef = response.merchtransactionreference.split("-")[0];
            Transaction.find({
                identifier: transactionRef
            }, function(err, trans) {
                if (trans) {
                    trans.responsecode = response.responsecode;
                    trans.transactionRef = transactionRef;
                    Investment.create(trans);
                    return res.send("Transaction unsuccessful, Close the page");
                }
            });
        }
    });

    app.post('/fundwallet', function(req, res) {

        User.update({
            _id: req.body.user_id
        }, {
            $inc: {
                balance: parseFloat(req.body.amount)
            }
        }, function(err, num) {

            res.send({
                status: true,
                message: "wallet Funded Successfully"
            });
        });
    });

    app.get('/getdashboard/:phone', function(req, res) {
        phone = req.params.phone;
        console.log(phone);

        User.findOne({
            'phone': phone
        }).select('id_card user_alert rebill_token _id balance').exec(function(err, user) {

            if (user) {
                Loan.find({
                    user: user._id,
                    repayment_left: {$gt: 0.00},
                    status: {
                        $nin: ['declined', 'rejected']
                    }
                }).lean().populate('withdraw').exec(function(err, loan) {

                    /*console.log({
                        status: true,
                        loan: loan,
                        wallet: user.balance
                    });*/

                    if (loan && loan.length)
                    {
                        loan[0].paidout = loan[0].withdraw
                         && constant.status.COMPLETED === loan[0].withdraw.payout_status ? true : false;
                    }

                    res.send({
                        status: true,
                        loan: loan ? loan : [],
                        wallet: user.balance,
                        user: user
                    });
                });
            } else {
                res.send("user doesn't exist");
            }
        });
    });

    function getWallet(phone, callback) {
        User.find({
            'phone': req.body.phone
        }, 'Wallet', function(err, transactions) {
            if (!transactions) {
                callback("");
            } else {
                var wallet = 0;
                async.forEach(transactions, function(transaction, callback) {
                    wallet += parseFloat(transaction.Wallet);
                    callback();
                }, function(data) {
                    callback(wallet);
                });
            }
        });
    }

    app.post('/addbank', function(req, res) {
        let body = req.body;

        User.update({
            _id: body.user_id
        },{
            bank_name: body.bank_name,
            bank_code: body.bank_code,
            account_name: body.account_name,
            account_number: body.account_number
        }, function(err, data){

            res.send({
                status: true,
                message: "Bank account added",
                data: req.body
            });
        });
    });

    app.get('/GetBank/:Phone', function(req, res) {

        if (req.params.Phone) {
            Bank.find({
                Phone: req.params.Phone
            }, function(err, bank) {
                res.send({
                    status: true,
                    data: bank
                });
            });
        } else {
            res.send({
                status: false,
                message: "Phone required"
            });
        }
    });
};